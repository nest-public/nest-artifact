#!/bin/bash
# usage example: bash quickly_test_phoronix_experiment_setup.sh your-grid5000-username


# Identify the host
machine="`echo $HOSTNAME | cut -d '.' -f 1`"


# Creat directories for phoronix-test-suite in /tmp on grid5000 machines
mkdir -p /tmp/phoronix-test-suite/installed-tests/pts
mkdir -p /tmp/phoronix-test-suite/test-results
mkdir -p /tmp/phoronix-test-suite/test-profiles/pts
echo "Info: Created necessary directories for phoronix-test-suite at /tmp"


# Create directories for storing results on the frontend
user_name=$1
kernel=`uname -r`
results_at="/home/$user_name/phoronix-results"
mkdir -p ${results_at}/${kernel}/results/
mkdir -p ${results_at}/${kernel}/terminal_output/
echo "Info: Created necessary directories to store results in /home."


# Check kernel and set taskset
if [[ "${kernel}" =~ Nest* ]]
then
	use_taskset="yes"
	cores=`nproc --all`
  	taskset_string="taskset -c 0-$((${cores} - 1))"
  	echo "Info: Using ${taskset_string} for the nest kernel."
else
	use_taskset="no"
	echo "Info: Not using taskset for CFS kernel"
fi


# Switch to schedutil power manager.
power_manager="schedutil"
power_status="passive"
/tmp/phoronix-scripts/change_powerstatus.sh ${power_status}
echo "Info: Changed power status to ${power_status}"
/tmp/phoronix-scripts/change_powermanager.sh ${power_manager}
echo "Info: Changed power manager to ${power_manager}"


# Configure paths to copy benchmarks files and retrieve experiment results
copy_data_from="/home/${user_name}/eurosys-artifact-evaluation/phoronix-benchmarks/phoronix-test-suite"
copy_data_to="/tmp/phoronix-test-suite"
result_dir_path="/tmp/phoronix-test-suite/test-results"


# Discard results from any previous experiments
rm -r ${result_dir_path}/*
echo "Info: Deleted results from previous experiments from ${result_dir_path}"


benchmark="arrayfire-1.1.0"

# Copy necessary benchmark files from frontend
cp -r ${copy_data_from}/installed-tests/pts/${benchmark}* ${copy_data_to}/installed-tests/pts/.
cp -r ${copy_data_from}/test-profiles/pts/${benchmark}* ${copy_data_to}/test-profiles/pts/.
echo "Info: Copied necessary files to configure and run the benchmark."
      	  
# Run a quick benchmark 5 times to test the setup
${taskset_string} /tmp/phoronix-scripts/phoronix_options.sh  benchmark_name="${benchmark}" number_of_runs=5 | tee terminal_output_${benchmark}_${kernel}_${power_manager}_${machine}_testrun.txt

# The nest image has phoronix configured to automatically save results for batch-run mode.
# We will find a new result directory created under /tmp/phoronix-test-suite/test-results/ for every successful run.
result_dir=`ls -t ${result_dir_path}/ | head -n 1`

# Phoronix names result directories in dd-mm-yyy* format
if [[ ${result_dir} =~ [0-9] ]]
then
	# Generate the .csv file from results
	phoronix-test-suite result-file-raw-to-csv ${result_dir}

	# Make a copy of the result .svg file and newly generated .csv file, give them a meaningful name
	cp ${result_dir_path}/${result_dir}/result-graphs/overview.svg ${results_at}/${kernel}/results/resultfile_${benchmark}_${kernel}_${power_manager}_${machine}_testrun.svg
	chown $user_name /root/${result_dir}-raw.csv
	cp /root/${result_dir}-raw.csv ${results_at}/${kernel}/results/resultfile_${benchmark}_${kernel}_${power_manager}_${machine}_testrun.csv

      	# Move the whole result directory to desired location for future reference
	cp -r  ${result_dir_path}/${result_dir} ${results_at}/${kernel}/results/resultdir_${benchmark}_${kernel}_${power_manager}_${machine}_testrun

else # Phoronix doesn't create a result directory if all tests are failed for the run
	echo "Test failed"
	continue
fi

# Copy the terminal output to /home
cp terminal_output_${benchmark}_${kernel}_${power_manager}_${machine}_testrun.txt ${results_at}/${kernel}/terminal_output/.

# Clean up
rm -r ${result_dir_path}/*


echo "Info: Checking if all went well..."
echo "Info: Result files at desired location:"
echo `ls -t /home/${user_name}/phoronix-results/$kernel/results/ | head -3`
echo "Info: Content of the resultfile:"
cat /home/${user_name}/phoronix-results/${kernel}/results/resultfile_${benchmark}_${kernel}_${power_manager}_${machine}_testrun.csv
echo "---"
echo "If the file exists and you see 5 numbers for each test in the benchmark, all went well :)."
