#!/bin/bash
# usage example: setup_phoronix_experiment.sh your-grid5000-username

mkdir -p /tmp/phoronix-scripts
echo "Created the working direcotry in /tmp"

user_name=$1
mkdir /home/$user_name/phoronix-results
echo "Created directory in /home to save results"

cp /home/$user_name/nest-artifact/phoronix-scripts/grid5000/scripts/* /tmp/phoronix-scripts/.
echo "Copied necessary scripts. You can cd to /tmp/phoronix-scripts/ and access them now."
