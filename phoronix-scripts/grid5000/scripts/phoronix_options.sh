#!/bin/bash

# Example usage:
#   bash phoronix_options.sh run_type="batch-run" benchmark_name="arrayfire" number_of_runs=10

# run_type - option to run phoronix
#            (batch-run, run, stress-run)
# benchmark_name - name of the benchmark or suite
#                  (arrayfire, aircrack-ng, multicore-suite)
# number_of_runs - number of times to run the benchmark, phoronix's default value is 3
#                  (use 1 for plotting graphs, use 10 for good accuracy)

# Default values
run_type="batch-run"

for ARGUMENT in "$@"
do
    KEY=$(echo $ARGUMENT | cut -f1 -d=)
    VALUE=$(echo $ARGUMENT | cut -f2 -d=)

    case "$KEY" in
            run_type)       run_type=${VALUE} ;;
	          benchmark_name) benchmark=${VALUE} ;;
            number_of_runs)		number_of_runs=${VALUE} ;;
            *)
    esac
done

if [ -z ${number_of_runs} ]
then
    echo "Info: Caller didn't provide number_of_runs argument, hence the benchmark will run 3 time by default."
    phoronix-test-suite $run_type $benchmark
else
    echo "Info: Running $benchmark $number_of_runs times"
    FORCE_TIMES_TO_RUN=$number_of_runs phoronix-test-suite $run_type $benchmark
fi
