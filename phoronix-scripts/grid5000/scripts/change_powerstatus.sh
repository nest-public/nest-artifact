#/bin/bash

# $1 - takes active/passive as arguments
#      (active for powersave or performance)
#      (passive for schedutil)
# Requires sudo previleges
# Example usage -
#   sudo bash change_powerstatus.sh passive

model=`lscpu | grep "Model name" | head -1`
if [[ "$model" =~ Intel* ]]
then
  echo "Detected an Intel machine, changing intel_pstate status:"
  echo "Current power status - `cat /sys/devices/system/cpu/intel_pstate/status`"
  echo ${1} | sudo tee /sys/devices/system/cpu/intel_pstate/status
  echo "New power status - `cat /sys/devices/system/cpu/intel_pstate/status`"
else
  echo "Not an intel machine. Hence doing nothing."
fi
