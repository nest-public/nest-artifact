#!/bin/bash

# $1 - takes name of the powermanager as the first argument.
#      (powersave, performance or schedutil)
# Requires sudo previleges
# Example usage -
# 	sudo bash change_powermanager.sh schedutil 

for i in /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
do
	echo ${1} > $i
done
