bin/bt.A.x
  pin no change
     hyperfine: 0.883349+-0.012780
  no pin
     hyperfine: 0.878809+-0.023741
bin/cg.A.x
  pin no change
     hyperfine: 0.118344+-0.002124
  no pin
     hyperfine: 0.124553+-0.005480
bin/ep.A.x
  pin no change
     hyperfine: 0.287521+-0.019189
  no pin
     hyperfine: 0.291582+-0.024253
bin/ft.A.x
  pin no change
     hyperfine: 0.148130+-0.002329
  no pin
     hyperfine: 0.155387+-0.006054
bin/is.A.x
  pin no change
     hyperfine: 0.060188+-0.001489
  no pin
     hyperfine: 0.062530+-0.005377
bin/lu.A.x
  pin no change
     hyperfine: 1.549918+-0.036366
  no pin
     hyperfine: 1.642313+-0.097724
bin/mg.A.x
  pin no change
     hyperfine: 0.162632+-0.002277
  no pin
     hyperfine: 0.173591+-0.009628
bin/sp.A.x
  pin no change
     hyperfine: 0.772168+-0.012115
  no pin
     hyperfine: 0.812599+-0.057074
bin/ua.A.x
  pin no change
     hyperfine: 3.659091+-0.049478
  no pin
     hyperfine: 3.332766+-0.050229
bin/bt.B.x
  pin no change
     hyperfine: 3.478504+-0.032816
  no pin
     hyperfine: 3.559115+-0.053087
bin/cg.B.x
  pin no change
     hyperfine: 1.398577+-0.019164
  no pin
     hyperfine: 1.404620+-0.017800
bin/ep.B.x
  pin no change
     hyperfine: 0.598620+-0.019052
  no pin
     hyperfine: 0.606866+-0.021892
bin/ft.B.x
  pin no change
     hyperfine: 1.319879+-0.005684
  no pin
     hyperfine: 1.427574+-0.061815
bin/is.B.x
  pin no change
     hyperfine: 0.167402+-0.002306
  no pin
     hyperfine: 0.173185+-0.008045
bin/lu.B.x
  pin no change
     hyperfine: 4.189203+-0.029743
  no pin
     hyperfine: 5.101394+-0.281828
bin/mg.B.x
  pin no change
     hyperfine: 0.470621+-0.005136
  no pin
     hyperfine: 0.488938+-0.036895
bin/sp.B.x
  pin no change
     hyperfine: 2.309443+-0.038873
  no pin
     hyperfine: 2.422216+-0.162012
bin/ua.B.x
  pin no change
     hyperfine: 4.887160+-0.087202
  no pin
     hyperfine: 4.573538+-0.181703
bin/bt.C.x
  pin no change
     hyperfine: 18.239194+-0.542065
  no pin
     hyperfine: 22.345977+-1.328267
bin/cg.C.x
  pin no change
     hyperfine: 6.611911+-0.094691
  no pin
     hyperfine: 6.653778+-0.062734
bin/ep.C.x
  pin no change
     hyperfine: 1.846881+-0.026322
  no pin
     hyperfine: 1.844315+-0.028963
bin/ft.C.x
  pin no change
     hyperfine: 5.843568+-0.014637
  no pin
     hyperfine: 6.084041+-0.128366
bin/is.C.x
  pin no change
     hyperfine: 0.567460+-0.005464
  no pin
     hyperfine: 0.566286+-0.008924
bin/lu.C.x
  pin no change
     hyperfine: 19.913860+-3.513692
  no pin
     hyperfine: 31.314119+-7.678454
bin/mg.C.x
  pin no change
     hyperfine: 3.468297+-0.023183
  no pin
     hyperfine: 3.788997+-0.282680
bin/sp.C.x
  pin no change
     hyperfine: 18.521498+-0.513245
  no pin
     hyperfine: 26.437313+-1.632978
bin/ua.C.x
  pin no change
     hyperfine: 16.591397+-0.162720
  no pin
     hyperfine: 24.849027+-1.778865
