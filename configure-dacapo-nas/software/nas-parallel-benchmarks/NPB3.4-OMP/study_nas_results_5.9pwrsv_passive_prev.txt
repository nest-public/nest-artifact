bin/bt.A.x
  pin no change
     hyperfine: 0.899882+-0.021153
  no pin
     hyperfine: 1.211882+-0.057117
bin/cg.A.x
  pin no change
     hyperfine: 0.127286+-0.004257
  no pin
     hyperfine: 0.158139+-0.007972
bin/ep.A.x
  pin no change
     hyperfine: 0.300104+-0.022372
  no pin
     hyperfine: 0.379395+-0.024165
bin/ft.A.x
  pin no change
     hyperfine: 0.152529+-0.005821
  no pin
     hyperfine: 0.202091+-0.016879
bin/is.A.x
  pin no change
     hyperfine: 0.063052+-0.002135
  no pin
     hyperfine: 0.097820+-0.008136
bin/lu.A.x
  pin no change
     hyperfine: 1.563730+-0.020500
  no pin
     hyperfine: 2.035661+-0.132156
bin/mg.A.x
  pin no change
     hyperfine: 0.165943+-0.002851
  no pin
     hyperfine: 0.348339+-0.044760
bin/sp.A.x
  pin no change
     hyperfine: 0.798598+-0.021297
  no pin
     hyperfine: 1.705055+-0.197026
bin/ua.A.x
  pin no change
     hyperfine: 3.708326+-0.038313
  no pin
     hyperfine: 4.283975+-0.443562
bin/bt.B.x
  pin no change
     hyperfine: 3.501874+-0.038788
  no pin
     hyperfine: 4.862451+-0.227328
bin/cg.B.x
  pin no change
     hyperfine: 1.436002+-0.051169
  no pin
     hyperfine: 1.605038+-0.082958
bin/ep.B.x
  pin no change
     hyperfine: 0.604840+-0.018590
  no pin
     hyperfine: 0.749597+-0.041265
bin/ft.B.x
  pin no change
     hyperfine: 1.321990+-0.005279
  no pin
     hyperfine: 2.122214+-0.527019
bin/is.B.x
  pin no change
     hyperfine: 0.169115+-0.004674
  no pin
     hyperfine: 0.228365+-0.020431
bin/lu.B.x
  pin no change
     hyperfine: 4.346531+-0.046913
  no pin
     hyperfine: 5.988888+-0.842938
bin/mg.B.x
  pin no change
     hyperfine: 0.474396+-0.004622
  no pin
     hyperfine: 1.148427+-0.142661
bin/sp.B.x
  pin no change
     hyperfine: 2.333257+-0.030598
  no pin
     hyperfine: 5.835442+-0.584086
bin/ua.B.x
  pin no change
     hyperfine: 4.989012+-0.126372
  no pin
     hyperfine: 6.464910+-0.529194
bin/bt.C.x
  pin no change
     hyperfine: 18.430027+-1.230709
  no pin
     hyperfine: 23.544036+-1.020189
bin/cg.C.x
  pin no change
     hyperfine: 6.762198+-0.110427
  no pin
     hyperfine: 7.573852+-0.219894
bin/ep.C.x
  pin no change
     hyperfine: 1.847869+-0.032410
  no pin
     hyperfine: 1.997766+-0.064202
bin/ft.C.x
  pin no change
     hyperfine: 5.888542+-0.034961
  no pin
     hyperfine: 7.384916+-0.419495
bin/is.C.x
  pin no change
     hyperfine: 0.576518+-0.010862
  no pin
     hyperfine: 0.659277+-0.071286
bin/lu.C.x
  pin no change
     hyperfine: 20.939842+-4.365340
  no pin
     hyperfine: 23.719295+-3.883848
bin/mg.C.x
  pin no change
     hyperfine: 3.484761+-0.016867
  no pin
     hyperfine: 5.684851+-0.530322
bin/sp.C.x
  pin no change
     hyperfine: 20.303899+-4.269862
  no pin
     hyperfine: 29.125363+-0.872300
bin/ua.C.x
  pin no change
     hyperfine: 17.298641+-0.179483
  no pin
     hyperfine: 25.728888+-1.539772
