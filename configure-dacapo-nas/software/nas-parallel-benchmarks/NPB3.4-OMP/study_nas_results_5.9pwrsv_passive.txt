bin/bt.A.x
  pin no change
     hyperfine: 0.895470+-0.017241
  no pin
     hyperfine: 1.201375+-0.107177
bin/cg.A.x
  pin no change
     hyperfine: 0.126137+-0.004262
  no pin
     hyperfine: 0.157364+-0.009129
bin/ep.A.x
  pin no change
     hyperfine: 0.288190+-0.023149
  no pin
     hyperfine: 0.366166+-0.025714
bin/ft.A.x
  pin no change
     hyperfine: 0.151741+-0.003535
  no pin
     hyperfine: 0.212140+-0.022286
bin/is.A.x
  pin no change
     hyperfine: 0.062783+-0.002551
  no pin
     hyperfine: 0.095985+-0.009986
bin/lu.A.x
  pin no change
     hyperfine: 1.558086+-0.013637
  no pin
     hyperfine: 2.044860+-0.123864
bin/mg.A.x
  pin no change
     hyperfine: 0.167163+-0.003876
  no pin
     hyperfine: 0.374977+-0.058300
bin/sp.A.x
  pin no change
     hyperfine: 0.784564+-0.020810
  no pin
     hyperfine: 1.747368+-0.218137
bin/ua.A.x
  pin no change
     hyperfine: 3.720318+-0.053966
  no pin
     hyperfine: 4.189106+-0.121161
bin/bt.B.x
  pin no change
     hyperfine: 3.509518+-0.082848
  no pin
     hyperfine: 5.012517+-0.277108
bin/cg.B.x
  pin no change
     hyperfine: 1.449634+-0.058918
  no pin
     hyperfine: 1.598819+-0.084755
bin/ep.B.x
  pin no change
     hyperfine: 0.596824+-0.023216
  no pin
     hyperfine: 0.759580+-0.033404
bin/ft.B.x
  pin no change
     hyperfine: 1.326563+-0.015373
  no pin
     hyperfine: 2.090491+-0.330757
bin/is.B.x
  pin no change
     hyperfine: 0.170756+-0.004785
  no pin
     hyperfine: 0.232647+-0.012860
bin/lu.B.x
  pin no change
     hyperfine: 4.344479+-0.046802
  no pin
     hyperfine: 6.281581+-0.872718
bin/mg.B.x
  pin no change
     hyperfine: 0.474408+-0.004555
  no pin
     hyperfine: 1.192746+-0.161092
bin/sp.B.x
  pin no change
     hyperfine: 2.359081+-0.036068
  no pin
     hyperfine: 5.630190+-0.828422
bin/ua.B.x
  pin no change
     hyperfine: 4.940075+-0.081663
  no pin
     hyperfine: 6.650111+-0.448779
bin/bt.C.x
  pin no change
     hyperfine: 18.557105+-1.086012
  no pin
     hyperfine: 25.330360+-1.028316
bin/cg.C.x
  pin no change
     hyperfine: 6.714483+-0.079551
  no pin
     hyperfine: 7.516466+-0.132724
bin/ep.C.x
  pin no change
     hyperfine: 1.847475+-0.033887
  no pin
     hyperfine: 1.973656+-0.044713
bin/ft.C.x
  pin no change
     hyperfine: 5.929999+-0.028198
  no pin
     hyperfine: 7.588960+-0.349328
bin/is.C.x
  pin no change
     hyperfine: 0.573554+-0.003591
  no pin
     hyperfine: 0.688658+-0.040901
bin/lu.C.x
  pin no change
     hyperfine: 21.488622+-3.985920
  no pin
     hyperfine: 35.872659+-4.872090
bin/mg.C.x
  pin no change
     hyperfine: 3.487464+-0.032689
  no pin
     hyperfine: 5.776112+-0.430522
bin/sp.C.x
  pin no change
     hyperfine: 19.659911+-1.756631
  no pin
     hyperfine: 32.141310+-2.289541
bin/ua.C.x
  pin no change
     hyperfine: 17.299687+-0.221074
  no pin
     hyperfine: 29.024597+-1.667049
