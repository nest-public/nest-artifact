# Directions for replicating the Nest experiments

This document explains how to replicate the Nest experiments within the Grid 5000 environment, using our prepared image.  We strongly suggest taking this approach.  To create an image from scratch, please clone `https://gitlab.inria.fr/nest-public/nest-artifact.git` and then follow the directions in `nest-artifact/image_creation/README.md` before returning to this file.

The artifact covers Figures 3-8 and Figure 9 in the paper.  We do not support reproducing Table 4, as that requires many weeks of compute time.  Unfortunately, it is also not practical to make the 160-core Intel E7-8870 v4 available, so we only provide instructions for reproducing the results on the Intel 6130's and the Intel 5218.

# Getting started

Request an open access account on [Grid 5000](https://www.grid5000.fr/w/Special:G5KRequestAccountUMS?openaccess).  Explain that you are evaluating the artifact for the EuroSys 2022 paper "OS Scheduling with Nest: Keeping Threads Close Together on Warm Cores" where some of the authors are from the Whisper team at Inria (Julia Lawall, Himadri Pandya, and Gilles Muller).

Please familiarize yourself with the [Grid 5000 usage policy](https://www.grid5000.fr/w/Grid5000:UsagePolicy).

Add the following to your `.ssh/config` file (replace xxx with your Grid 5000 username):

```
# example grenoble.g5k
Host g5k
     User xxx
     Hostname access.grid5000.fr

Host *.g5k
   User xxx
   ProxyCommand ssh g5k "nc -q 0 `basename %h .g5k` %p"
   ForwardAgent yes
```

Afterwards, it should be possible to connect to the Grenoble Grid 5000 frontend as follows:

```
ssh grenoble.g5k
```

Once you have connected to the Grenoble Grid 5000 frontend, clone the artifact into your home directory:

```
jlawall@fgrenoble:~$ git clone https://gitlab.inria.fr/nest-public/nest-artifact.git
```

The image is too large to store in git.  Do the following to store it in your list of known images (this only needs to be done once):

```
jlawall@fgrenoble:~$ cd /home/hpandya/eurosys-artifact-evaluation
jlawall@fgrenoble:/home/hpandya/eurosys-artifact-evaluation$ kaenv3 -a nest.yaml
```

# Directions for replicating the configure, DaCapo, and NAS experiments.

# Mostly automatic approach

The following scripts are found in `~/nest-artifact/configure-dacapo-nas/scripts`:

```
reserve
run
graphs
```

These scripts will run the complete configure, DaCapo and NAS experiments.  All of these scripts should be run on the Grenoble frontend.  These scripts are described in more detail below.  The subsequent section, "Manual approach", gives more detail about the operations performed by these scripts, and can be used as a fallback.

## Reserving and deploying the machines

The script `reserve` will make a single reservation for all of the following machines:

* configure: dahu-1, yeti-3, troll-1
* dacapo: dahu-1, dahu-6, dahu-14, dahu-15, yeti-2, yeti-3, troll-1
* nas: dahu-15, yeti-1, troll-4

These correspond to the machines listed in the paper as follows:

* dahu: 64-core Intel 6130
* yeti: 128-core Intel 6130
* troll: 64-core Intel 5218

For the DaCapo benchmarks, the tests are divided across the dahu and yeti machines as follows:

* dahu-1: All benchmarks except the ones noted below.
* dahu-6: tradebeans, batik-eval, cassandra-eval, jme-eval, kafka-eval
* dahu-14: tomcat-eval
* dahu-15: eclipse-eval, biojava-eval
* yeti-2: All benchmarks except tomcat-eval
* yeti-3: tomcat-eval

The script will ask about the duration of the reservation (for example, 3 for 3 hours, or 3:30 for 3 hours and 30 minutes).  It is important to be sure that all of the above machines are available for the entire time of the reservation.  You can check machine availability on the [Grid 5000 webpage](https://www.grid5000.fr/w/Grid5000:Home) by following the link `Platform status` on the far left and then `nodes` under `Grenoble`.  If any machine is not available for part of the requested time, the reservation will be refused or delayed.  Given the large number of machines used, it may never occur that all are available at the same time.  You can edit the reserve script to redefine the `NODES` variable to remove machines that are not available, and then do those tests at another time.

Note that reservations must follow the Grid 5000 reservation policy.  In simpified form, in terms of the specific machines needed for the experiments, weekday reservations between 9am and 5pm CET are limited to 8 hours, or completion by 7pm CET, whichever comes first.  Furthermore, only one troll or yeti can be reserved during that time.  There is no limit between 5pm and 9am CET and there is no limit on the weekend.  To check that your reservation does not violate the usage policy, you can run the following on the frontend:

```
usagepolicycheck -t
```

When you are done with your reservation, you can find the job id of your reservation on the Platform status page, and then run the following on the frontend to terminate the reservation:

```
oardel jobid
```

More information about reservations is available [here](https://www.grid5000.fr/w/Grid5000:UsagePolicy).

Running the `reserve` script may take 10-20 minutes.

## Running the experiments

_It is suggested to first read through this section and then try the quickrun script mentioned at the end._

At this point, your nodes should be running either the default kernel that comes up when you deploy an image, 5.10.0-10-amd64, or 5.9.0freq.  If a node is running some other kernel (check with `uname -r`), ssh to the node as root and run:

```
/boot/reboot_kexec.sh 5.9.0freq
```

This is only an issue if you have done some previous experiment.  It is not an issue when you first reserve and deploy the nodes.

The script `run` (no arguments) will ssh to the nodes that you have reserved and run the experiments.  If you did not reserve all of the nodes used by the script, the corresponding ssh will fail.  No harm is done.  The results are placed in your home directory, in the subdirectories `configuretrace`, `dacapotraces`, and `nastraces`.  These directories will be created if they do not exist.

When you run the `run` script, you may see the following output, which is normal:

```
Created symlink /etc/systemd/system/multi-user.target.wants/run_everything.service → /etc/systemd/system/run_everything.service.
```

The experiments run in parallel on the different machines and the script does not wait for them to complete.  You can observe the output files being created in the `configuretrace`, `dacapotraces`, and `nastraces` directories.  These directories also contain files xxx.err where xxx is the name of a machine, eg dahu-1.err.  This file contains the output of the various experiments and ends with "done" and the time, when the experiment completes.  Note that the .err files are overwritten when the script reboots into a different kernel.

Some results may not be available, for example if a machine was not reserved or if the reservation terminated before the experiment completed.  In this case, you can freely re-run the `run` script, without modification.  Completed experiments will not be redone.

While the configure and NAS benchmarks take only a few hours, the DaCapo benchmarks take more like a full day.  Except on the weekend, it is not possible to reserve a node for a full day, and on the weekend, some needed nodes may be reserved by others.  Thus, it is advisable to reserve machines for the amount of time available, and then continue the experiments, by running `run` again, later.

Note that the results of the DaCapo benchmarks require 34GB of disk space, which is above the default quota of 25GB on Grid 5000.  While Grid 5000 will send reminder messages, you can go up to a higher amount for up to one week.  It is best not to remove the files before the entire experiment is complete, because the scripts use the presence of the output files to determine which experiments need to be run.

The script `quickrun` is also available, which only does a few tests.  This script can be used to test the setup.  Use the `reserve` script to reserve any three machines among the dahus, trolls, and yetis for 2 hours.  For machines xxx-1 yyy-2 zzz-3, run

```
jlawall@fgrenoble:~/nest-artifact/configure-dacapo-nas/scripts$ quickrun xxx-1 yyy-2 zzz-3
```

## Making the graphs

The script `graphs` makes the graphs found in the paper, as well as some tables containing the absolute results.  This script takes as argument the name of some node that you have reserved.  Making the graphs takes around 15 minutes, so a reservation of one hour should be sufficient.  The output will be found in the `configuretrace`, `dacapotraces`, and `nastraces` directories.  These files are as follows (where xxx is configure, dacapo, and nas, respectively):

* `xxx.pdf`: An overview table of the tests with the most speedup or slowdown, followed by some graphs and then tables with the runtime, standard deviation and frequencies for each machine.
* `xxx_tables.pdf`: Tables comparing CFS and Nest for each type of machine
* `xxx_acm.pdf`: The graphs found in the paper.

Graphs can be produced at any time, even when the experiments are not yet complete.

# Manual approach

The instructions here can be used if the above instructions don't work for some reason.  The instructions assume that you are root.

## Setup

On the Grenoble frontend (or any other machine that is your starting point), set the environment variable LC_MYNAME to your username.  The experiments will be done as root, and this will enable the output to be put into your home directory on the frontend, via NFS.

## Reserve your machines

Use the `nest-artifact/configure-dacapo-nas/scripts/setup` script on the frontend to reserve the machines one by one.  Choose the `nest` image.  As noted above, you will need the following machines:

* configure: dahu-1, yeti-3, troll-1
* dacapo: dahu-1, dahu-6, dahu-14, dahu-15, yeti-2, yeti-3, troll-1
* nas: dahu-15, yeti-1, troll-4

It is suggested to work on only one or a few machines at a time, each in a separate window.  The `nest-artifact/configure-dacapo-nas/scripts/setup` script will conclude with an ssh as root onto the chosen node.

## Run the tests

Reboot, into one of the following kernels (5.9.0freq is Linux v5.9 with the CFS scheduler, 5.9.0nest is Linux v5.9 with the Nest scheduler):
```
/boot/reboot_kexec.sh 5.9.0freq
/boot/reboot_kexec.sh 5.9.0Nest
```

When the kernel has booted, the ssh connection will disconnect and return you to the frontend.  Do an `ssh` again to get back to the chosen node as root.

The following commands run the tests that are represented by the various figures in the paper for the current kernel:

#### configure:

```
run_everything /home/$LC_MYNAME/nest-artifact/configure-dacapo-nas/scripts/configure.config /home/$LC_MYNAME/nest-artifact/configure-dacapo-nas/scripts/smove_configure.config
```

#### dacapo:
```
run_everything /home/$LC_MYNAME/nest-artifact/configure-dacapo-nas/scripts/dacapo.config
```

#### nas:

```
run_everything /home/$LC_MYNAME/nest-artifact/configure-dacapo-nas/scripts/nas.config
```

The results, for the schedutil and performance governors, will be placed in your home directory, in the following subdirectories:

* configure: `configuretraces`
* dacapo: `dacapotraces`
* nas: `nastraces`

`run_everything` has some useful options if you lose your reservation or want to run some tests again:

* `--continue`: This will run any missing tests
* `--test abbrv`: This will run the test named `abbrv`.  The abbreviations for the various tests can be found by looking in the configuration file (`nest-artifact/configure-dacapo-nasscripts/configure.config`, etc.).
* `--trace-cmd-only`, `--hyperfine-only`, `--turbo-only`: Only produce a trace, only collect the running time, and only collect the energy usage, respectively.
* `--reboot`: On completion of the tests for the current kernel, this will reboot the machine into the next kernel listed in the configuration file and initiate the tests on that kernel.  The order of kernels is 5.9.0freq (ie, CFS) and the 5.9.0Nest (ie, Nest).  Thus, the `--reboot` option has no effect when the machine is already booted into 5.9.0Nest.
* `--help`: Learn about the other options.

If you are not using the Grid 5000 machines, it will be necessary to modify the `.config` scripts to add the names of the machines that you are using.  Given the naming convention of Grid 5000, providing the name `xxx` will allow running the experiment on any machine named `xxx-n`, for any number `n`. 

## Make the graphs

The graphs can be produced on any dahu, troll or yeti (not on the frontend) using the following commands (assuming that you are in the output directory of the corresponding experiment):

#### configure:
```
cleanup_turbo
make -j 20 -k -f /home/$LC_MYNAME/nest-artifact/configure-dacapo-nas/scripts/make_configure_freq
read_csvs configure.tex 5.9.0freq_schedutil *json --nomachine
```

#### dacapo: 
```
make -j 20 -k -f /home/$LC_MYNAME/nest-artifact/configure-dacapo-nas/scripts/make_dacapo_freq
read_csvs dacapo.tex 5.9.0freq_schedutil *json --nomachine
```

#### nas: 
```
read_csvs nas.tex 5.9.0freq_schedutil *json --nomachine
```

The generated pdf files are as described in the previous section.  Graphs can be produced at any time, even when the experiments are not yet complete.

Note that the frequency graphs are tuned to the frequencies found on the test machine.  This information is hard-coded for the Grid 5000 machines in `nest-artifact/image_creation/ocaml-scripts/machines.{ml,mli}`.  Some defaults are available, but you may want to adjust this information for the properties of your machine.  It should be possible to modify `i80_freq_intervals` in machines.ml. 1200000 should be replacesd with the lowest frequency on your machine.  2100000 should be replaced with the nominal frequency of your machine, and 3000000 should be replaced with the highest turbo frequency of your machine.  The other values can be chosen as you find convenient.  More lines, with more RGB colors (values between 0 and 1), can be added as needed.  You may be able to find out information about the frequencies used by your machine at `wikichip.org`.


# Instructions for replicating the phoronix benchmarks

As mentioned earlier, Grid5000 has somewhat tricky usage policies that should be respected while making the reservations. Many of the experiments are long, hence we recommend using night reservations of 14 hours and weekend reservations of 62 hours.

You can make these reservations up to 24 hours in advance.

We use dahu-2, dahu-13, dahu-18, dahu-32, yeti-1, yeti-4, troll-2, troll-3, troll-4 for the phoronix experiments. The reservation command below will succeed only if all of them are available in the requested time period. If the reservation command fails, you can check which machines are not available and try to reserve them seperately later.

## Make an advance night reservation

You can use the following command to make a night reservation (7 PM - 9 AM CET, 14 hours) for the nodes used for the Nest experiments.  Remember to change the date appropriately.  Note that it is necessary to specify both the names of the nodes and the number of nodes (9).  If the number of nodes is too small, the reservation will succeed with some subset of the nodes that were asked for:

```
hpandya@fgrenoble:~$ oarsub -t exotic -t deploy -r "2022-02-02 19:00:00" -l {"host in ('troll-2.grenoble.grid5000.fr', 'troll-3.grenoble.grid5000.fr', 'troll-4.grenoble.grid5000.fr', 'yeti-1.grenoble.grid5000.fr', 'yeti-4.grenoble.grid5000.fr', 'dahu-2.grenoble.grid5000.fr', 'dahu-13.grenoble.grid5000.fr', 'dahu-18.grenoble.grid5000.fr', 'dahu-32.grenoble.grid5000.fr')"}/nodes=9,walltime=13:55:00
```
## Make an advance reservation for the weekend

Similarly the following command can be used to make a weekend reservation (Friday 7 PM - Monday 9 AM CET, 62 hours)

```
hpandya@fgrenoble:~$ oarsub -t exotic -t deploy -r "2022-02-02 19:00:00" -l {"host in ('troll-2.grenoble.grid5000.fr', 'troll-3.grenoble.grid5000.fr', 'troll-4.grenoble.grid5000.fr', 'yeti-1.grenoble.grid5000.fr', 'yeti-4.grenoble.grid5000.fr', 'dahu-2.grenoble.grid5000.fr', 'dahu-13.grenoble.grid5000.fr', 'dahu-18.grenoble.grid5000.fr', 'dahu-32.grenoble.grid5000.fr')"}/nodes=9,walltime=61:55:00
```

## Deploy the Nest image on the nodes

After your reservation starts in the evening, the next thing is to deploy the Nest image on all the nodes. We assume that you already have the Nest environment available in your grid5000 profile (see the use of `kaenv3` above). Below is an example to deploy the Nest image on one of the dahus.

```
hpandya@fgrenoble:~$ kadeploy3 -m dahu-2 -e nest --verbose-level 4 -k
```
Once the deployment is complete, you can log-in to the machine as a root user.

## Replicating the baseline CFS results for phoronix

The nest image has a script for rebooting into the desired kernel using /sbin/kexec. It is called reboot_kexec.sh and it is located in /boot. Our experiments compare results for 5.9.0 nest schedutil with 5.9.0 cfs schedutil. Boot with 5.9.0 CFS kernel using reboot_kexec.sh script to reproduce the baseline results.

```
root@dahu-2:~$ bash /boot/reboot_kexec.sh 5.9.0freq
```

The kernel with CFS is called 5.9.0freq. We use the suffix freq because this kernel has an additional trace_printk statement to print the frequency information in arch/x86/kernel/smpboot.c.

In our experience, the reboot process is quick on grid5000 machines. So you should try logging-in again as the root user after about a minute of running the reboot script.

### Use /tmp as working directory

You should have cloned the nest-artifact git repository in your /home for the previous experiments. All necessary scripts and files for running phoronix experiments reside at  nest-artifact/phoronix-scripts/.

Please note that /home is hosted on NFS for all the grid5000 machines and hence accessing files from there is relatively slow. We performed all our experiments from /tmp as the working directory because it is hosted on the node itself so it is faster. You should run the setup_phoronix_experiment.sh to copy all necessary scripts and files to /tmp of the node that you want to run the experiment on as shown below.

```
root@dahu-2:~$ bash /home/your-user-name/nest-artifact/phoronix-scripts/grid5000/scripts/setup_phoronix_experiment.sh your-grid5000-username
```

The copy script should have created a directory /tmp/phoronix-scripts. So go to that directory. Please note that this should be your working directory for performing all the experiments.

```
root@dahu-2:~# cd /tmp/phoronix-scripts/
root@dahu-2:/tmp/phoronix-scripts#
```
### Running the phoronix experiments

The image contains all the necessary dependencies to run the phoronix benchmarks. It also has phoronix-test-suite set-up to use /tmp as the working directory.

We ran our experiments months ago, and it is quite possible that phoronix has newer versions available for some of the benchmarks. Moreover, a benchmark can have many sub-tests and not all of them might yield interesting results in terms of comparing Nest vs CFS. Hence we provide the version of phoronix benchmarks that we ran, and their configuration that runs only the interesting subtests of those benchmarks for the artifact evaluation. These benchmark files and configurations can be found at /home/hpandya/phoronix-test-suite/ . Our scripts for artifact evaluation also refers to these files and configurations. Since you have access to grid5000, you can access this directory. Feel free to check out our phoronix configuration files.

It is a good idea to check everything is set-up properly before running the long experiments. You can use the quickly_test_phoronix_experiment_setup.sh script for this purpose.

```
root@dahu-2:/tmp/phoronix-scripts$ bash quickly_test_phoronix_experiment_setup.sh your-grid5000-username
```

We used multiple machines of the same architecture for distributing the benchmarks. We suggest to follow the same benchmark-to-machine mapping for reproducing the results. Our script for running experiments takes care of this issue. You are welcome to have a look at the script, but it basically does the following things:


1. Retrives the list of benchmarks to run on the node
2. Creates necessary working directories for phoronix-test-suite in /tmp.
3. Create necessary directories to store result files on the frontend. This is important because files local on nodes will be lost when the reservation expires.
5. Identifies if the experiment is to be run with the nest kernel and uses taskset appropriately
6. Switches to schedutil power manager
7. Copies necessary benchmark files - data and configuration to /tmp
8. Performs two warm-up runs of the benchmark
9. Runs the benchmark for 10 times and copies the result file to /home



You are now all set to start running the expeiments by using the script run_phoronix_experiments.sh. The script expects three arguments - your username, name of the power manager and power status.

#### CFS schedutil

Get the results for schedutil power manager for CFS as shown below. Schedutil uses passive power status.

```
root@dahu-2:/tmp/phoronix-scripts$ bash run_phoronix_experiments.sh your-grid5000-username schedutil passive
```

#### Rodinia-1.3.1

Please note, that one of the phoronix benchmarks -  rodinia sometimes misbehaves when you try to run it with our scripts. The reason behind it is that there is a newer version available for this benchmark and phoronix is enthusiastic about using the newer version. You might get an error saying "Multiple tests are not installed". In this case try re-installing the older version of the benchmark.
```
root@dahu-2:/tmp/phoroix-scripts$ phoronix-test-suite install rodinia-1.3.1
```
Phoronix might still be enthusiastic to get you the latest version, so please test you have the correct version by running the benchmark.
```
root@dahu-2:/tmp/phoroix-scripts$ phoronix-test-suite run rodinia-1.3.1
```
If you managed to install the correct version, that it should refer to the test-profile copied earlier by our script before it threw the error. Our profile runs only one test. The latest profile has 5 tests in rodinia. So if you see something like "Running Test 1 of 5", it is the wrong version, and you should try installing of version 1.3.1 again.

In our experince, phoronix gets motivated to use the older 1.3.1 version after second attempt of installation.

#### Ffmpeg- 2.8.0 and yeti

Nother thing to note is that the ffmpeg benchmark fails to run on yeti as the number of threads generated by the benchmarks are not in proportion with the number of cored on this machine. The benchmark runs fine on the other machines like dahu and troll.

#### CFS performance

Next get the results for performance power manager with active power status for CFS as shown below.

```
root@dahu-2:/tmp/phoronix-scripts$ bash run_phoronix_experiments.sh your-grid5000-username performance active
```

The script also records the standard output from the phoronix benchmark in a file and copies the file to your /home after each experiment. So if something goes wrong with the benchmark, you can find the terminal output stored in a file at /home/your-user-name/phoronix-results/kernel-name/terminal_output/.

## Replicating the Nest results for phoronix

After getting the baseline results with 5.9.0freq kernel, boot into 5.9.0Nest kernel using the reboot script.

```
root@dahu-2:~$ bash /boot/reboot_kexec.sh 5.9.0Nest
```
Run the same benchmarks with the Nest kernel.
```
root@dahu-2:/tmp/phoronix-scripts$ bash run_phoronix_experiments.sh your-grid5000-username schedutil passive
```

## Analyzing the Results

Once done with all experiments for both the kernels on all the machines, use the read_csvs tool on any node to generate a pdf containing all results and comparisons. Please note that you should run the tool from results directory in /home to get the tables stored on nfs. This way the tables will remain available after the node reservation expires.

```
root@dahu-2:/home/your-user-name/phoronix-results$ read_csvs phoronix_results.tex  5.9.0freq_schedutil 5.9.0freq/results/*csv 5.9.0Nest/results/*csv --nomachine
```

You should now have file phoronix_results.pdf, phoronix_results_tables.pdf and phoronix_results_acm.tex in your working directory.  These files are described above in the "Making the graphs" section.

Please note, Our performance graphs only show interesting results for phoronix. So we removed all unpredictable and uninteresting results from the files before making the graphs. You should check that there are no additional results present than the ones shown in the paper before running read_csvs for phoronix results.




