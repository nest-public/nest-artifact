#! /bin/bash

[ "$1" != "" ] || { echo Error; exit 1; }

version="$1"

# See /proc/cmdline

/sbin/kexec -l /boot/vmlinuz-$version --initrd=/boot/initrd.img-$version --command-line="BOOT_IMAGE=/boot/vmlinuz-$version `cat /proc/cmdline` ro quiet"

/sbin/kexec -e
