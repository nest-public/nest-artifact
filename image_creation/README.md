# Nest environment setup

This document explains how to set up everything need to run the Nest experiments when starting from scratch from a vanilla **Debian 11** image.  To exactly replicate the Nest experiment results, we do not suggest doing this, but rather using the Grid 5000 image.

This document mostly focuses on how to replicate the experiments in the paper using the associated testing scripts.  If you would like to use Nest on your own please see the end of the document.

## Required Debian packages

A list of required Debian packages is available in the file `nest-artifact/image_creation/debian11_packages`.  These packages may be installed with

```
sudo xargs -a debian11_packages apt install
```

It may be necessary to do `apt update` and then `apt upgrade`.  The file `debian11_list` is also provided that contains the version numbers of the installed software.

The DaCapo benchmarks have been validated to work with Java 11.  This is the version of Java that is selected in the image.  If you are replicating the experiments in another Debian 11 installation, it may be necessary to run `sudo update-alternatives --config java`. [This website](https://linuxize.com/post/install-java-on-debian-10/) may be helpful.

## Other remote software

The Debian package for `trace-cmd` may be out of date.  We thus suggest installing it from sources as follows:

```
git clone https://git.kernel.org/pub/scm/libs/libtrace/libtraceevent.git
pushd libtraceevent
git checkout d0fbd046a090834ac1ee69d45f321d7fa2a5c9b4
make
sudo make install
popd

git clone https://git.kernel.org/pub/scm/libs/libtrace/libtracefs.git
pushd libtracefs
git checkout 351e998302a1bb9616816efd79f086cc19ea4d42
make
sudo make install
popd

git clone https://github.com/rostedt/trace-cmd.git
pushd trace-cmd
git checkout 196a3a63c466204f3e8f4fa35194fc9ab7cb1ab7
make
sudo make install
popd
```

To install hyperfine (used for the configure, DaCapo, and NAS benchmarks):

```
wget https://github.com/sharkdp/hyperfine/releases/download/v1.12.0/hyperfine_1.12.0_amd64.deb
sudo dpkg -i hyperfine_1.12.0_amd64.deb
```

## Local software

We offer a number of tools for running benchmarks and making graphs.  Go to `image_creation/ocaml-scripts` and run:

```
make
sudo make install
```

## The Linux kernel

The Linux kernel source code can be obtained as follows:

```
git clone https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
````

Three patches for the Linux kernel v5.9 are available:

* freq.patch: This extends Linux v5.9 with a print statement (`trace_printk`) printing the frequency on each core on each clock tick.  This is required to obtain the frequency graphs shown in the paper.
* nest.patch: This extends Linux v5.9 with the code for the Nest scheduler.
* smoveoriginal.patch: This extends Linux v5.9 with the Smove scheduler, published at USENIX ATC 2020.  The patch is also available [here](https://gitlab.inria.fr/whisper-public/atc20)
* grid5000_config.patch: This is the .config file that has been used on Grid 5000 and may be useful as a reference.

These kernels can be created as follows, using nest-artifact/image_creation/freq.patch as an example

```
git checkout v5.9
git checkout -b v5.9freq
patch -p1 < /home/$LC_MYNAME/nest-artifact/image_creation/freq.patch
git add -f .scmversion
git commit -m freq -a
make olddefconfig
```
At this point, you should modify your configuration file to replace

```
# CONFIG_LOCALVERSION_AUTO is not set
CONFIG_BUILD_SALT="5.10.0-10-amd64"
```

with

```
CONFIG_LOCALVERSION_AUTO=y
CONFIG_BUILD_SALT=""
```
and


```
CONFIG_SYSTEM_TRUSTED_KEYS="debian/certs/debian-uefi-certs.pem"
```
with

```
CONFIG_SYSTEM_TRUSTED_KEYS=""
```



These changes are only necessary once; afterwards the same .config will be reused.  Alternatively, nest-artifact/image_creation/grid5000_config.patch contains a file `grid5000_config` that may be a useful starting point.

Next, compile and install your kernel.

```
make -j `nproc` && INSTALL_MOD_STRIP=1 make -j`nproc` modules_install && make install
```

Your kernel should then be available in `/boot/vmlinuz-5.9.0freq`.

Set up the reboot script:

```
sudo cp /home/$LC_MYNAME/nest-artifact/image_creation/reboot_kexec.sh /boot
chmod +x /boot/reboot_kexec.sh
```

To reboot into eg 5.9.0freq, run the above script with the argument `5.9.0freq`.

Now that your environment is set up, you can return to the "Manual approach" section of the top-level `README.md` file, skipping the "Reserve your machines" section.

## Instructions for running the phoronix benchmarks

The Nest paper peresents results for the following benchmarks with phoronix test suite version 10.4.0.
* askap-2.1.0
* arrayfire-1.1.0
* avifenc-1.1.1
* cassandra-1.1.1
* compress-zstd-1.5.0
* cpuminer-opt-1.4.0
* ffmpeg-2.8.0
* graphics-magick-2.0.2
* libgav1-1.1.0
* oidn-1.4.0
* onednn-1.7.0
* rodinia-1.3.1

### Installation and Versions

You can download and install phoronix-test-suite 10.4.0 from https://github.com/phoronix-test-suite/phoronix-test-suite/tree/v10.4.0.

Once you have the phoronix-test-suite installed, you can install the benchmarks as shown in the example below.
```
phoronix-test-suite install cpuminer-opt-1.4.0
```


### Batch mode

In the interactive mode, Phoronix will ask you questions like if you want to save results of this experiment, name of the resultfile, description and if you want to upload the test results to openbenchmarking.com. Phoronix will run the benchamark 3 times by default. It will run it more number of times if the standard deviation is not below the threashold after running it for 3 times. If you choose to save the results, you will find them in /home/username/.phoronix-test-suite/test-results.

We run experiments for the Nest paper using phoronix's batch mode. We run each experiment 10 times by using FORCE_TIMES_TO_RUN environment variable. We also did 2 warm-up runs before each experiment. One needs to configure the batch-mode before using it. We configured it to run all available tests in the benchmark and store the results for all experiments. Use the following command to configure the batch mode.
```
phoronix-test-suite batch-setup
```

### Baseline results

You would want to compare nest schedutil results with cfs schedutil and cfs performance results. And hence you will need to run experiments once with each kernel.

#### CFS schedutil

Boot with 5.9.0freq kernel using the reboot_excec.sh script.
```
sudo bash /boot/reboot_kexcec.sh 5.9.0freq
```
Select the schedutil power governor using the change_powerstatus.sh and change_powermanager.sh scripts available in nest-artifact/phoronix-scripts/grid5000/scripts/
```
sudo bash change_powerstatus.sh passive
sudo bash change_powermanager.sh schedutil
```
Perform the two warm-up runs as shown below:
```
FORCE_TIMES_TO_RUN=2  phoronix-test-suite batch-run cpuminer-opt-1.4.0
```
Get rid of the results from the warm-up runs by removing the newest directory in /home/username/.phoronix-test-suite/test-results/. It is important to do so in order to get the result csv file containing only the next 10 runs for the real experiment.

Next, run the benchmark for 10 times using the batch mode.
```
FORCE_TIMES_TO_RUN=10 phoronix-test-suite batch-run cpuminer-opt-1.4.0
```
On successful completion of the experiment, the newest directory in /home/username/.phoronix-test-suite/test-results/ should now contain the results of this experiment.
```
username@machine:~$ ls -t /home/username/.phoronix-test-suite/test-results/ | head -1
2022-02-02-1013
```
You can look at the overview.svg file for the result at /home/username/.phoronix-test-suite/test-results/2022-02-02-1013/result-graphs/overview.svg. You can also get a csv file using the following command:
```
phoronix-test-suite result-file-raw-to-csv 2022-02-02-1013
```
Now you should have 2022-02-02-1013-raw.csv in your /home/username/. 

It is a good idea to give resultfiles meaningful and descriptive names. Our tool to analyze results expects resultfile names in a particular format. So rename the resultfile to match the format.
```
mv 2022-02-02-1013-raw.csv resultfile_cpuminer-1.4.0_5.9.0freq_schedutil_${HOSTNAME}_10runs.csv
```

#### CFS Performance

Assuming that you are still running the 5.9.0freq kernel, change the power governor to performance.
```
sudo bash change_powerstatus.sh active
sudo bash change_powermanager.sh performance
```
Perform the two warm-up runs, get rid of the results of warm-up runs and then perform the 10 runs as described earlier. Rename the result file appropriately. In the end you should have resultfile_cpuminer-1.4.0_5.9.0freq_schedutil_machinename_10runs.csv in your /home/username/.

#### Nest Schedutil

Boot with the nest kernel.
```
sudo bash /boot/reboot_kexcec.sh 5.9.0Nest
```
Select the schedutil power governor.
```
sudo bash change_powerstatus.sh passive
sudo bash change_powermanager.sh schedutil
```
Perform the two warm-up runs, get rid of the results of warm-up runs and then perform the 10 runs as described earlier. Rename the result file appropriately. In the end you should have resultfile_cpuminer-1.4.0_5.9.0nest_schedutil_machinename_10runs.csv in your /home/username/.

#### Interesting tests

Due to space constrains we present only interesting results for these benchmarks in the paper. For example, cpuminer-1.4.0 has total 11 tests with different algorithms. But we obsereved significant difference in performance between CFS schedutil and Nest schedutil only for 5 of them. So we present results for only those 5 tests called - Blake 2S, Myriad Groestl, Quad SHA256 Pyrite, Skeincoin and Triple SHA256 Onecoin. If you want to run just those, you can get rid of the entries for the other tests in /home/username/.phoronix-test-suite/test-profiles/pts/cpuminer-opt-1.4.0/test-definition.xml.

Or you can replace your test-profiles/pts/cpuminer-opt-1.4.0/ with the one given in nest-artifact/phoronix-scripts/external/test-profiles/cpuminer-opt-1.4.0/.

Follow the same steps for running all the phoronix benchmarks. If you would like to automate the process, you can have a look at nest-artifact/phoronix-scripts/grid5000/scripts/run_phoronix_experiment.sh which puts together all the above steps. Please note that this scrip assumes running phoronix benchmarks as the root, while above steps are for a non-root user.

#### Result analysis

Assuming you have compiled the read_csvs tool available in nest-artifacts/image-creation/ocaml-scripts, and you have all your result files in /home/username/phoronix-results/results/, use the read_csvs to make tables for result analysis.
```
/phoronix-results$ /home/username/nest-artifact/image_creation/ocaml-scripts/read_csvs phoronix_results.tex  5.9.0freq_schedutil results/*.csv --nomachine
```
You should now have file phoronix_results.pdf, phoronix_results_tables.pdf and phoronix_results_acm.tex in your phoronix-results directory. 

## Using Nest independent of the test scripts

Installing the Nest kernel triggers the use of wakeup work conservation, but not the complete Nest policy.  To run an application, App, with the Nest policy it is necessary to wrap the invocation of the application in a taskset that covers the set of cores on the machine.  For example, if the highest numbered core on the machine is 159, then you would run:

```
taskset -c 0-159 App
```

On the Intel servers used in the evaluation, the number of the highest numbered core is one less than the number of cores on the machine.  After the initial submission, we discovered that this is not always the case - on a 20-core AMD Ryzen 5, we found gaps in the core numbering.  So it may be necessary to print the value of cpu_possible_mask in

```
SYSCALL_DEFINE3(sched_setaffinity, pid_t, pid, unsigned int, len, unsigned long __user *, user_mask_ptr)
```

to figure out what the value should be.
