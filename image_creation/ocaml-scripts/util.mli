(* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * file license.txt for more details.
 *)

val cmd_to_list : string -> string list
val hashadd : ('a,'b list ref) Hashtbl.t -> 'a -> 'b -> unit
val hashadd_set : ('a,'b list ref) Hashtbl.t -> 'a -> 'b -> unit
val hashadd_unique : ('a,'b list ref) Hashtbl.t -> 'a -> 'b -> unit
val hashinc : ('a,int ref) Hashtbl.t -> 'a -> int -> unit
val hashincf : ('a,float ref) Hashtbl.t -> 'a -> float -> unit

val mean : float list -> float
val geomean : float list -> float
val stddev : float list -> float
val geosd : float list -> float
val pearson : (float * float) list -> float
val speedup_and_std : bool -> float -> float list -> float * float

val get_front : char -> string -> string

type file_context = string * bool * bool

val skip_front_continue : file_context -> int * float * in_channel
val skip_front : file_context -> int * float

val get_files :
    string option ->
      string list -> (string * float * float) -> bool -> bool -> string list ->
	(string * file_context * bool) list

val get_endpoint :
    (string * file_context * bool) list -> (string * float * float) ->
      (file_context * float) list option * (string * float * float)

val parse_loop :
    file_context -> float -> float -> string list -> 'a ->
      (float -> int -> 'a -> float -> Parse_line.events -> unit) ->
	(bool * float * float * 'a)

val remove : file_context -> unit

val choose_target : string list ref -> string list -> unit

val take : int -> 'a list -> 'a list (* first n elements *)

val mask2cores : string -> int list

val matches : string -> bool ref -> string -> bool

val interpret_show_commands :
    string list -> file_context -> float -> float ->
      (float * float * string * int list * int) list
