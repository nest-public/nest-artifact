(* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * file license.txt for more details.
 *)

let make_do = ref false

type input = RE of string | PHORONIX of string * string

type csv_data = { command : string; app : string option; runtime : float; std : float; values : float list; lower_is_better : bool; position : int; partial : int option; high_std : bool; csv_file : string }

type auc_data = { auc : float; aoc : float; aoc2 : float; window : int; running : int; other : int; auc_file : string }

type perf_data = { event : string; evavg : float; evstd : float; evvalues : float list; perf_file : string }

type freq_data = { low : int;  high : int;  count : float;  pct : float; freq_file : string }

type turbo_data = { cpu_joules_avg : float; cpu_joules_std : float; cpu_joules_values : float list;
		    ram_joules_avg : float; ram_joules_std : float; ram_joules_values : float list;
		    runs : int; turbo_file : string }

let mkphoronixtrace s =
  match Str.split (Str.regexp "/results/") s with
    [bef;aft] -> Printf.sprintf "%s/plots/%s" bef aft
  | _ -> Filename.basename s

(* ---------------------------------------------------------------- *)

let comma_split s = (* check for quotes *)
  let s1 = Str.full_split (Str.regexp_string "\"") s in
  let rec loop acc = function
      [(Str.Text s)] ->
	let comma = Str.split_delim (Str.regexp ",") s in
	(match comma with
	  a::b -> (String.concat "" (List.rev (a::acc))) :: b
	| [] -> failwith "not possible")
    | (Str.Text s)::rest -> loop (s :: acc) rest
    | (Str.Delim _)::(Str.Text a)::(Str.Delim _)::b ->
	loop ((Printf.sprintf "\"%s\"" a) :: acc) b
    | (Str.Delim _)::(Str.Delim _)::b ->
	loop ("\"\"" :: acc) b
    | [] ->
	if acc = []
	then []
	else [String.concat "" (List.rev acc)]
    | _ -> failwith "unexpected thing" in
  loop [] s1

let fos from s =
  try float_of_string s
  with e ->
    Printf.eprintf "float of string: %s: %s\n" from s;
    raise e

let reclean s =
  let s = Str.split (Str.regexp "> /dev/null") s in
  let s = String.concat "" s in
  let s = Str.split (Str.regexp "2>&1") s in
  let s = String.concat "" s in
  let s = Str.split (Str.regexp " >> /tmp/bug_report[0-9a-f]+.txt") s in
  let s = String.concat "" s in
  let s = Str.split (Str.regexp " +") s in
  String.concat " " s

(* ---------------------------------------------------------------- *)

let unquote s =
  String.trim(String.concat "" (Str.split (Str.regexp "\"") s))

let uncomma s =
  let pieces = Str.split_delim (Str.regexp ",") s in
  match List.rev pieces with
    [x] -> s
  | _::rest -> String.concat "," (List.rev rest)
  | [] -> s

let unpair str s =
  match Str.bounded_split (Str.regexp ": ") s 2 with
    [bef;aft] -> (unquote bef,unquote(uncomma aft))
  | _ -> failwith (Printf.sprintf "%s: not possible: %s" str s)

let phoronix_cut_at_results l =
  let isresults s =
    try
      ignore(Str.search_forward (Str.regexp_string "Results Are") s 0);
      true
    with Not_found -> false in
  let rec loop acc = function
      x::y::xs when isresults y ->
	(List.rev acc) :: loop [y;x] xs
    | x::xs -> loop (x::acc) xs
    | [] -> [List.rev acc] in
  loop [] l

let get_csv_data base appstr isrefos =
  match base with
    RE base ->
      let file = base^".json" in
      if Sys.file_exists file
      then
	let lines = Util.cmd_to_list ("cat "^file) in
	let lines = List.filter (fun x -> x <> "") lines in (* temporary problem *)
	(match lines with
	  _::_::_::c::m::s::_::_::_::_::_::t::rest ->
	    let (c,cmd) = unpair "command" c in
	    let (m,mn) = unpair "mean" m in
	    let (s,sv) = unpair "standard deviation" s in
	    let (t,_) = unpair "times" t in
	    (if not(c = "command" && m = "mean" && s = "stddev" && t = "times")
	    then failwith "bad json");
	    let rec loop = function
		x::xs ->
		  let x = String.trim x in
		  if x = "]" || x = "],"
		  then []
		  else (fos "tm" (uncomma x)) :: loop xs
	      | _ -> failwith "bad times" in
	    let runtime = fos "mn" mn in
	    let std = fos "sv" sv in
	    Some { command = reclean cmd; app = None; runtime = runtime; std = std; values = loop rest; lower_is_better = true; position = 0; partial = None; high_std = false;
		   csv_file = file } (* hack for high_std! *)
	| _ -> None)
      else None
  | PHORONIX (base,_) ->
      let file = base^".csv" in
      if Sys.file_exists file
      then
	begin
	  let lines = Util.cmd_to_list ("cat "^file) in
	  if lines <> [] (* interrupted file *)
	  then
	    begin
	      (* hd of pieces is the date and a blank line *)
	      let pieces = List.tl(phoronix_cut_at_results lines) in
	      let rec loop i = function
		  [] ->
		    Printf.eprintf "file: %s is missing %s\n" file appstr;
		    None
		| bench::rest ->
		    match bench with
		      cmd::dir::_::infos ->
			let cmd = unquote(uncomma cmd) in
			if cmd = appstr
			then
			  let lower_is_better =
			    match Str.split (Str.regexp " ") dir with
			      "Higher"::_ -> false
			    | "Lower"::_ -> true
			    | _ -> failwith "unknown value to favor" in
			  let infos = List.filter (fun x -> x <> "") infos in
			  let rec line = function
			      x::xs ->
				(match Str.split (Str.regexp_string "\",") x with
				  [_;data] -> data
				| _ -> line xs)
			    | [] -> "" in
			  (if List.length infos > 1 then Printf.eprintf "%s: %d result lines\n" file (List.length infos));
			  let data = line infos in
			  let pieces = Str.split (Str.regexp ",") data in
			  let values = List.map float_of_string pieces in
			  let vallen = List.length values in
			  (if vallen != 10 then Printf.eprintf "%s: %d\n" file vallen);
			  if values = []
			  then None
			  else
			    begin
			      let partial = if vallen = 10 then None else Some vallen in
			      let runtime = Util.mean values in
			      let std = Util.stddev values in
			      let high_std = isrefos && (std /. runtime) > 0.15 in
			      (if high_std then Printf.eprintf "high std (%d\\%%) for %s\n" (int_of_float((std /. runtime) *. 100.)) cmd);
			      Some { command = cmd; app = None; runtime = runtime; std = std; values = values; lower_is_better = lower_is_better; position = i; partial = partial; high_std = high_std;
				     csv_file = file }
			    end
			else loop (i+1) rest
		    | _ -> failwith "bad data" in
	      loop 0 pieces
	    end
	  else None
	end
      else None

let get_perf_data base n =
  match base with
    RE base ->
      let file = base^".perf" in
      if Sys.file_exists file
      then
	match Util.cmd_to_list ("cat "^file) with
	  [] -> []
	| _::lines ->
	    List.rev
	      (List.fold_left
		 (fun prev line ->
		   match Str.split (Str.regexp ",") line with
		     "info"::_ -> prev (* hack, avoid a bug *)
		   | name::avg::std::_::_::values ->
		       let avg = float_of_string avg in
		       let std = float_of_string std in
		       let values = List.map float_of_string values in
		       { event = name; evavg = avg; evstd = std;
			 evvalues = values; perf_file = file } :: prev
		   | _ -> failwith "bad lines")
	      [] lines)
      else []
  | PHORONIX (resbase,tracebase) -> []

let reported = ref []

let file_exists dat file =
  (* if the .dat file doesn't exist, then there is not much point *)
  if Sys.file_exists dat
  then
    begin
      let cmd = Printf.sprintf "ls %s 2> /dev/null" file in
      let options = Util.cmd_to_list cmd in
      let get_time file =
	let ls =
	  List.hd
	    (Util.cmd_to_list
	       (Printf.sprintf "ls --time-style=+%%s -g %s" file)) in
	match List.rev(Str.split (Str.regexp " ") ls) with
	  _file::date::_ -> int_of_string date
	| _ -> failwith "bad ls output" in
      let dat_date = get_time dat in
      let options =
	List.filter
	  (function fl ->
	    if get_time fl > dat_date
	    then true
	    else
	      begin
		(if !make_do
		then
		  begin
		    Printf.eprintf "file %s is out of date: drop? " fl;
		    flush stderr;
		    if List.mem (input_line stdin) ["y";"yes"]
		    then Sys.remove fl
		  end);
		false
	      end)
	  options in
      match options with
	[] -> None
      | [x] -> Some x
      | l ->
	  failwith
	    (Printf.sprintf "ambiguous for %s: %s" file (String.concat " " l))
    end
  else
    begin
      (if !make_do && not (List.mem dat !reported)
      then
	begin
	  (*Printf.eprintf "missing .dat file: %s\n" dat;*)
	  reported := dat :: !reported
	end);
      None
    end

let tried = ref []

let rerun cmd args base datafile =
  let infos = base^".dat" in
  let cmd =
    Printf.sprintf "%s/%s" (Filename.dirname(Array.get Sys.argv 0)) cmd in
  if Sys.file_exists cmd && not !make_do && Sys.file_exists infos && not (List.mem datafile !tried)
  then
    begin
      tried := datafile :: !tried;
      let doit _ =
	let cmd =
	  Printf.sprintf "%s --nograph %s %s"
	    cmd args infos in
	ignore(Sys.command cmd) in
      match file_exists infos datafile with
	None -> doit()
      | Some datafile -> ()
    end

let get_auc_data base n show =
  match base with
    RE base ->
      let file = base^"_1.conc" in
      let arg = "--forked --after-sleep" in
      rerun "count_concurrency2" arg (base^"_1") file;
      if Sys.file_exists file
      then
	let lines = Util.cmd_to_list ("cat "^file) in
	let (auc_aoc,n) =
	  if List.length lines >= 2
	  then
	    let l1 = Str.split (Str.regexp " ") (List.nth lines 0) in
	    let l2 = Str.split (Str.regexp " ") (List.nth lines 1) in
	    let l3 = try Str.split (Str.regexp " ") (List.nth lines 2) with _ -> [] in
	    match (l1,l2,l3) with
	      (["auc";auc],["aoc";aoc],["aoc2";aoc2]) -> (Some (fos "auc" auc, fos "aoc" aoc, fos "aoc2" aoc2),3)
	    | (["auc";auc],["aoc";aoc],_) -> (Some (fos "auc" auc, fos "aoc" aoc, -1.),2)
	    | _ -> failwith "bad auc line"
	  else (None,2) in
	let win_other =
	  if List.length lines >= n+4
	  then
	    let l1 = Str.split (Str.regexp " ") (List.nth lines (n+2)) in
	    let l2 = Str.split (Str.regexp " ") (List.nth lines (n+3)) in
	    let l3 = Str.split (Str.regexp " ") (List.nth lines (n+4)) in
	    match (l1,l2,l3) with
	      (["window:";win],["running:";running],["other:";other]) ->
		Some (int_of_string win, int_of_string running, int_of_string other)
	    | _ -> failwith ("bad winvis line: "^file)
	  else None in
	match (auc_aoc,win_other) with
	  (None,_) -> None
	| (Some(auc,aoc,aoc2),None) ->
	    let aoc = if aoc < 0.0 then -1. *. aoc else aoc in
	    let aoc2 = if aoc2 < 0.0 then -1. *. aoc2 else aoc2 in
	    Some { auc = auc; aoc = aoc; aoc2 = aoc2; window = -1; running = -1; other = -1; auc_file = file }
	| (Some(auc,aoc,aoc2),Some(win,run,other)) ->
	    let aoc = if aoc < 0.0 then -1. *. aoc else aoc in
	    let aoc2 = if aoc2 < 0.0 then -1. *. aoc2 else aoc2 in
	    Some { auc = auc; aoc = aoc; aoc2 = aoc2; window = win; running = run; other = other; auc_file = file }
      else None
  | PHORONIX (resbase,tracebase) ->
	(*let arg = Printf.sprintf "--forked --show %s" app in *)
      None

let get_freq_data base n show =
  let interpret_freq dat file =
    match file_exists dat file with
      None -> None
    | Some file ->
	let lines = Util.cmd_to_list ("cat "^file) in
	if List.length lines >= 2
	then
	  Some
	    (List.sort compare
	       (List.map
		  (function l ->
		    match Str.split (Str.regexp ",") l with
		      [low;high;count;pct] ->
			{ low = int_of_string low; high = int_of_string high; count = float_of_string count; pct = float_of_string pct; freq_file = file }
		    | _ -> failwith "bad freq line")
		  (List.tl lines)))
	else None in
  match base with
    RE base ->
      let file = base^"_1*tmp*.freq" in
      let arg = "-q --freq --show tmp --after-sleep" in
      rerun "dat2graph" arg (base^"_1") file;
      interpret_freq (base^"_1.dat") file
  | PHORONIX (resbase,tracebase) ->
      if show = ""
      then None
      else
	let file = Printf.sprintf "%s_*_0.freq" tracebase in
	let arg = "-q --freq --show-list "^show in
	rerun "dat2graph" arg tracebase file;
	let file = Printf.sprintf "%s_*_%d.freq" tracebase n in
	interpret_freq (tracebase ^ ".dat") file

let get_event_data base n =
  match base with
    RE base ->
      let file = base^"_1.events" in
      let arg = "" in
      rerun "events --after-sleep" arg (base^"_1") file;
      if Sys.file_exists file
      then
	let lines = Util.cmd_to_list ("cat "^file) in
	if List.length lines >= 2
	then
	  Some
	    (List.sort compare
	       (List.map
		  (function l ->
		    match List.rev(Str.split (Str.regexp " (") l) with
		      vl::event ->
			let event = String.concat " (" (List.rev event) in
			let vl = int_of_string(List.hd (Str.split (Str.regexp_string ")") vl)) in
			(event,vl)
		    | _ -> failwith ("bad event line: "^l))
		  lines))
	else None
      else None
  | PHORONIX (resbase,tracebase) ->
      (*let arg = Printf.sprintf "--show %s" app in*)
      None

let get_mig_data base n = None (*
  match base with
    RE base ->
      let file = base^"_1.mig" in
      let arg = "-q --print-everything --migrate-stats --after-sleep" in
      rerun "dat2graph" arg (base^"_1") file;
      if Sys.file_exists file
      then
	let lines = Util.cmd_to_list ("cat "^file) in
	if List.length lines = 3
	then
	  Some
	    (List.sort compare
	       (List.map
		  (function l ->
		    match List.rev(Str.split (Str.regexp " ") l) with
		      vl::event ->
			let event = String.concat " " (List.rev event) in
			(event,int_of_string vl)
		    | _ -> failwith ("bad event line: "^l))
		  lines))
	else None
      else None
  | PHORONIX (resbase,tracebase) ->
      (*let arg = -> Printf.sprintf "--migrate-stats --show %s" app in *)
      None *)

type dac = JSON | TURBO | BOTH

let get_dacapo_stderr base cmd data =
  match base with
    RE base ->
      let file =
	match data with
	  JSON -> base^".json.stderr"
	| TURBO -> base^"_0.turbo.stderr"
	| BOTH -> base^"*.{json,turbo}.stderr" in
      let files =
	let cmd = Printf.sprintf "ls %s 2> /dev/null" file in
	Util.cmd_to_list cmd in
      let values =
	List.concat
	  (List.map
	     (function file ->
	       let lines = Util.cmd_to_list(Printf.sprintf "grep PASSED %s" file) in
	       List.map
		 (function s ->
		   match List.rev(Str.split (Str.regexp " ") s) with
		     _::_::time::_ -> float_of_string time
		   | _ -> failwith "bas passed line")
		 lines)
	     files) in
      if values = []
      then None
      else Some { command = cmd^"_last"; app = None; runtime = Util.mean values; std = Util.stddev values; values = values; lower_is_better = true; position = 0; partial = None; high_std = false;
		  csv_file = base } (* again a hack *)
  | _ -> None

(* -------------------------------------------------------------- *)
(* expects output of turbostat --quiet --out file ./application *)

let get_turbo_data base =
  match base with
    RE base ->
      let file = base^".turbostat" in
      if Sys.file_exists file
      then
	let values = List.tl (Util.cmd_to_list ("cat "^file)) in
	(if List.length values < 30
	then Printf.eprintf "%s: expected at least 30 turbostat runs\n" file);
	let values =
	  List.map
	    (fun x ->
	      match Str.split (Str.regexp ",") x with
		[cpu;ram] -> (float_of_string cpu,float_of_string ram)
	      | _ -> failwith "bad turbo values")
	    values in
	let (pkg,ram) = List.split values in
	Some { cpu_joules_avg = Util.mean pkg;
	       cpu_joules_std = Util.stddev pkg;
	       cpu_joules_values = pkg;
	       ram_joules_avg = Util.mean ram;
	       ram_joules_std = Util.stddev ram;
	       ram_joules_values = ram;
	       runs = List.length values;
	       turbo_file = file }
      else
	begin
	  (*if Sys.file_exists (base^".json")
	  then Printf.eprintf "missing %s\n" file;*)
	  None
	end
  | PHORONIX (resbase,tracebase) -> None
