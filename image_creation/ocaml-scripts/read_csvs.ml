(* Collect various information into graphs.
Required parmap and rubber. *)

(* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * file license.txt for more details.
 *)

(*
app: abbreviation * index * string representation
ok: os name * governor name
*)

module PL = Print_latex

(*let machines = ["dahu"]*)
let machines = []
(*let geomean = ref false*)
let nomachine = ref false
let quick = ref false
let forcemap = ref false
let debug = ref false
let production = ref ""
let show = ref ""
let acm_only = ref false
let flip = ref false
let winlose = ref false
let summary_threshold = ref 0.05
let toignore = ref []
let do_replace50 = ref true
let show_ram = ref false
let show_over_under = ref false

let drop_machine s =
  let rec loop = function
      m::ms ->
	(match Str.bounded_split (Str.regexp m) s 2 with
	  [a;b] -> a
	| _ -> loop ms)
    | _ -> s in
  loop machines

(*\\definecolor{gr}{rgb}{0.09,0.45,0.27}*)

let pack_of_four l mx =
  let rec loop cur acc n = function
      [] ->
	if cur = []
	then List.rev acc
	else List.rev ((List.rev cur) :: acc)
    | x::xs ->
	if n = mx
	then loop [] ((List.rev cur) :: acc) 0 (x::xs)
	else loop (x::cur) acc (n+1) xs in
  loop [] [] 0 l

let pct v = (v -. 1.) *. 100.

(* ------------------------------------------------------------------ *)

let drop_number bef =
  let number s = try ignore(int_of_string s); true with _ -> false in
  match List.rev (Str.split (Str.regexp_string "-") bef) with
    [x] -> x
  | num::rest ->
      if number num
      then String.concat "-" (List.rev rest)
      else bef
  | [] -> failwith "bad machine name"

let machine_string s =
  try
    let s = drop_number s in
    List.assoc s
      [("dahu","64-core Intel 6130");("yeti","128-core Intel 6130");("troll","64-core Intel 5218");
	("w2155","20-core Intel W-2155");("i80","160-core Intel E7-8870 v4");("4650g","12-core AMD 4650g")]
  with _-> s

let machine_to_title machine =
  Printf.sprintf ",title={%s},title style={align=left,at={(0.000000,0.9)},anchor=south west},"
    (machine_string machine)

let used_os_strings = ref []

let os_string os_gov =
  let res =
    try
      List.assoc os_gov
	[(("5.9.0freq","schedutil"),"CFS schedutil");(("5.9.0freq","performance"),"CFS performance");
	  (("5.9.0cfs","schedutil"),"CFS schedutil");(("5.9.0cfs","performance"),"CFS performance");
	  (("5.9.0retry","schedutil"),"\\snest{}-old schedutil");(("5.9.0retry","performance"),"\\snest{}-old performance");
	  (("5.9.0retryfixed","schedutil"),"\\snest{} schedutil");(("5.9.0retryfixed","performance"),"\\snest{} performance");
	  (("5.9.0Nest","schedutil"),"\\snest{} schedutil");(("5.9.0Nest","performance"),"\\snest{} performance");
	  (("5.9.0nest","schedutil"),"\\snest{} schedutil");(("5.9.0nest","performance"),"\\snest{} performance");
	  (("5.9.0gc","schedutil"),"\\snest{} schedutil");(("5.9.0gc","performance"),"\\snest{} performance");
	  (("5.9.0gcnospin","schedutil"),"\\snest{} nospin schedutil");(("5.9.0gcnospin","performance"),"\\snest{} nospin performance");
	  (("5.9.0smovefixed","schedutil"),"Smove schedutil");(("5.9.0smovefixed","performance"),"Smove performance");
	  (("5.9.0smoveoriginal","schedutil"),"Smove schedutil");(("5.9.0smoveoriginal","performance"),"Smove performance");
	  (("5.9.0smoveoriginalzero","schedutil"),"Smove0 schedutil");(("5.9.0smoveoriginalzero","performance"),"Smove0 performance")]
    with Not_found -> (fst os_gov ^ " " ^ snd os_gov) in
  (if not(List.mem (res,os_gov) !used_os_strings)
  then used_os_strings := (res,os_gov) :: !used_os_strings);
  res

let os_string2 os_gov =
  let s = os_string os_gov in
  let pieces = List.rev(Str.split (Str.regexp " ") s) in
  (String.concat " " (List.rev (List.tl pieces)), List.hd pieces)

let short_os_string os_gov =
  let res =
    try
      List.assoc os_gov
	[(("5.9.0freq","schedutil"),"CFS sched");(("5.9.0freq","performance"),"CFS perf");
	  (("5.9.0cfs","schedutil"),"CFS sched");(("5.9.0cfs","performance"),"CFS perf");
	  (("5.9.0retry","schedutil"),"\\snest{}-old sched");(("5.9.0retry","performance"),"\\snest{}-old perf");
	  (("5.9.0retryfixed","schedutil"),"\\snest{} sched");(("5.9.0retryfixed","performance"),"\\snest{} perf");
	  (("5.9.0Nest","schedutil"),"\\snest{} sched");(("5.9.0Nest","performance"),"\\snest{} perf");
	  (("5.9.0nest","schedutil"),"\\snest{} sched");(("5.9.0nest","performance"),"\\snest{} perf");
	  (("5.9.0gc","schedutil"),"\\snest{} sched");(("5.9.0gc","performance"),"\\snest{} perf");
	  (("5.9.0gcnospin","schedutil"),"\\snest{} nospin sched");(("5.9.0gcnospin","performance"),"\\snest{} nospin perf");
	  (("5.9.0smovefixed","schedutil"),"Smove sched");(("5.9.0smovefixed","performance"),"Smove perf");
	  (("5.9.0smoveoriginal","schedutil"),"Smove sched");(("5.9.0smoveoriginal","performance"),"Smove perf");
	  (("5.9.0smoveoriginalzero","schedutil"),"Smove0 sched");(("5.9.0smoveoriginalzero","performance"),"Smove0 perf")]
    with Not_found -> (fst os_gov ^ " " ^ snd os_gov) in
  ignore (os_string os_gov); (* for the abbreviation index *)
  res

let short_os_string2 os_gov =
  let s = short_os_string os_gov in
  let pieces = List.rev(Str.split (Str.regexp " ") s) in
  (String.concat " " (List.rev (List.tl pieces)), List.hd pieces)

let osorder oses =
  let index os =
    try
      List.assoc os 
	[(("5.9.0freq","schedutil"),0);(("5.9.0freq","performance"),1);
	  (("5.9.0nest","schedutil"),2);(("5.9.0nest","performance"),3);
	  (("5.9.0retry","schedutil"),2);(("5.9.0retry","performance"),3);
	  (("5.9.0retryfixed","schedutil"),2);
	  (("5.9.0retryfixed","performance"),3);
	  (("5.9.0Nest","schedutil"),2);
	  (("5.9.0Nest","performance"),3)]
    with Not_found -> 4 in
  let l = List.map (fun os -> (index os,os)) oses in
  let l = List.sort compare l in
  List.map snd l

let machine_index machine =
  let get_index = function
      "dahu" -> 0.
    | "yeti" -> 1.
    | "troll" -> 2.
    | "i80" -> 3.
    | "w2155" -> 4.
    | "4650g" -> 5.
    | "gruss" -> 5.
    | "gros" -> 5.
    | "grvingt" -> 5.
    | "grappe" -> 5.
    | "neowise" -> 5.
    | "5.9.0freq" -> 7. (* for flip *)
    | "5.15.0freq" -> 7.
    | "5.9.0retry" -> 8.
    | "5.9.0retryfixed" -> 9.
    | "5.9.0Nest" -> 10.
    | machine -> failwith ("unknown machine: "^machine) in
  match Str.split (Str.regexp_string "-") machine with
    [machine] -> get_index machine
  | [machine;index] -> get_index machine +. ((float_of_string index) /. 1000.) (* hack *)
  | _ -> failwith ("unknown machine: "^machine)

let normalize_phoronix = function
    "ASKAP1.0 - Test: Hogbom Clean OpenMP" -> "askap 5"
  | "Apache Cassandra4.0 - Test: Writes" -> "cassandra 1"
  | "ArrayFire3.7 - Test: BLAS CPU" -> "arrayfire 2"
  | "ArrayFire3.7 - Test: Conjugate Gradient CPU" -> "arrayfire 3"
  | "Cpuminer-Opt3.15.5 - Algorithm: Blake-2 S" -> "cpuminer-opt 6"
  | "Cpuminer-Opt3.15.5 - Algorithm: Myriad-Groestl" -> "cpuminer-opt 8"
  | "Cpuminer-Opt3.15.5 - Algorithm: Quad SHA-256, Pyrite" -> "cpuminer-opt 11"
  | "Cpuminer-Opt3.15.5 - Algorithm: Skeincoin" -> "cpuminer-opt 7"
  | "Cpuminer-Opt3.15.5 - Algorithm: Triple SHA-256, Onecoin" -> "cpuminer-opt 9"
  | "FFmpeg4.0.2 - H.264 HD To NTSC DV" -> "ffmpeg 1"
  | "GraphicsMagick1.3.33 - Operation: Resizing" -> "graphics-magick 4"
  | "Intel Open Image Denoise1.4.0 - Run: RT.hdr_alb_nrm.3840x2160" -> "oidn 1"
  | "Intel Open Image Denoise1.4.0 - Run: RT.ldr_alb_nrm.3840x2160" -> "oidn 2"
  | "Intel Open Image Denoise1.4.0 - Run: RTLightmap.hdr.4096x4096" -> "oidn 3"
  | "Rodinia3.1 - Test: OpenMP Leukocyte" -> "rodinia 5"
  | "Zstd Compression1.5.0 - Compression Level: 3, Long Mode - Compression Speed" -> "zstd compression 10"
  | "Zstd Compression1.5.0 - Compression Level: 8, Long Mode - Compression Speed" -> "zstd compression 7"
  | "libavif avifenc0.9.0 - Encoder Speed: 6, Lossless" -> "libavif avifenc 1"
  | "libgav10.16.3 - Video Input: Chimera 1080p" -> "libgav1 4"
  | "libgav10.16.3 - Video Input: Chimera 1080p 10-bit" -> "libgav1 3"
  | "libgav10.16.3 - Video Input: Summer Nature 1080p" -> "libgav1 2"
  | "libgav10.16.3 - Video Input: Summer Nature 4K" -> "libgav1 1"
  | "oneDNN2.1.2 - Harness: IP Shapes 1D - Data Type: f32 - Engine: CPU" -> "onednn 5"
  | "oneDNN2.1.2 - Harness: IP Shapes 3D - Data Type: f32 - Engine: CPU" -> "onednn 4"
  | "oneDNN2.1.2 - Harness: Recurrent Neural Network Training - Data Type: bf16bf16bf16 - Engine: CPU" -> "onednn 11"
  | "oneDNN2.1.2 - Harness: Recurrent Neural Network Training - Data Type: f32 - Engine: CPU" -> "onednn 7"
  | "oneDNN2.1.2 - Harness: Recurrent Neural Network Training - Data Type: u8s8f32 - Engine: CPU" -> "onednn 14"
  | "oneDNN2.1.2 - Harness: IP Shapes 1D - Data Type: u8s8f32 - Engine: CPU" -> "onednn 17"
  | s -> s

let normalize_app small s =
  match Str.split_delim (Str.regexp "_configure") s with
    [s;""] -> s
  | _ ->
      let res =
	match Str.split_delim (Str.regexp "_R1") s with
	  [s;_] -> s
	| _ -> s in
      if small
      then
	match res with
	  "tradebeans" -> "trade-beans"
	| "tradesoap" -> "trade-soap"
	| "tradesoap-eval" -> "trade-soap-eval"
	| "lusearch-fix" -> "lu-search-fix"
	| "lusearch" -> "lu-search"
	| "graphchi-eval" -> "graph-chi-eval"
	| "cassandra-eval" -> "cas-sandra-eval"
	| _ -> res
      else normalize_phoronix res

type maxvalue = Free of float * float | Fixed

let dacapo_single_threaded = (* overrides CN designation which is not always correct *)
  ["avrora";
    "batik";
    "eclipse";
    "fop";
    "jython";
    "luindex";
    "batik-eval";
    "biojava-eval";
    "eclipse-eval";
    "jme-eval";
    "tradesoap-eval";
    "trade-soap-eval";
    "kafka-eval"]

let make_dacapo l =
  let (single,multi) =
    List.partition
      (function app ->
	let na = normalize_app false app in
	List.mem na dacapo_single_threaded)
      l in
  (List.sort compare single) @ (List.sort compare multi)

let dacapo_r1 =
  ("One run, and thus the application is mixed with JIT compilation",
   Fixed,Some Printer.BIG,
   (make_dacapo
      ["avrora_R1_C1";
	"batik-eval_R1_C";
	"eclipse-eval_R1_C1";
	"fop_R1_C1";
	"jython_R1_C1";
	"kafka-eval_R1_CN";
	"luindex_R1_C1";
	"jme-eval_R1_C32";
	"lusearch-fix_R1_C64";
	"lusearch_R1_C64";
	"xalan_R1_C100";
	"biojava-eval_R1_CN";
	"cassandra-eval_R1_CN";
	"graphchi-eval_R1_CN";
	"h2_R1_CN";
	"pmd_R1_C1";
	"sunflow_R1_CN";
	"tomcat-eval_R1_CN_last";
	"tradebeans_R1_CN";
	"tradesoap-eval_R1_CN";
	"zxing-eval_R1_CN"]))

let dacapo_r10 =
  ("Ten runs, application with decreasing interference from JIT compilation",
   Free(60.,-15.),Some Printer.BIG,
   (make_dacapo
      ["avrora_R10_C1";
	"batik-eval_R10_C";
	"eclipse-eval_R10_C1";
	"fop_R10_C1";
	"jython_R10_C1";
	"kafka-eval_R10_CN";
	"luindex_R10_C1";
	"jme-eval_R10_C32";
	"lusearch-fix_R10_C64";
	"lusearch_R10_C64";
	"xalan_R10_C100";
	"biojava-eval_R10_CN";
	"cassandra-eval_R10_CN";
	"graphchi-eval_R10_CN";
	"h2_R10_CN";
	"pmd_R10_C1";
	"sunflow_R10_CN";
	"tomcat-eval_R10_CN";
	"tradebeans_R10_CN";
	"tradesoap-eval_R10_CN";
	"zxing-eval_R10_CN"]))

let dacapo_last =
  ("Last run, application with little or no interference from JIT compilation",
   Fixed,Some Printer.BIG,
   (make_dacapo
      ["avrora_R10_C1_last";
	"batik-eval_R10_C_last";
	"eclipse-eval_R10_C1_last";
	"fop_R10_C1_last";
	"jython_R10_C1_last";
	"kafka-eval_R10_CN_last";
	"luindex_R10_C1_last";
	"jme-eval_R10_C32_last";
	"lusearch-fix_R10_C64_last";
	"lusearch_R10_C64_last";
	"xalan_R10_C100_last";
	"biojava-eval_R10_CN_last";
	"cassandra-eval_R10_CN_last";
	"graphchi-eval_R10_CN_last";
	"h2_R10_CN_last";
	"pmd_R10_C1_last";
	"sunflow_R10_CN_last";
	"tomcat-eval_R10_CN_last";
	"tradebeans_R10_CN_last";
	"tradesoap-eval_R10_CN_last";
	"zxing-eval_R10_CN_last"]))

let dacapo_better =
  ("Applications where \\snest{} is better than CFS",
   Fixed,Some (Printer.MIDDLE 3),
   (make_dacapo
      ["lusearch-fix_R10_C64";
	"lusearch_R10_C64";
	"xalan_R10_C100";
	"graphchi-eval_R10_CN";
	"h2_R10_CN";
	"pmd_R10_C1";
	"sunflow_R10_CN";
	"tradebeans_R10_CN"]))

let all_configures =
  ("",Fixed,Some (Printer.MIDDLE 3),
   ["erlang_configure";
     "ffmpeg_configure";
     "gcc_configure";
     "gdb_configure";
     "imagemagick_configure";
     "linux_configure";
     "llvm_ninja_configure";
     "llvm_unix_configure";
     "mplayer_configure";
     "nodejs_configure";
     "php_configure"])

let fullnas =
  ("NAS",Free (20.,-15.),Some Printer.SMALL,
   ["bt.C.x";
     "cg.C.x";
     "ep.C.x";
     "ft.C.x";
     "is.C.x";
     "lu.C.x";
     "mg.C.x";
     "sp.C.x";
     "ua.C.x"])
(*
let normalnas =
  ("NAS without lu.C",Free (20.,-25.),
   ["bt.C.x";
     "cg.C.x";
     "ep.C.x";
     "ft.C.x";
     "is.C.x";
     "mg.C.x";
     "sp.C.x";
     "ua.C.x"])
let variablenas =
  ("lu.C",Free (20.,-25.),
   ["lu.C.x"])
*)

let os_nest_cfs_with_smove =
  ("CFS performance and \\snest{} and Smove",
   [("5.9.0freq","schedutil");
     ("5.9.0freq","performance");
     ("5.9.0retry","schedutil");
     ("5.9.0retry","performance");
     ("5.9.0retryfixed","schedutil");
     ("5.9.0retryfixed","performance");
     ("5.9.0Nest","schedutil");
     ("5.9.0Nest","performance");
     ("5.9.0smoveoriginal","schedutil")])
let os_nest_cfs =
  ("CFS performance and \\snest{}",
   [("5.9.0freq","schedutil");
     ("5.9.0freq","performance");
     ("5.9.0retry","schedutil");
     ("5.9.0retry","performance");
     ("5.9.0retryfixed","schedutil");
     ("5.9.0retryfixed","performance");
     ("5.9.0Nest","schedutil");
     ("5.9.0Nest","performance")])
let os_nest_smove =
  ("\\snest{} and Smove",
   [("5.9.0freq","schedutil");
     ("5.9.0retry","schedutil");
     ("5.9.0retryfixed","schedutil");
     ("5.9.0Nest","schedutil");
     ("5.9.0smoveoriginal","schedutil");
     ("5.9.0smoveoriginalzero","schedutil")])

(*let collections =
  [(normalnas,os_nest_cfs);(variablenas,os_nest_cfs);
    (dacapo_r1,os_nest_cfs);(dacapo_r10,os_nest_cfs);(dacapo_last,os_nest_cfs);
    (dacapo_better,os_nest_smove);(all_configures,os_nest_cfs);
    (all_configures,os_nest_smove)]*)
let collections =
  [(*(normalnas,os_nest_cfs);(variablenas,os_nest_cfs);*)(fullnas,os_nest_cfs);
    (dacapo_r10,os_nest_cfs);(all_configures,os_nest_cfs_with_smove)]
let power_collections =
  [(*(normalnas,os_nest_cfs);(variablenas,os_nest_cfs);*)(fullnas,os_nest_cfs);
    (dacapo_r10,os_nest_cfs);(all_configures,os_nest_cfs)]

let free_fixed largest smallest maxvalue fn =
  match maxvalue with
    Fixed -> (largest,smallest)
  | Free(mx,mn) ->
      let (largest,smallest) = fn() in
      if largest <= mx && smallest >= mn
      then (mx,mn)
      else (largest,smallest)

type infos =
    { perfinfo : Read_data.csv_data option;
      ctrinfo : Read_data.perf_data list;
      aucinfo : Read_data.auc_data option;
      freqinfo : Read_data.freq_data list option;
      eventinfo : (string * int) list option;
      miginfo : (string * int) list option;
      turboinfo : Read_data.turbo_data option
    }

let get_perf app os perfs defaultv cont =
  let info =
    try List.assoc os (List.assoc app perfs)
    with _ ->
      let (_,name) = app in
      let (os,gov) = os in
      failwith (Printf.sprintf "get_perf: problem finding %s-%s or %s" os gov name) in
  match info.perfinfo with
    Some perf ->
      if perf.Read_data.high_std
      then defaultv
      else cont perf
  | None -> defaultv

let get_all_perf app os perfs defaultv cont = (* include high stds *)
  let info =
    try List.assoc os (List.assoc app perfs)
    with _ ->
      let (_,name) = app in
      let (os,gov) = os in
      failwith (Printf.sprintf "get_all_perf: problem finding %s-%s or %s" os gov name) in
  match info.perfinfo with
    Some perf -> cont perf
  | None -> defaultv

let get_ctrs ctr app os perfs defaultv cont =
  let info = List.assoc os (List.assoc app perfs) in
  let res =
    try Some(List.find (fun s -> s.Read_data.event = ctr) info.ctrinfo)
    with Not_found -> None in
  match res with
    Some v -> cont v
  | None -> defaultv

let get_auc app os perfs defaultv cont =
  let info = List.assoc os (List.assoc app perfs) in
  match info.aucinfo with
    Some auc -> cont auc
  | None -> defaultv

let get_freq app os perfs defaultv cont =
  let info = List.assoc os (List.assoc app perfs) in
  match info.freqinfo with
    Some freq -> cont freq
  | None -> defaultv

let get_events app os perfs defaultv cont =
  let info = List.assoc os (List.assoc app perfs) in
  match info.eventinfo with
    Some events -> cont events
  | None -> defaultv

let get_mig app os perfs defaultv cont =
  let info = List.assoc os (List.assoc app perfs) in
  match info.miginfo with
    Some mig -> cont mig
  | None -> defaultv

let get_power app os perfs defaultv cont =
  let info = List.assoc os (List.assoc app perfs) in
  match info.turboinfo with
    Some power -> cont power
  | None -> defaultv

let get_std app os stds defaultv cont =
  match List.assoc os (List.assoc app stds) with
    Some std -> cont std
  | None -> defaultv

let get_imprv app os improvements defaultv cont =
  match List.assoc os (List.assoc app improvements) with
    Some imprv -> cont imprv
  | None -> defaultv

let red s = Printf.sprintf "\\textcolor{red}{%s}" s
let green s = Printf.sprintf "\\textcolor{gr}{%s}" s
let bold s = Printf.sprintf "{\\bf %s}" s
let black s = s

(* ------------------------------------------------------------------ *)

let print_mean_and_std avg std units size apps =
  let side = "r" (*if avg > 10. *. std then "r" else "l"*) in
  let pct_std = (100. *. std) /. avg in
  let pct_std =
    if pct_std < 1.
    then Printf.sprintf "%0.1f" pct_std
    else Printf.sprintf "%d" (int_of_float pct_std) in
  let avg =
    if avg > 1000000.
    then Printf.sprintf "%0.1fM" (avg /. 1000000.)
    else if avg > 100000.
    then Printf.sprintf "%dK" (int_of_float(avg /. 1000.))
    else if avg > 100.
    then Printf.sprintf "%d" (int_of_float avg)
    else Printf.sprintf "%0.2f" avg in
  let font =
    match size with
      Printer.SMALL -> "tiny"
    | Printer.MIDDLE _ when apps > 10 -> "tiny"
    | _ -> "scriptsize" in
  let ismiddle = function Printer.MIDDLE _ -> true | _ -> false in
  if ismiddle size && apps > 12 (* hack for phoronix *)
  then Printf.sprintf "\\%s\\begin{tabular}{@{}%s@{}}%s\\end{tabular}" font side avg (* no units for phoronix *)
  else Printf.sprintf "\\%s\\begin{tabular}{@{}%s@{}}%s%s\\\\[-0.3em]$\\pm$%s\\%%\\end{tabular}" font side avg units pct_std

let graph_runtime_for_paper o apps oses refos perfs largest smallest center =
  let legend = List.map PL.clean (List.map os_string oses) in
  let appstrs = List.map (fun (_,str) -> str) apps in
  let ymax = largest *. 1.2 in
  let ymin = min (-0.2) (smallest *. 1.2) in
  Printer.start_tikz_graph o appstrs "" 45 "seconds" true
    ymax ymin (Printer.MIDDLE 3) 1.4 false 3 false center 0.06 false "";
  List.iter
    (fun os ->
      let fn =
	if os = refos
	then Printer.tikz_quiet_bar_plot "red"
	else Printer.tikz_bar_plot in
      fn o
	(List.mapi
	   (fun i app ->
	     get_perf app os perfs (i,0.0,0.0,"none")
	       (fun perf ->
		 (i,perf.Read_data.runtime,perf.Read_data.std,perf.Read_data.csv_file)))
	   apps))
    oses;
  Printer.end_tikz_graph o (List.length apps) ymax ymin legend false center

let graph_improvements_for_paper o machine apps oses refos perfs improvements largest smallest center big first last right fixed_free =
  (* first means need legend, last mean needs application names *)
  let unref_oses = List.filter (fun x -> x <> refos) oses in
  let os_string = if big = Printer.BIG then os_string else short_os_string in
  let legend =
    if first && not (big = Printer.SMALL)
    then List.map PL.clean (List.map os_string unref_oses)
    else [] in
  let largest = (largest -. 1.) *. 1.02 in (* space for graph *)
  let smallest = (smallest -. 1.) *. 1.02 in
  let real_largest = smallest +. ((largest -. smallest) *. 1.6) in
  let node_largest = smallest +. ((largest -. smallest) *. 1.6) in
  let apps =
    let phor = ref false in
    let numify appstr =
      let p = normalize_phoronix appstr in
      if appstr = p
      then (appstr,0)
      else
	try
	  phor := true;
	  let pieces = List.rev(Str.split (Str.regexp " ") p) in
	  let n = int_of_string(List.hd pieces) in
	  let rest = String.concat " " (List.rev (List.tl pieces)) in
	  (rest,n)
	with _ -> (p,0) in
    let l =
      List.map
	(function ((app,appstr) as a) -> (numify appstr,a))
	apps in
    if !phor
    then List.map snd (List.sort compare l)
    else apps in
  let appstrs =
    if last then List.map (fun (_,str) -> normalize_app false str) apps else [] in
  let appstrs =
    let smaller appstr =
      if List.length apps > 21
      then Printf.sprintf "\\scriptsize{%s}" appstr
      else appstr in
    List.map
      (fun appstr ->
	let appstr = smaller appstr in
	if List.mem appstr dacapo_single_threaded
	then Printf.sprintf "\\textcolor{blue}{%s}" appstr
	else Printf.sprintf "{%s}" appstr)
      appstrs in
  Printer.start_tikz_graph o appstrs "" 45
    "\\% improvement" (not(right && fixed_free = Fixed))
    real_largest smallest big 1.4 false 4 (not last) center
    0.03 true ((machine_to_title machine)^",xmin=0");
  List.iter
    (fun os ->
      let l =
	List.mapi
	  (fun i app ->
	    get_perf app os perfs (i,None,0.0,"none")
	      (fun perf ->
		get_imprv app os improvements (i,None,0.0,"none")
		  (fun (imprv,impstd) ->
		    (i,Some(pct imprv),impstd *. 100.,perf.Read_data.csv_file))))
	  apps in
      let l =
	List.rev
	  (List.fold_left
	     (fun prev ->
	       function (i,Some v,s,file) -> (i,v,s,file)::prev | _-> prev)
	     [] l) in
      Printer.tikz_bar_plot o l)
    unref_oses;
  List.iteri
    (fun i app ->
      let runtime =
	get_perf app refos perfs "---"
	  (function perf ->
	    print_mean_and_std perf.Read_data.runtime perf.Read_data.std "s" big
	      (List.length apps)) in
      Printf.fprintf o "\\node[rotate=90,anchor=east] at (%d,%f) {%s};\n"
	i node_largest runtime)
    apps;
  Printer.end_tikz_graph o (List.length apps) real_largest smallest legend false center

let graph_improvements o apps oses refos perfs improvements largest smallest =
  let unref_oses = List.filter (fun x -> x <> refos) oses in
  let legend = List.map PL.clean (List.map os_string unref_oses) in
  let largest = (largest -. 1.) *. 1.02 in (* space for graph *)
  let smallest = (smallest -. 1.) *. 1.02 in
  let real_largest = smallest +. ((largest -. smallest) *. 1.25) in
  let node_largest = smallest +. ((largest -. smallest) *. 1.1) in
  let appstrs = List.map (fun (_,str) -> str) apps in
  Printer.start_tikz_graph o appstrs "" 45
    "\\begin{tabular}{c}\\% improvement over\\\\CFS schedutil\\end{tabular}" true
    real_largest smallest Printer.BIG 3. false 4 false true 0.06 false "";
  List.iter
    (fun os ->
      Printer.tikz_bar_plot o
	(List.mapi
	   (fun i app ->
	     get_perf app os perfs (i,0.0,0.0,"none")
	       (fun perf ->
		 get_imprv app os improvements (i,0.0,0.0,"none")
		   (fun (imprv,impstd) ->
		     (i,pct imprv,impstd *. 100.,perf.Read_data.csv_file))))
	   apps))
    unref_oses;
  List.iteri
    (fun i app ->
      let runtime =
	get_perf app refos perfs "---"
	  (function perf ->
	    Printf.sprintf "%0.3fs" perf.Read_data.runtime) in
      let (auc,aoc,aoc2) =
	get_auc app refos perfs ("---","---","---")
	  (function auc ->
	    (Printf.sprintf "U:%0.3f" auc.Read_data.auc,
	     Printf.sprintf "O:%0.3f" (0. -. auc.Read_data.aoc),
	     Printf.sprintf "O2:%0.3f" (0. -. auc.Read_data.aoc2))) in
      Printf.fprintf o "\\node at (%d,%f) {\\begin{tabular}{c}%s\\\\%s\\\\%s\\\\%s\\end{tabular}};\n"
	i node_largest runtime auc aoc aoc2)
    apps;
  Printer.end_tikz_graph o (List.length apps) real_largest smallest legend false true

(*
i80:
10 seconds: cpu: 83.236600+-0.580135, ram: 10.468700+-0.071308
20 seconds: cpu: 82.737133+-0.391936, ram: 10.406167+-0.043364
30 seconds: cpu: 82.445256+-0.311337, ram: 10.385267+-0.039549
40 seconds: cpu: 82.204275+-0.255195, ram: 10.358517+-0.021235
50 seconds: cpu: 82.153007+-0.186754, ram: 10.369447+-0.026464
60 seconds: cpu: 82.252561+-0.177483, ram: 10.370667+-0.021332
overall: cpu: 82.504805+-0.512209, ram: 10.393127+-0.054931
root@troll-1:/home/jlawall/idle_turbostat# more troll-1-idle 
10 seconds: cpu: 35.523133+-0.327948, ram: 167.026200+-4.712174
20 seconds: cpu: 35.323450+-0.323547, ram: 166.218933+-2.831783
30 seconds: cpu: 35.184556+-0.161592, ram: 164.354400+-1.900205
40 seconds: cpu: 35.233875+-0.859509, ram: 165.385892+-5.230763
50 seconds: cpu: 34.908953+-0.291500, ram: 163.903140+-2.995885
60 seconds: cpu: 34.856044+-0.235051, ram: 163.641422+-3.095978
overall: cpu: 35.171669+-0.484275, ram: 165.088331+-3.802149
root@dahu-1:/home/jlawall/idle_turbostat# more dahu-1-idle
10 seconds: cpu: 22.405867+-0.413723, ram: 10.130000+-0.263546
20 seconds: cpu: 22.166033+-0.299743, ram: 9.974817+-0.208733
30 seconds: cpu: 22.352867+-0.369762, ram: 9.815222+-0.438498
40 seconds: cpu: 21.992992+-0.307478, ram: 9.451808+-0.410097
50 seconds: cpu: 22.147647+-0.254904, ram: 9.297627+-0.342153
60 seconds: cpu: 22.261872+-0.221396, ram: 9.532111+-0.270098
overall: cpu: 22.221213+-0.342441, ram: 9.700264+-0.442210
root@dahu-1:/home/jlawall/idle_turbostat# more yeti-2-idle 
10 seconds: cpu: 58.860433+-0.452538, ram: 16.893500+-0.668338
20 seconds: cpu: 58.255817+-0.461711, ram: 16.481133+-0.653404
30 seconds: cpu: 59.010322+-1.725617, ram: 17.393467+-2.185676
40 seconds: cpu: 57.947400+-0.415700, ram: 15.261800+-0.606752
50 seconds: cpu: 58.137220+-0.347374, ram: 15.605193+-0.626916
60 seconds: cpu: 58.374728+-0.339871, ram: 16.167500+-0.525126
overall: cpu: 58.430987+-0.873439, ram: 16.300432+-1.270003
*)

let idle_table =
  [("dahu",22.261872,9.532111);("yeti",58.430987,16.300432);("troll",34.856044,163.641422)]

let graph_power_improvements o machine getter str apps oses refos perfs improvements largest smallest forpaper big first last right center free_fixed =
  let unref_oses = List.filter (fun x -> x <> refos) oses in
  let os_string = if big = Printer.BIG then os_string else short_os_string in
  let legend = if first then List.map PL.clean (List.map os_string unref_oses) else [] in
  let found = ref false in
  let data =
    List.map
      (fun os ->
	List.mapi
	  (fun i app ->
	    get_power app os perfs (i,0.0,0.0,"none")
	      (fun ospower ->
		let (_,_,osvalues) = getter ospower in
		get_power app refos perfs (i,0.0,0.0,"none")
		  (fun refospower ->
		    found := true;
		    let (refospower,_,_) = getter refospower in
		    let (imprv,impstd) = Util.speedup_and_std true refospower osvalues in
		    (i,pct imprv,impstd *. 100.,ospower.Read_data.turbo_file))))
	  apps)
      unref_oses in
  if !found
  then
    begin
      let largest = largest *. 1.02 in (* space for graph *)
      let smallest = smallest *. 1.02 in
      let factor = if forpaper then 1.8 else 1.25 in
      let real_largest = largest *. factor in
      let node_largest = largest *. 1.1 in
      let appstrs =
	if last then List.map (fun (_,str) -> normalize_app false str) apps else [] in
      let appstrs =
	List.map
	  (fun appstr ->
	    if List.mem appstr dacapo_single_threaded
	    then Printf.sprintf "\\textcolor{blue}{%s}" appstr
	    else appstr)
	  appstrs in
      let height = if forpaper then 1.4 else 3. in
      let legcols = 4 in
      Printer.start_tikz_graph o appstrs ""
	(if String.length  (snd(List.hd apps)) > 30 then 90 else 45)
	(if forpaper
	then "\\% improvement"
	else Printf.sprintf "\\begin{tabular}{c}\\%% %s\\\\improvement over\\\\CFS schedutil\\end{tabular}"
	   str)
	 (forpaper && not(right && free_fixed = Fixed))
	real_largest smallest big height false legcols true center 0.06 true
	(machine_to_title machine);
      List.iter (Printer.tikz_bar_plot o) data;
      List.iteri
	(fun i app ->
	  let power =
	    get_power app refos perfs "---"
	      (function power ->
		let (avg,std,_) = getter power in
		print_mean_and_std avg std "J" big (List.length apps)) in
	  if forpaper
	  then
	    Printf.fprintf o "\\node[rotate=90,anchor=east] at (%d,%f) {%s};\n"
	      i real_largest power
	  else
	    let runtime =
	      get_perf app refos perfs "---"
		(function perf ->
		  Printf.sprintf "%0.3fs" perf.Read_data.runtime) in
	    Printf.fprintf o "\\node at (%d,%f) {\\begin{tabular}{c}%s\\\\%s\\end{tabular}};\n"
	      i node_largest runtime power)
	apps;
      Printer.end_tikz_graph o (List.length apps)
	real_largest smallest legend false center
    end

(* speedup in X, power improvement in Y *)
let graph_speedup_vs_power o getter str apps oses refos perfs improvements largest smallest max_speedup min_speedup =
  let unref_oses = List.filter (fun x -> x <> refos) oses in
  List.iter
    (function os ->
      Printf.fprintf o "%s-%s\n\n" (fst os) (snd os);
      Printer.start_tikz_graph o [] "" 45 str true
	largest smallest Printer.SMALL 1. false 3 true true 0.06 false "";
      let (worse,middle,better) =
	List.fold_left
	  (fun ((worse,middle,better) as prev) app ->
	    get_power app os perfs prev
	      (fun ospower ->
		Printf.fprintf o "%% %s\n" ospower.Read_data.turbo_file;
		let (_,_,osvalues) = getter ospower in
		get_power app refos perfs prev
		  (fun refospower ->
		    let (refospower,_,_) = getter refospower in
		    let (pwimprv,_) = Util.speedup_and_std true refospower osvalues in
		    get_imprv app os improvements prev
		      (fun (imprv,_) ->
			let res = (pct imprv,pct pwimprv) in
			if pwimprv < 0.95
			then (res::worse,middle,better)
			else if pwimprv > 1.05
			then (worse,middle,res::better)
			else (worse,res::middle,better)))))
	  ([],[],[]) apps in
      Printer.tikz_mark_fplot o "red" "*" worse;
      Printer.tikz_mark_fplot o "black" "*" middle;
      Printer.tikz_mark_fplot o "gr" "*" better;
      Printer.end_tikz_graph o (int_of_float (1. +. (max max_speedup min_speedup))) largest smallest ["worse";"middle";"better"] false true) (* second argument is not right *)
    unref_oses

let global_correlations o machine_table =
  let build_list perfs improvements refos oses apps auc_getter =
    List.fold_left
      (fun prev os ->
	List.fold_left
	  (fun prev app ->
	     get_perf app os perfs prev
	       (fun perf ->
		 get_imprv app os improvements prev
		   (fun (imprv,_) ->
		     get_auc app refos perfs prev
		       (fun auc ->
			 (imprv,auc_getter auc) :: prev))))
	  prev apps)
      [] oses in
  let columns = [PL.Left;PL.Center;PL.Center;PL.Center] in
  let titles1 = ["Machine";"underload";"overload";"overload**2"] in
  let titles2 = ["";"correlation";"correlation";"correlation"] in
  let auc_lines  = ref [] in
  let aoc_lines  = ref [] in
  let aoc2_lines = ref [] in
  let lines =
    List.concat
      (List.map
	 (function (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
	   List.map
	     (function os ->
	       let getline = build_list perfs improvements (refos,refgov) [os] apps in
	       let aucline = getline (fun auc -> auc.Read_data.auc) in
	       let aocline = getline (fun auc -> auc.Read_data.aoc) in
	       let aoc2line = getline (fun auc -> auc.Read_data.aoc2) in
	       auc_lines := aucline @ !auc_lines;
	       aoc_lines := aocline @ !aoc_lines;
	       aoc2_lines := aoc2line @ !aoc2_lines;
	       let machine = Printf.sprintf "%s %s" machine (os_string os) in
	       [PL.StringV machine;PL.FloatV4(Util.pearson aucline);
		 PL.FloatV4(Util.pearson aocline);PL.FloatV4(Util.pearson aoc2line)])
	     oses)
	 machine_table) in
  let overall =
    [[PL.StringV "overall";PL.FloatV4(Util.pearson !auc_lines);
       PL.FloatV4(Util.pearson !aoc_lines);PL.FloatV4(Util.pearson !aoc2_lines)]] in
  PL.print_table o columns [titles1;titles2] (lines @ overall) false

let graph_stds o apps oses perfs =
  let legend = List.map PL.clean (List.map os_string oses) in
  let largest = ref 0.0 in
  let stds =
    List.map
      (function (app,infos) ->
	(app,
	 List.map
	   (function (os,info) ->
	     (os,
	     match info.perfinfo with
	       None -> None
	     | Some perf ->
		 let pct = (perf.Read_data.std *. 100.) /. perf.Read_data.runtime in
		 largest := max pct !largest;
		 Some (pct,perf.Read_data.csv_file)))
	   infos))
      perfs in
  let appstrs = List.map (fun (_,str) -> str) apps in
  let ymax = 1.1 *. !largest in
  Printer.start_tikz_graph o appstrs "" 45 "\\% standard deviation" true ymax 0. Printer.BIG 3. false 4 true true 0.06 false "";
  List.iter
    (fun os ->
      Printer.tikz_bar_plot o
	(List.mapi
	   (fun i app ->
	     get_std app os stds (i,0.0,0.0,"none")
	       (fun (std,file) -> (i,std,0.,file)))
	   apps))
    oses;
  Printer.end_tikz_graph o (List.length apps) ymax 0. legend false true

(* ------------------------------------------------------------------ *)
(* multicolored evenly spaced bars *)

let print_top_stats o oses perfs improvements keys mx forpaper tall =
  let prev = ref ("","") in
  let prevstart = function
      (_,n,_)::_ -> n
    | [] -> 0 in
  let env = ref [] in
  List.iteri
    (fun i ((app,os),_) ->
      (if app <> !prev
      then
	begin
	  (if !prev <> ("","") then env := (!prev,i,prevstart !env) :: !env);
	  prev := app
	end);
      get_perf app os perfs ()
	(fun perf ->
	  (if not forpaper
	  then Printf.fprintf o "\\node at (%d,%f) {%0.2f};\n" i (mx *. 1.15) perf.Read_data.runtime);
	  let imprv =
	    get_imprv app os improvements (if forpaper then "" else "---")
	      (fun (imprv,_) ->
		let si = Printf.sprintf "%0.2f" imprv in
		if imprv > 1.05 then green si
		else if imprv < 0.95 then red si
		else si) in
	  let rotation = if forpaper then "[rotate=90,anchor=east]" else "" in
	  let top = if forpaper then (if tall > 1 then 1.4 else 1.55) else 1.06 in
	  Printf.fprintf o "\\node%s at (%d,%f) {\\scriptsize %s};\n" rotation i (mx *. top) imprv))
    keys;
  env := (!prev,List.length keys,prevstart !env) :: !env;
  let model =
    Array.to_list(Array.make tall "\\mbox{}") in
  List.iter
    (function (app,endi,starti) ->
      let (_,app) = app in
      let x = (float_of_int starti) +. ((float_of_int(endi - starti - 1)) /. 2.) in
      let top = if forpaper then (if tall > 1 then 1.55 else 1.6) else 1.22 in
      let app = normalize_app true app in
      let single_threaded = List.mem app dacapo_single_threaded in
      let app = PL.clean app in
      let app =
	if forpaper
	then
	  let pieces = Str.split (Str.regexp "-") app in
	  let pieces =
	    match List.rev pieces with
	      last::rest ->
		(List.map (fun x -> x^"-") (List.rev rest)) @ [last]
	    | _ -> failwith "not possible" in
	  let pieces =
	    if single_threaded
	    then
	      List.map
		(function piece -> Printf.sprintf "\\textcolor{blue}{%s}" piece)
		pieces
	    else pieces in
	  let pieces =
	    let rec loop pieces model =
	      match (pieces, model) with
		([],model) -> String.concat "\\\\[-1mm]" model
	      | ([p],_::ms) -> String.concat "\\\\[-1mm]" (p::ms)
	      | p::ps,_::ms -> p ^ "\\\\[-1mm]" ^ loop ps ms
	      | (_,[]) -> failwith "too big" in
	    loop pieces model in
	  Printf.sprintf
	    "{\\scriptsize\\begin{tabular}{@{}c@{}}%s\\end{tabular}}"
	    pieces
	else Printf.sprintf "\\phantom{lg}{%s}\\phantom{lg}" app in
      Printf.fprintf o "\\node at (%f,%f) {%s};\n" x (mx *. top) app)
    !env

let stacked_bar_graph_with_improvements o str mx tbl xlabels ylabels keys range legend oses perfs improvements forpaper center machine =
  if Hashtbl.length tbl > 0
  then
    begin
      let legcols =
	let maxlen =
	  List.fold_left max 0 (List.map String.length legend) in
	150 / maxlen in
      let tall =
	List.fold_left
	  (fun prev (((_,app),os),_) ->
	    let app = normalize_app true app in
	    max prev (List.length (Str.split (Str.regexp "-") app)))
	  0 keys in
      let headroom = if forpaper then 1.75 else 1.26 in
      let top = mx *. headroom in
      let widthratio = Printer.BIG in
      let height = if forpaper then (if tall > 1 then 1.65 else 1.4) else 3.0 in
      let rotation = 90 in
      let xlabelstrings =
	if xlabels
	then
	  if forpaper
	  then List.map (fun x -> Printf.sprintf "{\\scriptsize %s}" x) (List.map snd keys)
	  else List.map snd keys
	else [] in
      Printer.start_tikz_graph o xlabelstrings ylabels rotation str true
	top 0. widthratio height true legcols true center 0.01 (not (machine = None))
	(match machine with Some machine -> machine_to_title machine | None -> "");
      (if keys <> []
      then
	begin
	  List.iter
	    (fun (range,color) ->
	      let infos =
		try !(Hashtbl.find tbl range) with Not_found -> [] in
	      let pcts =
		(List.mapi
		   (fun i key ->
		     let pct =
		       try List.assoc key infos
		       with Not_found -> 0. in
		     (i,pct))
		   keys) in
	      Printf.fprintf o "\\addplot%s coordinates {%s};\n" color
		(String.concat " "
		   (List.map (fun (i,pct) -> Printf.sprintf "(%d,%f)" i pct)
		      pcts)))
	    range
	end);
      let rec loop i = function
	  ((app,os),_)::((((app1,os1),_)::_) as rest) ->
	    (if not (app = app1)
	    then
	      Printf.fprintf o "\\draw[dashed] (%d.5,0) -- (%d.5,%f);\n" i i top);
	    loop (i+1) rest
	| _ -> () in
      loop 0 keys;
      print_top_stats o oses perfs improvements keys mx forpaper tall;
      Printer.end_tikz_graph o (List.length keys) top 0. (List.map PL.clean legend) true center
    end

let freq_color_table =
  let lcolors = ["color1";"color2";"color3";"color4";"colortop"] in
  let colors = ["color1";"color2";"color3";"color4";"color5";"color6";"colortop"] in
  (* i80 *)
  let i80freqs =
    List.map2 (fun (l,h,_,_,_) color -> ((l,h),color))
      (snd Machines.i80_freq_intervals) lcolors in
  let yetifreqs =
    List.map2 (fun (l,h,_,_,_) color -> ((l,h),color))
      (snd Machines.yeti_freq_intervals2) colors in
  let w2155freqs =
    List.map2 (fun (l,h,_,_,_) color -> ((l,h),color))
      (snd Machines.w2155_freq_intervals) lcolors in
  let trollfreqs =
    List.map2 (fun (l,h,_,_,_) color -> ((l,h),color))
      (snd Machines.troll_freq_intervals2) colors in
  i80freqs @ yetifreqs @ w2155freqs @ trollfreqs

let graph_freqs o machine apps oses perfs best improvements forpaper center xlabels right =
  let keys = ref [] in
  let tbl = Hashtbl.create 101 in
  let freqs = ref [] in
  let os_string = if forpaper then short_os_string else os_string in
  List.iter
    (fun app ->
      let best = List.assoc app best in
      List.iter
	(fun os ->
	  let key =
	    let k = os_string os in
	    if forpaper
	    then ((app,os),k)
	    else
	      ((app,os),
	       get_perf app os perfs k
		 (fun perf ->
		   if Some perf.Read_data.runtime = best
		   then Printf.sprintf "{\\bf %s}" k
		   else k)) in
	  keys := key :: !keys;
	  get_freq app os perfs ()
	    (fun freq ->
	      List.iter
		(fun f ->
		  let range = (f.Read_data.low,f.Read_data.high) in
		  let pct = f.Read_data.pct in
		  (if not (List.mem range !freqs)
		  then freqs := range :: !freqs);
		  Util.hashadd tbl range (key,pct))
		freq))
	oses)
    apps;
  let ranges = List.sort compare !freqs in
  let legend =
    List.map
      (fun (low,high) ->
	Printf.sprintf "{(%d.%d, %d.%d] GHz}"
	  (low/1000000) ((low/100000) mod 10)
	  (high/1000000) ((high/100000) mod 10))
      ranges in
  let ranges =
    List.map
      (function r ->
	let color = List.assoc r freq_color_table in
	(r,Printf.sprintf "[%s,fill=%s]" color color))
      ranges in
  let ylabels = "ytick={0,50,100},yticklabels={0\\%,50\\%,100\\%}," in
  stacked_bar_graph_with_improvements o ""
    100. tbl xlabels ylabels (List.rev !keys) ranges legend oses perfs improvements forpaper center (Some machine)

let max_ovd_per_sec apps oses perfs =
  let mx = ref 0.0 in
  List.iter
    (fun app ->
      List.iter
	(fun os ->
	  get_perf app os perfs ()
	    (fun perf ->
	      get_auc app os perfs ()
		(fun auc ->
		  let runtime = perf.Read_data.runtime in
		  let wv = (float_of_int auc.Read_data.window) /. runtime in
		  let rn = (float_of_int auc.Read_data.running) /. runtime in
		  let other = (float_of_int auc.Read_data.other) /. runtime in
		  mx := max !mx (wv +. rn +. other))))
	oses)
    apps;
  !mx

let graph_ovds o apps oses perfs best improvements mx forpaper center =
  let keys = ref [] in
  let tbl = Hashtbl.create 101 in
  let os_string = if forpaper then short_os_string else os_string in
  List.iter
    (fun app ->
      let best = List.assoc app best in
      List.iter
	(fun os ->
	  get_perf app os perfs ()
	    (fun perf ->
	      get_auc app os perfs ()
		(fun auc ->
		  let wv = auc.Read_data.window in
		  if wv >= 0
		  then
		    begin
		      let runtime = perf.Read_data.runtime in
		      let wv = (float_of_int wv) /. runtime in
		      let running =
			(float_of_int auc.Read_data.running) /. runtime in
		      let other =
			(float_of_int auc.Read_data.other) /. runtime in
		      let key =
			let k = os_string os in
			((app,os),
			 if Some runtime = best
			 then Printf.sprintf "{\\bf %s}" k
			 else k) in
		      keys := key :: !keys;
		      Util.hashadd tbl "winvis" (key,wv);
		      Util.hashadd tbl "running" (key,running);
		      Util.hashadd tbl "other" (key,other)
		    end)))
	oses)
    apps;
  let range = ["winvis";"running";"other"] in
  stacked_bar_graph_with_improvements o "overload placements per second"
    mx tbl true "" (List.rev !keys) (List.map (fun r -> (r,"")) range)
    range oses perfs improvements forpaper center None

let max_things_per_sec get_events apps oses perfs =
  let mx = ref 0.0 in
  List.iter
    (fun app ->
      List.iter
	(fun os ->
	  get_perf app os perfs ()
	    (fun perf ->
	      get_events app os perfs ()
		(fun events ->
		  let runtime = perf.Read_data.runtime in
		  let events =
		    float_of_int
		    (List.fold_left (fun prev (_,cur) -> cur + prev)
		       0 events) in
		  let eventsps = events /. runtime in
		  mx := max !mx eventsps)))
	oses)
    apps;
  !mx

let graph_events o getter str apps oses perfs best improvements mx forpaper center =
  let keys = ref [] in
  let tbl = Hashtbl.create 101 in
  let range = ref [] in
  let os_string = if forpaper then short_os_string else os_string in
  List.iter
    (fun app ->
      let best = List.assoc app best in
      List.iter
	(fun os ->
	  get_perf app os perfs ()
	    (fun perf ->
	      let runtime = perf.Read_data.runtime in
	      getter app os perfs ()
		(fun events ->
		  let key =
		    let k = os_string os in
		    ((app,os),
		     if Some runtime = best
		     then Printf.sprintf "{\\bf %s}" k
		     else k) in
		  keys := key :: !keys;
		  List.iter
		    (function (event,v) ->
		      let v = (float_of_int v) /. runtime in
		      (if not (List.mem event !range) then range := event :: !range);
		      Util.hashadd tbl event (key,v))
		    events)))
	oses)
    apps;
  let range = List.sort compare !range in
  stacked_bar_graph_with_improvements o (str^" per second")
    mx tbl true "" (List.rev !keys) (List.map (fun r -> (r,"")) range)
    range oses perfs improvements forpaper center None

(* ------------------------------------------------------------------ *)

let printperf perf =
  if perf > 1000000.
  then PL.StringV (Printf.sprintf "%0.2fM" (perf /. 1000000.))
  else if perf > 100000.
  then PL.StringV (Printf.sprintf "%0.2fK" (perf /. 1000.))
  else if perf > 10000.
  then PL.FloatV2 perf
  else PL.FloatV4 perf

let print_summary_table o machine_table =
  let interesting_apps = ref [] in
  let good = 1. +. !summary_threshold in
  let bad  = 1. -. !summary_threshold in
  List.iter
    (function (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
      let unref_oses = List.filter (fun x -> x <> (refos,refgov)) oses in
      List.iter
	(fun app ->
	  if not (List.mem app !interesting_apps)
	  then
	    try
	      ignore
		(List.find
		   (function os ->
		     get_imprv app os improvements false
		       (fun (imprv,_) -> imprv > good || imprv < bad))
		   unref_oses);
	      interesting_apps := app :: !interesting_apps
	    with Not_found -> ())
	apps)
    machine_table;
  let machines = List.map fst machine_table in
  let oses =
    let (refos,refgov,oses,apps,perfs,best,improvements) = snd(List.hd machine_table) in
    oses in
  let os_machines =
    List.concat
      (List.map
	 (function os ->
	   List.map (function machine -> (os,machine)) machines)
	 oses) in
  let long =
    List.exists
      (fun (_,appstr) -> String.length appstr > 50)
      !interesting_apps in
  let oslong =
    List.exists
      (fun (os,_) -> String.length (fst (short_os_string2 os)) > 20)
      os_machines in
  let columns =
    let left = if long then PL.Sized 2.8 else PL.Left in
    left :: PL.Center :: List.map (fun x -> if oslong then PL.Sized 1. else PL.Center) os_machines in
  let titles1 = "Test" :: "Better" :: (List.map (fun (os,machine) -> fst (short_os_string2 os)) os_machines) in
  let titles2 = "" :: "" :: (List.map (fun (os,machine) -> snd (short_os_string2 os)) os_machines) in
  let titles3 = "" :: "" :: (List.map (fun (os,machine) -> machine) os_machines) in
  let nothing = PL.StringV "---" in
  let data_lines =
    List.mapi
      (fun i (((nm,appstr) : (string * string)) as app) ->
	let appstr =
	  if nm <> appstr
	  then Printf.sprintf "%d. %s: %s" i nm appstr
	  else appstr in
	let better = ref "unk." in
	let res =
	  List.map
	    (function (os,machine) ->
	      let (refos,refgov,oses,apps,perfs,best,improvements) =
		List.assoc machine machine_table in
	      if os = (refos,refgov)
	      then
		get_perf app os perfs nothing
		  (fun perf ->
		    (if perf.Read_data.lower_is_better
		    then better := "L"
		    else better := "H");
		    PL.FloatV2 perf.Read_data.runtime)
	      else
		get_imprv app os improvements nothing
		  (fun (imprv,_) ->
		    if imprv > 1.05 then PL.Color("gr",PL.FloatV2 imprv)
		    else if imprv < 0.95 then PL.Color("red",PL.FloatV2 imprv)
		    else PL.FloatV2 imprv))
	    os_machines in
	(PL.StringV appstr) :: (PL.StringV !better) :: res)
      (List.sort compare !interesting_apps) in
  PL.print_table o columns [titles1;titles2;titles3] data_lines long;
  !interesting_apps

let print_high_stddev_table o machine_table =
  let interesting_apps = ref [] in
  List.iter
    (function (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
      List.iter
	(fun app ->
	  if not (List.mem app !interesting_apps)
	  then
	    get_perf app (refos,refgov) perfs ()
	      (fun perf ->
		if perf.Read_data.std > 0.15 *. perf.Read_data.runtime
		then interesting_apps := app :: !interesting_apps))
	apps)
    machine_table;
  let machines = List.map fst machine_table in
  let oses =
    let (refos,refgov,oses,apps,perfs,best,improvements) = snd(List.hd machine_table) in
    [(refos,refgov)] in
  let os_machines =
    List.concat
      (List.map
	 (function os ->
	   List.map (function machine -> (os,machine)) machines)
	 oses) in
  let long =
    List.exists
      (fun (_,appstr) -> String.length appstr > 50)
      !interesting_apps in
  let columns =
    let left = if long then PL.Sized 2.8 else PL.Left in
    left :: List.map (fun x -> PL.RightLeft) os_machines in
  let titles1 = "Test" :: (List.map (fun ((os,gov),machine) -> os) os_machines) in
  let titles2 = "" :: (List.map (fun ((os,gov),machine) -> gov) os_machines) in
  let titles3 = "" :: (List.map (fun ((os,gov),machine) -> machine) os_machines) in
  let nothing = PL.StringV "---" in
  let data_lines =
    List.map
      (function ((nm,appstr) as app) ->
	let appstr =
	  if nm <> appstr
	  then Printf.sprintf "%s: %s" nm appstr
	  else appstr in
	let res =
	  List.map
	    (function (os,machine) ->
	      let (refos,refgov,oses,apps,perfs,best,improvements) =
		List.assoc machine machine_table in
	      let imprv =
		if List.mem ("5.9.0Nest","schedutil") oses (* why is this a special case? *)
		then
		  get_imprv app ("5.9.0Nest","schedutil") improvements nothing
		    (fun (imprv,_) ->
		      if imprv > 1.05 then PL.Color("gr",PL.FloatV2 imprv)
		      else if imprv < 0.95 then PL.Color("red",PL.FloatV2 imprv)
		      else PL.FloatV2 imprv)
		else nothing in
	      let pct =
		get_perf app (refos,refgov) perfs None
		  (fun perf ->
		    Some
		      (PL.StringV
			 (Printf.sprintf "%d\\%%"
			    (int_of_float
			       (100. *. (perf.Read_data.std /. perf.Read_data.runtime)))))) in
	      PL.Pct(imprv,pct))
	    os_machines in
	(PL.StringV appstr) :: res)
      (List.sort compare !interesting_apps) in
  PL.print_table o columns [titles1;titles2;titles3] data_lines long

let print_global_table o machine apps oses perfs best improvements extra =
  let long =
    List.exists
      (fun (_,(_,appstr)) -> String.length appstr > 50)
      apps in
  let (c,t1,t2) =
    if extra
    then ([PL.Center],["Underload"],["/s"])
    else ([],[],[]) in
  let left = if long then PL.Sized 2.8 else PL.Left in
  let cut s =
    if String.length s < 25
    then s
    else
      match Str.bounded_split (Str.regexp "_") s 2 with
	[a;b] ->
	  Printf.sprintf "\\begin{tabular}{@{}c@{}}%s_\\\\%s\\end{tabular}" a b
      | _ -> s in
  let columns = left :: PL.Center :: c @ (List.map (function os -> PL.RightLeft) oses) in
  let titles1 = ("Test: "^machine) :: "Better" :: t1 @ (List.map (function os -> cut(fst(os_string2 os))) oses) in
  let titles2 = "" :: "" :: t2 @ (List.map (function os -> snd(os_string2 os)) oses) in
  let nothing = PL.StringV "---" in
  let data_lines =
    List.mapi
      (fun i (str,app) ->
	let best = List.assoc app best in
	let better = ref "unk." in
	let res =
	  List.map
	    (function os ->
	      get_perf app os perfs nothing
		(fun perf ->
		  (if perf.Read_data.lower_is_better
		  then better := "L"
		  else better := "H");
		  let perf = perf.Read_data.runtime in
		  let perf =
		    if Some perf = best
		    then PL.Bold (printperf perf)
		    else printperf perf in
		  get_imprv app os improvements (PL.Pct(perf,None))
		    (fun (imprv,_) ->
		      let imprv =
			if imprv > 1.05 then PL.Color("gr",PL.FloatV2 imprv)
			else if imprv < 0.95 then PL.Color("red",PL.FloatV2 imprv)
			else PL.FloatV2 imprv in
		      PL.Pct(perf,Some imprv))))
	    oses in
	let (app,appstr) = app in
	let appstr =
	  if app <> appstr
	  then Printf.sprintf "%s: %s" app appstr
	  else appstr in
	let a = if extra then [PL.StringV str] else [] in
	(PL.StringV (Printf.sprintf "%d. %s" (i+1) appstr)) :: (PL.StringV !better) :: a @ res)
      apps in
  PL.print_table o columns [titles1;titles2] data_lines long

let best_and_worst_improvements machine_table =
  let ymax = ref 0. in
  let ymin = ref 1. in
  List.iter
    (function (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
      List.iter
	(fun os ->
	  List.iter
	    (fun app ->
	      get_imprv app os improvements ()
		(fun (imprv,_) ->
		  ymax := max !ymax imprv;
		  ymin := min !ymin imprv))
	    apps)
	oses)
    machine_table;
  (!ymax,!ymin)

let good_and_bad_improvements machine_table = (* count the low and high improvements *)
  let xmax = ref 0 in
  let xmin = ref 0 in
  List.iter
    (function (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
      List.iter
	(fun os ->
	  let (good,bad) =
	    List.fold_left
	      (fun (gd,bd) app ->
		get_imprv app os improvements (gd,bd)
		  (fun (imprv,_) ->
		    if imprv < 0.95
		    then (gd,bd+1)
		    else if imprv > 1.05
		    then (gd+1,bd)
		    else (gd,bd)))
	      (0,0) apps in
	  xmax := max !xmax good;
	  xmin := max !xmin bad)
	oses)
    machine_table;
  (!xmax,0 - !xmin)

let print_global_graph o apps oses refos improvements xmax xmin ymax ymin exclude_middle left right center =
  let unref_oses = List.filter (fun x -> x <> refos) oses in
  let localxmax = ref 0 in
  let localpmax = ref 22 in (* hack! *)
  let all_improvements_per_os =
    List.map
      (fun os ->
	let ai =
	  List.fold_left
	    (fun prev app ->
	      get_imprv app os improvements prev
		(fun (imprv,_) -> imprv :: prev))
	    [] apps in
	let aipos = List.filter (fun x -> x > 1.05) ai in
	localpmax := max (List.length aipos) !localpmax;
	localxmax := max (List.length ai) !localxmax;
	(os,ai))
      unref_oses in
  let ymax = pct ymax in
  let ymin = pct ymin in
  let width = if exclude_middle then 0.2 else 1. in
  let height = if center then 2. else 1.5 in
  let xmax =
    if exclude_middle
    then !localpmax
    else !localxmax in
  Printf.fprintf o "%s\\begin{tikzpicture}\\begin{axis}[ytick distance=20, ymax = %f, ymin = %f, xmin = %d, xmax = %d,\nwidth = %f\\textwidth, height = %fin,\n"
    (if center then "\\[" else "")
    ymax ymin xmin xmax width height;
  (if left || not exclude_middle then Printf.fprintf o "     ylabel={\\%% improvement},\n" else Printf.fprintf o "     ylabel={},\n");
  (if right || not exclude_middle then Printf.fprintf o "     legend style={legend columns=1,at={(1,0.5)},anchor=west}");
  Printf.fprintf o "     ]\n";
  let edge_colors = ["blue";"red";"gr";"black";"cyan";"purple"] in
  let infos =
    List.fold_left
      (fun prev (i,os) ->
	let all_improvements = List.sort compare (List.assoc os all_improvements_per_os) in
	let slow = List.filter (fun x -> x < 0.95) all_improvements in
	let slowlen = List.length slow in
	let middle = List.filter (fun x -> x >= 0.95 && x <= 1.05) all_improvements in
	let middlelen = List.length middle in
	let fast = List.filter (fun x -> x > 1.05) all_improvements in
	let fastlen = List.length fast in
	let slowinc =
	  if exclude_middle
	  then 0 - slowlen
	  else 1 in
	let slowi = List.mapi (fun i x -> (i+slowinc,pct x)) slow in
	let middleinc =
	  if exclude_middle
	  then 0 (* not used *)
	  else slowlen + 1 in
	let middlei = List.mapi (fun i x -> (i+middleinc,pct x)) middle in
	let fastinc =
	  if exclude_middle
	  then 1
	  else slowlen + middlelen + 1 in
	let fasti = List.mapi (fun i x -> (i+fastinc,pct x)) fast in
	let color = List.nth edge_colors (i mod (List.length edge_colors)) in
	(if exclude_middle
	then
	  begin
	    Printer.tikz_edge_plot o color slowi 0 0. false;
	    Printer.tikz_edge_plot o color fasti 0 0. true;
	    (if List.length slow = 1 then Printer.tikz_mark_plot o color "*" slowi);
	    (if List.length fast = 1 then Printer.tikz_mark_plot o color "*" fasti)
	  end
	else
	  begin
	    Printer.tikz_edge_plot o color (slowi @ middlei @ fasti) 0 0. false;
	    (match List.rev slowi with x::xs -> Printer.tikz_mark_plot o "red" "x" [x] | _-> ());
	    (match fasti with x::xs -> Printer.tikz_mark_plot o "gr" "x" [x] | _-> ())
	  end);
	let refinedveryslowlen = List.length (List.filter (fun x -> x < 0.8) all_improvements) in
	let refinedslowlen = List.length (List.filter (fun x -> x < 0.95 && x >= 0.8) all_improvements) in
	let refinedfastlen = List.length (List.filter (fun x -> x > 1.05 && x <= 1.2) all_improvements) in
	let refinedveryfastlen = List.length (List.filter (fun x -> x > 1.2) all_improvements) in
	(i,os,(slowlen,middlelen,fastlen,refinedveryslowlen,refinedslowlen,refinedfastlen,refinedveryfastlen)) :: prev)
      [] (List.mapi (fun i os -> (i,os)) unref_oses) in
  let (xmin,xmax) =
    if exclude_middle then (xmin,xmax) else (0 - !localxmax,!localxmax) in
  let ln color y =
    Printf.fprintf o "\\addplot[%sdotted,sharp plot,update limits=false] coordinates {(%d,%d) (%d,%d)};\n"
      color xmin y xmax y in
  ln "" 0;
  (if ymax >= 5. then ln "red," 5);
  (if ymin <= (-5.) then ln "red," (-5));
  List.iter
    (fun (i,os,(slow,middle,fast,_,_,_,_)) ->
      let color = List.nth edge_colors (i mod (List.length edge_colors)) in
      Printf.fprintf o "\\node[right] at (%f,%f) {%s};\n"
	((float_of_int xmin) +. ((float_of_int(xmax-xmin)) *. 0.05))
	((ymax *. 0.9) -. (ymax *. (float_of_int i) *. 0.1))
	(Printf.sprintf "\\textcolor{%s}{%d unchanged}" color middle))
    infos;
  let legend = List.map (fun os -> PL.clean(short_os_string os)) unref_oses in
  (if right || not exclude_middle then Printf.fprintf o "\\legend{%s}\n" (String.concat "," legend));
  Printf.fprintf o "\\end{axis}\\end{tikzpicture}%s\n\n" (if center then "\\]" else "");
  if center
  then
    let columns = [PL.Left;PL.RightLeft;PL.RightLeft;PL.RightLeft;PL.RightLeft;PL.RightLeft] in
    let titles1 = ["OS";"slower by more than 20\\%";"slower by (5,20]\\%";"same";"faster by (5,20]\\%";"faster by more than 20\\%"] in
    let data_lines =
      List.map
	(function (i,os,(slow,middle,fast,refinedveryslowlen,refinedslowlen,refinedfastlen,refinedveryfastlen)) ->
	  let numbers = [refinedveryslowlen;refinedslowlen;middle;refinedfastlen;refinedveryfastlen] in
	  let sum = List.fold_left (+) 0 numbers in
	  (PL.StringV (os_string os)) :: (List.map (fun x -> PL.Pct(PL.IntV x, Some(PL.StringV(Printf.sprintf "%d\\%%" (int_of_float ((float_of_int(x*100)) /. (float_of_int sum))))))) numbers))
	infos in
    PL.print_table o columns [titles1] data_lines false

let print_power_table o machine apps oses refos perfs best gettype getter =
  let long =
    List.exists
      (fun (_,appstr) -> String.length appstr > 50)
      apps in
  let left = if long then PL.Sized 2.8 else PL.Left in
  let columns = left :: (List.map (function os -> PL.RightLeft) oses) in
  let titles1 = ("Test: "^machine) :: (List.map (function os -> fst (os_string2 os)) oses) in
  let titles2 = "" :: (List.map (function os -> snd (os_string2 os)) oses) in
  let nothing = PL.StringV "---" in
  let data_lines =
    List.map
      (function app ->
	let best = List.assoc app best in
	let res =
	  List.map
	    (function os ->
	      get_perf app os perfs nothing
		(fun perf ->
		  let perf = perf.Read_data.runtime in
		  gettype app os perfs nothing
		    (fun pwr ->
		      let power = getter pwr in
		      let perf =
			if Some perf = best
			then PL.Bold (printperf power)
			else printperf power in
		      if os = refos
		      then perf
		      else
			gettype app refos perfs nothing
			  (fun pwr ->
			    let refpower = getter pwr in
			    let imprv = refpower /. power in
			    let imprv =
			      if imprv > 1.05 then PL.Color("gr",PL.FloatV2 imprv)
			      else if imprv < 0.95 then PL.Color("red",PL.FloatV2 imprv)
			      else PL.FloatV2 imprv in
			    PL.Pct(perf,Some imprv)))))
	    oses in
	let (_,appstr) = app in
	(PL.StringV appstr) :: res)
      apps in
  PL.print_table o columns [titles1;titles2] data_lines long

let power_vs_speedup o machine apps oses refos perfs improvements gettype getter =
  let unref_oses = List.filter (fun x -> x <> refos) oses in
  let long =
    List.exists
      (fun (_,appstr) -> String.length appstr > 50)
      apps in
  let left = if long then PL.Sized 2.8 else PL.Left in
  let columns = left :: (List.map (function os -> PL.RightLeft) unref_oses) in
  let titles1 =
    ("Test: power improvement vs runtime speedup: "^machine) ::
    (List.map (function os -> fst (os_string2 os)) unref_oses) in
  let titles2 = "" :: (List.map (function os -> snd (os_string2 os)) unref_oses) in
  let nothing = PL.StringV "---" in
  let data_lines =
    List.map
      (function app ->
	let res =
	  List.map
	    (function os ->
	      gettype app os perfs nothing
		(fun pwr ->
		  let power = getter pwr in
		  gettype app refos perfs nothing
		    (fun pwr ->
		      let refpower = getter pwr in
		      let powerimprv = refpower /. power in
		      get_imprv app os improvements nothing
			(fun (imprv,_) ->
			  PL.StringV
			    (Printf.sprintf "%0.4f\\%%" (powerimprv /. imprv))))))
	    unref_oses in
	let (_,appstr) = app in
	(PL.StringV appstr) :: res)
      apps in
  PL.print_table o columns [titles1;titles2] data_lines long

let get_counters perfs =
  let ctrs = ref [] in
  List.iter
    (function (app,perfs) ->
      List.iter
	(function (os,perf) ->
	  let lst = perf.ctrinfo in
	  List.iter
	    (function ctr ->
	      let ev = ctr.Read_data.event in
	      if not (List.mem ev !ctrs)
	      then ctrs := ev :: !ctrs)
	    lst)
	perfs)
    perfs;
  List.sort compare !ctrs

let print_abbreviation_table o apps perfs =
  let columns = [PL.Left;PL.Left] in
  let titles = ["Abbreviation";"Command"] in
  let data_lines =
    List.rev
    (List.fold_left
      (fun prev app ->
	let perfs = List.assoc app perfs in
	let command =
	  let rec loop = function
	      (_,info)::rest ->
		(match info.perfinfo with
		  None -> loop rest
		| Some perf -> perf.Read_data.command)
	    | [] -> "---" in
	  loop perfs in
	let (_,appstr) = app in
	if appstr = command
	then prev
	else [PL.StringV appstr; PL.StringV command] :: prev)
      [] apps) in
  PL.print_table o columns [titles] data_lines false

let print_os_name_table o os_strings =
  let columns = [PL.Left;PL.Left] in
  let titles = ["Abbreviation";"OS"] in
  let os_strings =
    List.map
      (fun (str,(os,gov)) ->
	[PL.StringV str; PL.StringV (Printf.sprintf "%s %s\n" os gov)])
      os_strings in
  PL.print_table o columns [titles] (List.sort compare os_strings) false

let print_os_table o machine apps os refos perfs best improvements haspower =
  let isref = os = refos in
  let union l1 l2 =
    List.fold_left
      (fun prev x -> if List.mem x prev then prev else x :: prev)
      l2 l1 in
  let frequencies =
    let rec loop = function
	(app,infos)::apps ->
	  let rec iloop = function
	      (os,info)::rest ->
		(match info.freqinfo with
		  None -> iloop rest
		| Some f ->
		    union
		      (List.map (fun f -> (f.Read_data.low,f.Read_data.high)) f)
		      (iloop rest))
	    | [] -> loop apps in
	  iloop infos
      | [] -> [] in
    loop perfs in
  let frequencies = List.sort compare frequencies in
  let columns =
    PL.Left :: PL.RightCenterLeft :: (if isref then [] else [PL.Center]) @ (if haspower then (if !show_ram then [PL.RightCenterLeft; PL.RightCenterLeft] else [PL.RightCenterLeft]) else []) @
    (if !show_over_under then [PL.Right; PL.Right; PL.RightLeft] else []) @ (List.map (function os -> PL.Center) frequencies) in
  let titles1 =
    (Printf.sprintf "Test: %s" machine) ::
    "Runtime" :: (if isref then [] else ["`Speedup"]) @ (if haspower then (["CPU power"] @ if !show_ram then ["RAM power"] else []) else []) @ (if !show_over_under then ["Under"; "Over"; "WinVis"] else []) @
    (List.map (function (low,_) -> Printf.sprintf "%d.%d --" (low/1000000) ((low/100000) mod 10)) frequencies) in
  let titles2 =
    (Printf.sprintf "%s-%s" (fst os) (snd os)) :: "" :: (if isref then [] else [""]) @ (if haspower then (if !show_ram then [""; ""] else [""]) else []) @ (if !show_over_under then ["/s"; "/s"; ""] else []) @
    (List.map (function (_,high) -> Printf.sprintf "%d.%dGHz" (high/1000000) ((high/100000) mod 10)) frequencies) in
  let nothing = PL.StringV "---" in
  let data_lines =
    List.rev
      (List.fold_left
	 (fun prev app ->
	   let best  = List.assoc app best in
	   try
	     let getpct std perf =
	       let pct_std = int_of_float((100. *. std) /. perf) in
	       let spct_std = PL.StringV(Printf.sprintf "%i\\%%" pct_std) in
	       if pct_std > 40
	       then PL.Color("red",spct_std)
	       else if pct_std > 20
	       then PL.Color("orange",spct_std)
	       else spct_std in
	     let (perf,imprv) =
	       get_perf app os perfs (nothing,nothing)
		 (function perf ->
		   let std  = perf.Read_data.std in
		   let perf = perf.Read_data.runtime in
		   let pct_std = getpct std perf in
		   let perf =
		     if Some perf = best
		     then PL.Bold (printperf perf)
		     else printperf perf in
		   get_imprv app os improvements (PL.PM(perf,std,pct_std),nothing)
		     (function (imprv,_) ->
		       let imprv =
			 if imprv > 1.05 then PL.Color("gr",PL.FloatV2 imprv)
			 else if imprv < 0.95 then PL.Color("red",PL.FloatV2 imprv)
			 else PL.FloatV2 imprv in
		       (PL.PM(perf,std,pct_std), imprv))) in
	     let power_cpu =
	       get_power app os perfs nothing
		 (function perf ->
		   let std  = perf.Read_data.cpu_joules_std in
		   let perf = perf.Read_data.cpu_joules_avg in
		   PL.PM(printperf perf,std,getpct std perf)) in
	     let power_ram =
	       get_power app os perfs nothing
		 (function perf ->
		   let std  = perf.Read_data.ram_joules_std in
		   let perf = perf.Read_data.ram_joules_avg in
		   PL.PM(printperf perf,std,getpct std perf)) in
	     let auc =
	       if !show_over_under
	       then
	         get_auc app os perfs [nothing;nothing;nothing]
		   (function auc ->
		     let red_blue v =
		       if v > 1.
		       then PL.Color("red",PL.FloatV4 v)
		       else if v > 0.1
		       then PL.Color("blue",PL.FloatV4 v)
		       else PL.FloatV4 v in
		     let wv =
		       let wv = auc.Read_data.window in
		       let other = auc.Read_data.other in
		       if wv < 0
		       then nothing
		       else if wv + other = 0
		       then PL.IntV wv
		       else
		         let pct =
			   Printf.sprintf "%d\\%%"
			   ((  wv * 100) / (wv + other)) in
		         PL.Pct(PL.IntV wv,Some(PL.StringV pct)) in
		      [red_blue auc.Read_data.auc;
		       red_blue (0. -. auc.Read_data.aoc); wv])
	       else [] in
	     let freq =
	       List.map
		 (function (low,high) ->
		   get_freq app os perfs nothing
		     (function freq ->
		       try
			 let f : Read_data.freq_data =
			   List.find
			     (function f -> f.Read_data.low = low && f.Read_data.high = high)
			     freq in
			 let pct = f.Read_data.pct in
			 PL.StringV (Printf.sprintf "%0.1f\\%%" pct)
		       with Not_found -> PL.StringV "---"))
		 frequencies in
	     let (_,appstr) = app in
	     ((PL.StringV appstr :: perf :: (if isref then [] else [imprv]) @ (if haspower then (if !show_ram then [power_cpu; power_ram] else [power_cpu]) else []) @ auc @ freq)) :: prev
	   with Not_found -> prev)
	 [] apps) in
  PL.print_table o columns [titles1;titles2] data_lines false

(* ------------------------------------------------------------------ *)

let replace50 files =
  let files = List.sort compare files in
  let get s file =
    match Str.split (Str.regexp s) file with
      [a;b] -> Some (a,b)
    | _ -> None in
  let rec loop acc = function
      x::((y::ys) as xs) ->
	(match (get "10runs" x,get "50runs" y) with
	  (Some(a,b),Some(c,d)) when a = c && b = d ->
	    loop (y::acc) ys
	| _ ->
	    (match (get "50runs" x,get "10runs" y) with
	      (Some(a,b),Some(c,d)) when a = c && b = d ->
		loop (x::acc) ys
	    | _ -> loop (x::acc) xs))
    | l -> List.rev(l@acc) in
  loop [] files

let organize_file_data files reference =
  let (refos,refgov) =
    match Str.split (Str.regexp "_") reference with
      [refos;refgov] -> (refos,refgov)
    | _ -> failwith "bad reference version" in
  let files = if !do_replace50 then replace50 files else files in
  let machinetbl = Hashtbl.create 101 in
  let oses = ref [] in
  let apps = ref [] in
  List.iter
    (function ofile ->
      let file = Filename.remove_extension ofile in
      let depth = Str.split (Str.regexp "/") file in
      if Filename.extension ofile = ".json"
      then (* run_everything *)
	match Str.bounded_full_split (Str.regexp "5[\\.0-9]+") file 2 with
	  [Str.Text bef;Str.Delim os;Str.Text aft] ->
	    let aft_pieces = List.rev(Str.split_delim (Str.regexp "_") aft) in
	    let gov = List.hd aft_pieces in
	    let extra = String.concat "_" (List.rev(List.tl aft_pieces)) in
	    let bef =
	      if !nomachine
	      then drop_number bef
	      else bef in
	    let (app,machine) =
	      let pieces = Str.split (Str.regexp "_") bef in
	      match List.rev pieces with
		[app] -> (app,"i80")
	      | m::app -> (String.concat "_" (List.rev app),m)
	      | [] -> failwith "bad filename" in
	    let os = os^extra in
	    (if not (List.mem (os,gov) !oses) then oses := (os,gov) :: !oses);
	    let fullapp = (app,app) in
	    (if not (List.mem fullapp !apps) then apps := fullapp :: !apps);
	    Util.hashadd machinetbl machine ((app,os,gov),Read_data.RE file)
	| _ -> failwith ("bad file name: "^ofile)
      else (* phoronix *)
	match List.rev depth with
	  filename :: _ ->
	    (* underscore to be more sure to be at version number *)
	    (match Str.bounded_full_split (Str.regexp "_5[\\.0-9]+") filename 2 with
	      [Str.Text bef;Str.Delim os;Str.Text aft] ->
		let bef = bef ^ "_" in (* shift underscore to bef *)
		let os = String.sub os 1 (String.length os - 1) in
		let n = String.length "resultfile_" in
		let app = String.sub bef n (String.length bef - n -1) in
		let aft_pieces =
		  List.rev(Str.split_delim (Str.regexp "_") aft) in
		let gov = List.nth aft_pieces 2 in
		let machine = List.nth aft_pieces 1 in
		let machine =
		  if !nomachine
		  then drop_number machine
		  else machine in
		let extra =
		  String.concat "_" (List.tl(List.tl(List.tl aft_pieces))) in
		let os = os^extra in
		let (machine,os) =
		  if !flip
		  then (os,machine)
		  else (machine,os) in
		(if not (List.mem (os,gov) !oses)
		then oses := (os,gov) :: !oses);
		let appnames =
		  Util.cmd_to_list
		    ("grep -B1 \"Results Are Better\" "^ofile^" | grep -v \"Results Are Better\" | grep ^'\"'") in
		let appnames =
		  List.map (* drop quotes *)
		    (fun x -> Read_data.unquote(Read_data.uncomma x))
		    appnames in
		(* app is derived from the file name, appstr is the name
		   of the test *)
		List.iter
		  (function nm ->
		    let app = (app,nm) in
		    if not (List.mem app !apps) then apps := app :: !apps)
		  appnames;
		Util.hashadd machinetbl machine
		  ((app,os,gov),Read_data.PHORONIX(file,Read_data.mkphoronixtrace file))
	    | _ -> failwith ("bad file name: "^ofile))
	| _ -> failwith ("bad file name: "^ofile))
    files;
  let oses =
    let (refos,others) = List.partition (fun x -> x = (refos,refgov)) !oses in
    refos @ (osorder others) in
  let apps = List.sort compare !apps in
  let apps =
    List.filter (fun (name,appstr) -> not (List.mem appstr !toignore)) apps in
  let machine_files =
    Hashtbl.fold
      (fun machine files r ->
	(machine_index machine,(machine, List.sort compare !files)) :: r)
      machinetbl [] in
  let machine_files = List.map snd (List.sort compare machine_files) in
  List.map
    (fun (machine,files) ->
      let perfs = (* map app to stats for each os *)
	let mapper = (* to see stderr in quick case ... *)
	  (*if !quick || !Read_data.make_do || !forcemap
	  then*) (fun f -> List.map f apps)
	  (*else (fun f -> Parmap.parmap ~ncores:44 ~chunksize:1 f (Parmap.L apps))*) in
	let imapper = (* to see stderr in quick case ... *)
	  (*if !quick || !Read_data.make_do || !forcemap || (List.length apps) * (List.length oses) > 70
	  then*) (fun f -> List.map f oses) (* could check available space in /tmp *)
	  (*else (fun f -> Parmap.parmap ~ncores:44 ~chunksize:1 f (Parmap.L oses))*) in
	mapper
	  (function ((name,appstr) as app) ->
	    (app,
	     imapper
	       (function (os,gov) ->
		 try
		   let fl = List.assoc (name,os,gov) files in
		   match Read_data.get_csv_data fl appstr ((refos,refgov) = (os,gov)) with
		     Some csv ->
		       let n = csv.Read_data.position in
		       if !quick
		       then
			 ((os,gov),
			  { perfinfo = Some csv;
			    ctrinfo = Read_data.get_perf_data fl n;
			    aucinfo = None; freqinfo = None;
			    eventinfo = None; miginfo = None; turboinfo = None })
		       else
			 ((os,gov),
			  { perfinfo = Some csv;
			    ctrinfo = Read_data.get_perf_data fl n;
			    aucinfo = Read_data.get_auc_data fl n !show;
			    freqinfo = Read_data.get_freq_data fl n !show;
			    eventinfo = Read_data.get_event_data fl n;
			    miginfo = Read_data.get_mig_data fl n;
			    turboinfo = Read_data.get_turbo_data fl })
		   | None ->
		       ((os,gov),
			{ perfinfo = None; ctrinfo = []; aucinfo = None; freqinfo = None;
			  eventinfo = None; miginfo = None; turboinfo = None })
		 with Not_found ->
		   ((os,gov),
		    { perfinfo = None; ctrinfo = []; aucinfo = None; freqinfo = None;
		      eventinfo = None; miginfo = None; turboinfo = None })))) in
      let more_perfs =
	let found_any = ref false in
	let res =
	  List.concat
	    (List.map
	       (function app ->
		 let appinfo =
		   List.map
		     (function (os,gov) ->
		       try
			 let fl = List.assoc (fst app,os,gov) files in
			 (match Read_data.get_dacapo_stderr fl (fst app) Read_data.JSON with
			   None ->
			     ((os,gov),
			      { perfinfo = None;
				ctrinfo = []; aucinfo = None; freqinfo = None;
				eventinfo = None; miginfo = None; turboinfo = None })
			 | Some dac ->
			     found_any := true;
			     ((os,gov),
			      { perfinfo = Some dac;
				ctrinfo = []; aucinfo = None; freqinfo = None;
				eventinfo = None; miginfo = None; turboinfo = None }))
			      with Not_found ->
				((os,gov),
				 { perfinfo = None;
				   ctrinfo = []; aucinfo = None; freqinfo = None;
				   eventinfo = None; miginfo = None; turboinfo = None }))
		     oses in
		 if appinfo = []
		 then []
		 else [(((fst app),(snd app^"_last")),appinfo)])
	       apps) in
	if !found_any
	then res
	else [] in
      let perfs = perfs @ more_perfs in
      let apps = apps @ (List.map fst more_perfs) in
      let best = (* map app to best mean runtime over all oses *)
	List.map
	  (function app ->
	    let perfs =
	      List.map snd (List.assoc app perfs) in
	    (app,
	     List.fold_left
	       (fun mn info ->
		 match info.perfinfo with
		   None -> mn
		 | Some perf ->
		     match (mn,perf.Read_data.runtime) with
		       None,x -> Some x
		     | Some mn,x ->
			 let comparer =
			   if perf.Read_data.lower_is_better then min else max in
			 Some(comparer x mn))
	       None perfs))
	  apps in
      let co = open_out ("slower_"^machine^".csv") in
      let fo = open_out ("faster_"^machine^".csv") in
      let seen_slower = ref [] in
      let seen_faster = ref [] in
      Printf.fprintf co "suite,scheduler,governor\n";
      let improvements =
	List.map
	  (function app ->
	    (app,
	     List.map
	       (fun os ->
		 (os,
		  if os = (refos,refgov)
		  then None
		  else
		    get_perf app os perfs None
		      (fun perf ->
			get_perf app (refos,refgov) perfs None
			  (fun refperf ->
			    let values = perf.Read_data.values in
			    let refavg = refperf.Read_data.runtime in
			    let (speedup,std) = 
			      Util.speedup_and_std perf.Read_data.lower_is_better refavg values in
			    let entry = (fst app,fst os,snd os) in
			    (if speedup < 0.95 && not(List.mem entry !seen_slower)
			    then
			      begin
				seen_slower := entry :: !seen_slower;
				Printf.fprintf co "%s,%s,%s\n" (fst app) (fst os) (snd os)
			      end);
			    (if speedup > 1.05 && not(List.mem entry !seen_faster)
			    then
			      begin
				seen_faster := entry :: !seen_faster;
				Printf.fprintf fo "%s,%s,%s\n" (fst app) (fst os) (snd os)
			      end);
			    Some(speedup,std)))))
	       oses))
	  apps in
      close_out co;
      close_out fo;
      (machine,(refos,refgov,oses,apps,perfs,best,improvements)))
    machine_files

let output = ref ""

let check_production o machine ext fn =
  fn o true;
  if !production <> ""
  then
    begin
      let ext = (* don't put latex in file names *)
	let pieces = Str.split (Str.regexp "\\") ext in
	let ext = String.concat "" pieces in
	let pieces = Str.split (Str.regexp "{}") ext in
	String.concat "" pieces in
      let fl =
	Printf.sprintf "%s_%s_%s.tex"
	  machine (Filename.remove_extension !output) ext in
      let o = open_out fl in
      fn o false;
      close_out o;
      ignore(Sys.command (Printf.sprintf "mv %s %s" fl !production))
    end

let rec process_collections o collections str ext machine_table forpaper mult fn =
  let machines = List.length machine_table in
  let any_relevant = ref false in
  List.iter
    (function ((anm,maxvalue,size,app_collection),(onm,os_collection)) ->
      (* are the collections relevant for this test suite? *)
      let relevant =
	List.exists
	  (fun (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
	    (List.exists
	       (fun os -> List.mem os os_collection)
	       oses) &&
	    (List.exists
	       (fun (_,app) -> List.mem app app_collection)
	       apps))
	  machine_table in
      if relevant || (app_collection = [] && os_collection = [])
      then
	begin
	  any_relevant := true;
	  (if not forpaper
	  then
	    if anm = ""
	    then Printf.fprintf o "\\paragraph*{%s: %s}\n\n" str onm
	    else Printf.fprintf o "\\paragraph*{%s: %s -- %s}\n\n" str onm anm);
	  let (big,width,cstr) =
	    if List.length app_collection = 1
	    then (false,Printer.TINY,"cc")
	    else
	      match size with
		None ->
		  if (List.length app_collection * mult > 12) || (app_collection = [] && mult > 1)
		  then (true,Printer.BIG,"c")
		  else
		    let middle_width =
		      if List.length app_collection * mult > 20
		      then 2
		      else 3 in
		    (false,Printer.MIDDLE middle_width,"cc")
	      | Some Printer.BIG -> (true,Printer.BIG,"c")
	      | Some sz -> (false,sz,"cc") in
	  (if forpaper then Printf.fprintf o "\\begin{figure*}\n");
	  Printf.fprintf o "\\begin{center}\\begin{tabular}{%s}\n" cstr;
	  let print i machine (refos,refgov,oses,apps,perfs,best,improvements) right =
	    let apps =
	      if app_collection = []
	      then apps
	      else List.concat (List.map (fun app -> try [List.find (fun (_,a) -> a = app) apps] with Not_found -> []) app_collection) in
	    let oses =
	      if os_collection = []
	      then oses
	      else List.filter (fun os -> List.mem os oses) os_collection in
	    let last =
	      if big
	      then i = machines - 1
	      else if machines mod 2 = 0
	      then i >= machines - 2
	      else i = machines - 1 in
	    let first = if big then i = 0 else i <= 1 in
	    check_production o machine
	      (Printf.sprintf "%s_%s_%s" ext
		 (String.concat "_" (Str.split (Str.regexp " ") onm))
		 (String.concat "_" (Str.split (Str.regexp " ") anm)))
	      (fn machine (refos,refgov,oses,apps,perfs,best,improvements) maxvalue width first last right) in
	  let rec loop i = function
	      (machine1,info1)::(machine2,info2)::rest when not big ->
		Printf.fprintf o "%s&%s\\\\\n" (machine_string machine1)
		  (machine_string machine2);
		print i machine1 info1 false;
		Printf.fprintf o "&\n";
		print (i+1) machine2 info2 true;
		(if rest <> [] then Printf.fprintf o "\\\\\n");
		loop (i+2) rest
	    | (machine1,info1)::rest ->
		Printf.fprintf o "%s%s\\\\\n" (machine_string machine1)
		  (if big then "" else "&");
		print i machine1 info1 false;
		(if not big then Printf.fprintf o "&\n");
		(if rest <> [] then Printf.fprintf o "\\\\\n");
		loop (i+1) rest
	    | [] -> () in
	  loop 0 machine_table;
	  Printf.fprintf o "\\end{tabular}\\end{center}\n";
	  (if forpaper
	  then
	    (if anm = ""
	    then Printf.fprintf o "\\caption{%s: %s}\n\\end{figure*}\n" str onm
	    else Printf.fprintf o "\\caption{%s: %s -- %s}\n\\end{figure*}\n" str onm anm))
	end)
    collections;
  if not !any_relevant
  then
    begin
      any_relevant := true; (* don't loop *)
      let (app_collection,os_collection) =
	let (machine,(refos,refgov,oses,apps,perfs,best,improvements)) = List.hd machine_table in
	(List.map snd apps,oses) in
      let sz = if !winlose then Some (Printer.MIDDLE 2) else None in (* hack! what we want for phoronix *)
      process_collections o [(("",Free(0.,0.),sz,app_collection),("",os_collection))] str ext machine_table forpaper mult fn
    end

let collect_missing_data machine_table =
  let o = open_out (Printf.sprintf "missing_data.csv") in
  List.iter
    (function (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
      Printf.fprintf o "suite,test,machine,%s\n" (String.concat "," (List.map (function (os,gov) -> Printf.sprintf "%s-%s" os gov) oses));
      List.iter
	(fun app ->
	  let avails =
	    List.map
	      (fun os ->
		get_all_perf app os perfs "missing"
		  (fun perf ->
		    let std_pct = perf.Read_data.std /. perf.Read_data.runtime in
		    if std_pct > 0.15
		    then
		      let fifty =
			match perf.Read_data.partial with
			  Some n ->
			    if n > 10
			    then Printf.sprintf " %d" n
			    else Printf.sprintf ""
			| None -> "" in
		      Printf.sprintf "%d%% std%s" (int_of_float(std_pct *. 100.)) fifty
		    else
		      match perf.Read_data.partial with
			Some n ->
			  if n > 10
			  then Printf.sprintf "ok %d" n
			  else if n < 8
			  then Printf.sprintf "partial %d" n
			  else Printf.sprintf "ok"
		      | None -> "ok"))
	      oses in
	  Printf.fprintf o "%s,\"%s\",%s,%s\n" (fst app) (snd app) machine (String.concat "," avails))
	apps)
    machine_table;
  close_out o

let anydata machine_table gettype =
  List.exists
    (function (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
      List.exists
	(fun app ->
	  List.exists
	    (fun os ->
	      gettype app os perfs false
		(fun v -> true))
	    oses)
	apps)
    machine_table

(* ------------------------------------------------------------------ *)
(* find maximum values for consistent graphs *)

let max_value gettype getter machine_table =
  let largest = ref 0.0 in
  let smallest = ref 1.0 in
  List.iter
    (function (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
      let refos = (refos,refgov) in
      let unref_oses = List.filter (fun x -> x <> refos) oses in
      List.iter
	(fun os ->
	  List.iter
	    (fun app ->
	      gettype app os perfs ()
		(fun osvalue ->
		  let (_,_,osvalues) = getter osvalue in
		  gettype app refos perfs ()
		    (fun refosvalue ->
		      let (refosvalue,_,_) = getter refosvalue in
		      let (imprv,impstd) = Util.speedup_and_std true refosvalue osvalues in
		      let v = pct imprv in
		      let std = impstd *. 100. in
		      largest := max (v +. std) !largest;
		      smallest := min (v -. std) !smallest)))
	    apps)
	unref_oses)
    machine_table;
  (!largest,!smallest)

let max_imprv gettype getter machine_table =
  let largest = ref 0.0 in
  let smallest = ref 1.0 in
  List.iter
    (function (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
      let refos = (refos,refgov) in
      let unref_oses = List.filter (fun x -> x <> refos) oses in
      List.iter
	(fun os ->
	  List.iter
	    (fun app ->
	      get_imprv app os improvements ()
		(fun (imprv,impstd) ->
		  let v = pct imprv in
		  let std = impstd *. 100. in
		  largest := max (v +. std) !largest;
		  smallest := min (v -. std) !smallest))
	    apps)
	unref_oses)
    machine_table;
  (!largest,!smallest)

(* ------------------------------------------------------------------ *)

let options = [
(*  "--geomean", Arg.Set geomean, "  compute geomeans";*)
  "--nomachine", Arg.Set nomachine,
  "  ignore difference between the grid 5000 machines";
  "--quick", Arg.Set quick, "  just the first table";
  "--acm-only", Arg.Set acm_only, "  acm format only";
  "--force-map", Arg.Set forcemap, "  no parallelism - more output";
  "--debug", Arg.Set debug, "  show some extra output";
  "--make-do", Arg.Set Read_data.make_do, "  just use available data";
  "--production", Arg.Set_string production, "  put files in paper directory";
  "--show-list", Arg.Set_string show, "  commands of interest (phoronix)";
  "--flip", Arg.Set flip, "  exchange machine name and scheduler name";
  "--ignore", Arg.String (fun s -> toignore := s :: !toignore), "  tests to ignore";
  "--winlose", Arg.Int (fun n -> winlose := true; summary_threshold := (float_of_int n) /. 100.),
  "  consider only apps where some scheduler gives an improvement or degradation of at least N%";
  "--no-replace-50", Arg.Clear do_replace50, "  ignore 50 run files"
]
    
let reference = ref ""
let files = ref []

let anonymous s =
  if !output = "" then output := s
  else if !reference = "" then reference := s
  else files := s :: !files

let usage = "read_csv output reference cvss [options]"

let check_output s =
  let ext = Filename.extension s in
  let pieces = Str.split_delim (Str.regexp "/") s in
  if not(ext = ".tex" && List.length pieces = 1)
  then failwith "The first argument must be a .tex file in the current directory"

let _ =
  Arg.parse (Arg.align options) anonymous usage;
  let output = !output in
  check_output output;
  let toutput = (Filename.chop_extension output) ^ "_tables.tex" in
  let aoutput = (Filename.chop_extension output) ^ "_acm.tex" in
  let machine_table = organize_file_data !files !reference in
  collect_missing_data machine_table;
  let o = open_out output in
  let t = open_out toutput in
  let a = open_out aoutput in
  Printer.latex_starter o;
  Printer.latex_starter t;
  Printer.acm_latex_starter a;
  let (ymax,ymin) = best_and_worst_improvements machine_table in
  let (xmax,xmin) = good_and_bad_improvements machine_table in
  Printf.fprintf o "\\paragraph*{Summary of cases that are better or worse by at least %d\\%% than the reference scheduler (%s)}\n\n"
    (int_of_float(100. *. !summary_threshold)) (PL.clean !reference);
  let interesting_apps =
    let machine_table =
      if !winlose
      then List.filter (fun (machine,_) -> machine <> "i80") machine_table
      else machine_table in
    print_summary_table o machine_table in
  let machine_table =
    if !winlose
    then
      List.map
	(fun (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
	  (machine,(refos,refgov,oses,interesting_apps,perfs,best,improvements)))
	machine_table
    else machine_table in
  Printf.fprintf o "\\paragraph*{High standard deviations: shows improvement of nest-schedutil, then \\%% standard deviation}\n\n";
  print_high_stddev_table o machine_table;
  List.iteri
    (fun i (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
      let aucapps = List.map (fun app -> ("",app)) apps in
      let oses4 = pack_of_four oses 6 in
      Printf.fprintf t "\\paragraph*{Performance: %s: %s}\n\n" (PL.clean output) machine;
      List.iter
	(function oses ->
	  print_global_table t machine aucapps oses perfs best improvements false)
	oses4;
      Printf.fprintf t "\\paragraph*{Speedups: %s: %s}\n\n" (PL.clean output) machine;
      print_global_graph t apps oses (refos,refgov) improvements (List.length apps) 1 ymax ymin false (i=0) (i = (List.length machine_table)-1) true;
      Printf.fprintf o "\\paragraph*{Speedups: %s: %s}\n\n" (PL.clean output) machine;
      check_production o machine "pspeedup"
	(fun o center ->
	  print_global_graph o apps oses (refos,refgov) improvements xmax xmin ymax ymin true (i=0) (i = (List.length machine_table)-1) center))
    machine_table;
  (if not !quick
  then
    List.iter
      (function (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
	let aucapp =
	  List.map
	    (function app ->
	      (get_auc app (refos,refgov) perfs (-1.)
		 (function auc -> auc.Read_data.auc),
	       app))
	    apps in
	let apps = List.rev(List.sort compare aucapp) in
	let some_underloads = ref false in
	let apps =
	  List.map
	    (function (auc,app) ->
	      if auc < 0.
	      then ("unk.",app)
	      else
		begin
		  some_underloads := true;
		  (Printf.sprintf "%0.2f" auc,app)
		end)
	    apps in
	if !some_underloads
	then
	  begin
	    let oses4 = pack_of_four oses 6 in
	    Printf.fprintf o "\\paragraph*{Performance: %s: %s ordered by decreasing underload}\n\n" (PL.clean output) machine;
	    List.iter
	      (function oses ->
		print_global_table o machine apps oses perfs best improvements true)
	      oses4
	  end)
      machine_table);
  let haspower = anydata machine_table get_power in
  (if not !quick && haspower
  then
    begin
      let get_cpu s = s.Read_data.cpu_joules_avg in
      List.iter
	(function (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
	  let oses4 = pack_of_four oses 6 in
	  Printf.fprintf t "\\paragraph*{CPU power %s: %s}\n\n" (PL.clean output) machine;
	  List.iter
	    (function oses -> print_power_table t machine apps oses (refos,refgov) perfs best get_power get_cpu)
	    oses4)
	machine_table;
      (if !show_ram
      then
        begin
          let get_ram s = s.Read_data.ram_joules_avg in
          List.iter
	    (function (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
	      let oses4 = pack_of_four oses 6 in
	      Printf.fprintf t "\\paragraph*{RAM power: %s: %s}\n\n" (PL.clean output) machine;
	      List.iter
	        (function oses -> print_power_table t machine apps oses (refos,refgov) perfs best get_power get_ram)
	        oses4)
	    machine_table
	end)(*;
      List.iter
	(function (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
	  let oses4 = pack_of_four oses 6 in
	  Printf.fprintf t "\\paragraph*{CPU power %s vs speedup: %s}\n\n" (PL.clean output) machine;
	  List.iter
	    (function oses -> power_vs_speedup t machine apps oses (refos,refgov) perfs improvements get_power get_cpu)
	    oses4)
	machine_table;
      List.iter
	(function (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
	  let oses4 = pack_of_four oses 6 in
	  Printf.fprintf t "\\paragraph*{RAM power vs speedup: %s: %s}\n\n" (PL.clean output) machine;
	  List.iter
	    (function oses -> power_vs_speedup t machine apps oses (refos,refgov) perfs improvements get_power get_ram)
	    oses4)
	machine_table *)
    end);
  let get_ctr_avg s = s.Read_data.evavg in
  List.iter
    (function (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
      let oses4 = pack_of_four oses 6 in
      let counters = get_counters perfs in
      List.iter
	(function counter ->
	  Printf.fprintf t "\\paragraph*{%s counter %s: %s}\n\n" counter (PL.clean output) machine;
	  List.iter
	    (function oses -> print_power_table t machine apps oses (refos,refgov) perfs best (get_ctrs counter) get_ctr_avg)
	    oses4)
	counters)
    machine_table;
  (if not !quick
  then
    begin
      let (largest_perf,smallest_perf) = max_imprv get_perf (fun s -> (s.Read_data.runtime,s.Read_data.values)) machine_table in
      (* paper shaped tables *)
      let imprv_fn machine ((refos,refgov,oses,apps,perfs,best,improvements) as info) maxvalue big first last right o _center =
	let (largest_perf,smallest_perf) =
	  free_fixed largest_perf smallest_perf maxvalue
	    (fun _ -> max_imprv get_perf (fun s -> (s.Read_data.runtime,s.Read_data.values)) [("",info)]) in
	graph_improvements_for_paper o machine apps oses (refos,refgov)
	  perfs improvements largest_perf smallest_perf false big first last right maxvalue in
      let cstr =
	let refos =
	  match Str.split (Str.regexp "_") !reference with
	    [refos;refgov] -> (refos,refgov)
	  | _ -> failwith "bad reference version" in
	Printf.sprintf "Performance improvements over %s" (os_string refos) in
      process_collections o collections cstr "perf" machine_table false 1 imprv_fn;
      process_collections a collections cstr "perf" machine_table true 1 imprv_fn;
      (*List.iter
	(function (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
	  Printf.fprintf t "\\paragraph*{Performance improvements: %s}\n\n" machine;
	  List.iter
	    (fun apps ->
	      graph_improvements t apps oses (refos,refgov) perfs improvements largest_perf smallest_perf)
	    (pack_of_four apps 8))
	machine_table;*)
      (*Printf.fprintf o "\\paragraph*{Pearson correlations for metrics with performance improvement}\n";
      global_correlations o machine_table;*)
      let freq_fn machine (refos,refgov,oses,apps,perfs,best,improvements) _maxvalue big first last right o _center =
	graph_freqs o machine apps oses perfs best improvements true false last right in
      let coll = (* size in collection is for improvement graphs *)
	List.map
	  (function ((anm,maxvalue,size,app_collection),(onm,os_collection)) ->
	    ((anm,maxvalue,None,app_collection),(onm,os_collection)))
	  power_collections in
      process_collections o coll "Frequencies for paper" "freq" machine_table false 4 freq_fn;
      process_collections a coll "Frequencies" "freq" machine_table true 4 freq_fn;
      let get_cpu s = (s.Read_data.cpu_joules_avg,s.Read_data.cpu_joules_std,s.Read_data.cpu_joules_values) in
      let get_ram s = (s.Read_data.ram_joules_avg,s.Read_data.ram_joules_std,s.Read_data.ram_joules_values) in
      let (largest_cpu,smallest_cpu) = max_value get_power get_cpu machine_table in
      let (largest_ram,smallest_ram) = max_value get_power get_ram machine_table in
      let cpu_fn machine ((refos,refgov,oses,apps,perfs,best,improvements) as info) maxvalue big first last right o _center =
	let (largest_power,smallest_power) =
	  free_fixed largest_cpu smallest_cpu maxvalue
	    (fun _ -> max_value get_power get_cpu [("",info)]) in
	graph_power_improvements o machine get_cpu "CPU power" apps oses (refos,refgov) perfs improvements largest_power smallest_power true big first last right false maxvalue in
      process_collections o power_collections "CPU power consumption improvements for paper" "cpu" machine_table false 1 cpu_fn;
      process_collections a power_collections "CPU power consumption improvements" "cpu" machine_table true 1 cpu_fn;
      (if !show_ram
      then
        begin
          let ram_fn machine ((refos,refgov,oses,apps,perfs,best,improvements) as info) maxvalue big first last right o _center =
	    let (largest_power,smallest_power) =
	      free_fixed largest_ram smallest_ram maxvalue
	        (fun _ -> max_value get_power get_ram [("",info)]) in
	    graph_power_improvements o machine get_ram "RAM power" apps oses (refos,refgov) perfs improvements largest_power smallest_power true big first last right false maxvalue in
          process_collections o power_collections "RAM power consumption improvements for paper" "ram" machine_table false 1 ram_fn;
          process_collections a power_collections "RAM power consumption improvements, CFS-performance and \\snest{} vs CFS-schedutil" "ram" machine_table true 1 ram_fn
        end);
(*    let largest_power = max largest_cpu largest_ram in
      let smallest_power = min smallest_cpu smallest_ram in
      List.iter
	(function (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
	  let ct = get_power (List.hd apps) (List.hd oses) perfs 0 (fun p -> p.Read_data.runs) in
	  Printf.fprintf t "\\paragraph*{CPU power consumption improvements: %s (%d data points)}\n\n" machine ct;
	  (if ct > 0
	  then
	    begin
	      List.iter
		(fun apps ->
		  graph_power_improvements t machine get_cpu "CPU power" apps oses (refos,refgov) perfs improvements largest_power smallest_power false Printer.BIG true true false true Fixed)
		(pack_of_four apps 8);
	      Printf.fprintf o "\\paragraph*{CPU power consumption improvements: %s (%d data points)}\n\n" machine
		(get_power (List.hd apps) (List.hd oses) perfs 0 (fun p -> p.Read_data.runs));
	      graph_speedup_vs_power o get_cpu "CPU power" apps oses (refos,refgov) perfs improvements largest_power smallest_power ymax ymin;
	      Printf.fprintf t "\\paragraph*{RAM power consumption improvements: %s}\n\n" machine;
	      List.iter
		(fun apps ->
		  graph_power_improvements t machine get_ram "RAM power" apps oses (refos,refgov) perfs improvements largest_power smallest_power false Printer.BIG true true false true Fixed)
		(pack_of_four apps 8);
	      Printf.fprintf o "\\paragraph*{RAM power consumption improvements: %s}\n\n" machine;
	      graph_speedup_vs_power o get_ram "RAM power" apps oses (refos,refgov) perfs improvements largest_power smallest_power ymax ymin
	    end))
	machine_table*)
    end);
  List.iter
    (function (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
      List.iter
	(function os ->
	  Printf.fprintf o "\\paragraph*{%s\\_%s: %s}\n\n" (PL.clean(fst os)) (snd os) machine;
	  print_os_table o machine apps os (refos,refgov) perfs best improvements haspower)
	oses)
    machine_table;
  List.iter
    (function (machine,(refos,refgov,oses,apps,perfs,best,improvements)) ->
      print_abbreviation_table o apps perfs)
    machine_table;
  print_os_name_table o !used_os_strings;
  (if not !quick
  then
    begin
      Printf.fprintf o
	"\\paragraph*{Legend.} Bar graphs contain the running time of the reference version.  In the improvement graph, there is also the underload and overload per second.  In the other graphs, there is the improvement.  The best performing version is noted in boldface.\n"(*;
      Printf.fprintf o
	"\\paragraph*{Pearson correlation.} -1 means an inverse correlation, 0 means no correlation, and 1 means correlation.\n"*)
    end);
  Printer.latex_ender o;
  Printer.latex_ender t;
  Printer.latex_ender a;
  close_out o;
  close_out t;
  close_out a;
  (if not !acm_only
  then
    begin
      ignore(Sys.command (Printf.sprintf "rubber -d %s %s" (if !debug then "" else "--quiet") output));
      ignore(Sys.command (Printf.sprintf "rubber -d %s %s" (if !debug then "" else "--quiet") toutput))
    end);
  ignore(Sys.command (Printf.sprintf "rubber -d %s %s" (if !debug then "" else "--quiet") aoutput))
