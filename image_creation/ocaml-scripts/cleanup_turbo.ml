(* 30 turbo files for every benchmark is hard to manage *)

(* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * file license.txt for more details.
 *)

let _ =
  let files = Util.cmd_to_list "ls *.turbo" in
  let tbl = Hashtbl.create 101 in
  List.iter
    (function file ->
      let key =
	String.concat "_"
	  (List.rev(List.tl(List.rev(Str.split (Str.regexp "_") file)))) in
      Util.hashadd tbl key file)
    files;
  Hashtbl.iter
    (fun key files ->
      try
	let values =
	  List.map
	    (function file ->
	      let i = open_in file in
	      let _ = input_line i in
	      let _ = input_line i in
	      let line = input_line i in
	      close_in i;
	      let pieces = List.rev(Str.split (Str.regexp "[ \t]+") line) in
	      match pieces with
		_::_::ram::cpu::_ ->
		  (try (float_of_string cpu,float_of_string ram)
		  with _ -> failwith (Printf.sprintf "problem in %s with %s or %s\n" file ram cpu))
	      | _ -> failwith "bad line")
	    !files in
	let ofile = Printf.sprintf "%s.turbostat" key in
	let o = open_out ofile in
	Printf.fprintf o "cpu,ram\n";
	List.iter
	  (fun (cpu,ram) -> Printf.fprintf o "%f,%f\n" cpu ram)
	  values;
	close_out o
      with End_of_file -> Printf.eprintf "missing information for %s, no output produced\n" key)
    tbl
