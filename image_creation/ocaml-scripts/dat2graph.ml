(* make a graph from a .dat file *)

(* typical usage:
dat2graph -q --print-everything file.dat # color by pid
dat2graph -q --color-by-command file.dat # color by command
*)

(* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * file license.txt for more details.
 *)

(* The first argument is a .dat file.  It can be a shell regular
expression, to process multiple files.  In this case it should be enclosed
in "".  Other arguments are --max N, --target cmd, and --sockets N, which
are described in the definition of options below. *)

let optional o f =
  match o with
    None -> ()
  | Some o -> f o

(* ----------------------------------------------------- *)

type cores_info = INFERRED | SPECIFIED

let debug = ref false
let target = ref [] (* infos of interest *)
let sockets = ref []
let migrations = ref false
let migsockets = ref 160
let save_tmp = ref false
let no_events = ref false
let no_fork_lines = ref false
let no_migrate_arrows = ref false
type migrate_type = LB|Unblock|Numa
let migrate_type = ref []
let generate_frequencies_graph = ref false
let generate_merged_graph = ref false
let get_other_events = ref false
let io_events = ref false
let print_everything = ref false
let target_functions = ref [] (* empty -> all traced functions are targetted *)
let ty = ref Printer.Jgraph
let nas_events = ref false
let others = ref false
let cores = ref (-1)
let numsockets = ref 4
let cores_info = ref INFERRED
let relaxed = ref false
let minsleep = ref (-1.0)
let waking_actor_only = ref false
let color_by_command = ref false
let top_commands = ref false
let color_by_pid = ref []
let has_requested_pids = ref false
let requested_pids = Hashtbl.create 101
let requested_command = ref None
let requested_something = ref false
let hyper = ref false
let hyperfork = ref false
type phase = Last | First | All
let print_sizes = ref false
let show = ref []
let cores_with_target = ref false
let highlight_non_targets = ref false
let after_sleep = ref false
let sleep_types = ref false
let socket_order = ref false
let migrate_stats = ref false
let no_normalize = ref false
let tmp = ref None
let outdir = ref ""

(* ----------------------------------------------------- *)
(* debugging *)

let runtime s f =
  if !debug
  then
    begin
      let t = Sys.time() in
      let res = f() in
      let u = Sys.time() in
      Printf.eprintf "%s: %f\n" s (u -. t);
      res
    end
  else f()

let acctime timeacc timect f =
  if !debug
  then
    begin
      let t = Sys.time() in
      let res = f() in
      let u = Sys.time() in
      timect := !timect + 1;
      timeacc := !timeacc +. (u -. t);
      res
    end
  else f()

(* ----------------------------------------------------- *)

let get_front c s =
  try
    let res = String.sub s 0 (String.index s c) in
    if res = ""
    then s
    else res
  with _ -> s

let nas pid s matches_succeeded =
  !print_everything || List.exists (Util.matches s matches_succeeded) !target ||
  (!has_requested_pids && Hashtbl.mem requested_pids pid)

type mig_reason =
    New_Same | New_Different | Wakeup_Same | Wakeup_Different
  | LB_Same | LB_Different

let normalize_command cmd matches_succeeded = (* replace numbers by X *)
  if !no_normalize || List.exists (Util.matches cmd matches_succeeded) !target
  then cmd
  else
    let pieces = Str.split_delim (Str.regexp "[0-9]+") cmd in
    String.concat "X" pieces

let switchto pid cmd time core state freqstate startpoint first_last cmd_cores cmd_pids matches_succeeded =
  let cmd = get_front '/' cmd in
  let ncmd = normalize_command cmd matches_succeeded in
  (if (!color_by_command || !cores_with_target) &&
    startpoint <= time &&
    (List.exists (Util.matches cmd matches_succeeded) !target || !print_everything)
  then Util.hashadd_set cmd_cores ncmd core);
  (if !color_by_command && startpoint <= time &&
    (List.exists (Util.matches cmd matches_succeeded) !target || !print_everything)
  then
    begin
      Hashtbl.replace cmd_pids pid ncmd;
      Printer.inc_command_count ncmd
    end);
  (try
    let (_,ender) = Hashtbl.find first_last ncmd in
    if !ender < 0.0
    then Hashtbl.replace first_last ncmd (time,ref (-1.))
  with Not_found -> Hashtbl.replace first_last ncmd (time,ref (-1.)));
  let freqtrace =
    if !generate_frequencies_graph && !has_requested_pids && Hashtbl.mem requested_pids pid
    then
      let (tm,frq) = Array.get freqstate core in
      if time -. tm > 0.004
      then [(time,(-1.0),-1)] (* out of date, no info *)
      else [(time,(-1.0),frq)]
    else [] in
  Array.set state core
    ((pid,nas pid cmd matches_succeeded,ncmd,time,(-1.0),freqtrace) ::
     (Array.get state core))

let switchfrom pid cmd time core state trace cmd_trace startpoint
    first_last matches_succeeded =
  let isnas = nas pid cmd matches_succeeded in
  let process p t freqtrace rest =
    let cmd = get_front '/' cmd in
    if time >= startpoint
    then
      begin
       let ncmd = normalize_command cmd matches_succeeded in
       let freqtrace =
	 match freqtrace with
	   [] -> freqtrace
	 | (s,e,freq)::rest -> (s,time,freq)::rest in
       let entry = (p,isnas,ncmd,t,time,freqtrace) in
       (if !color_by_command
       then
	 begin
	   Util.hashadd cmd_trace ncmd (t,time);
	 end);
       (if not !relaxed then trace := (entry,core) :: !trace);
       (try
	 let (first,last) = Hashtbl.find first_last ncmd in
	 last := time
       with Not_found -> ());
       Array.set state core (entry::rest)
      end
    else Array.set state core rest in
  match Array.get state core with
    (p,_,_,t,_,freqtrace) :: rest when p = pid -> process p t freqtrace rest
  | _ -> () (* process pid startpoint [] *)

let tracking pid =
  (not !has_requested_pids && !requested_command = None && pid > 0) ||
  Hashtbl.mem requested_pids pid

let pid_transition frompid topid leafpid =
  if List.mem topid !color_by_pid
  then Hashtbl.add leafpid topid topid
  else if topid <> frompid
  then
    try
      let parent = Hashtbl.find leafpid frompid in
      Hashtbl.add leafpid topid parent
    with _ -> ()

type event_counts =
    { sched_switch : int ref;
      sched_wakeup_idle_without_ipi : int ref }

let make_event_counts _ =
  { sched_switch = ref 0;
    sched_wakeup_idle_without_ipi = ref 0 }

type migrate_stat =
    { waking : int ref;
      woken : int ref;
      numa : int ref }

let make_migrate_stat _ =
  { waking = ref 0;
    woken = ref 0;
    numa = ref 0 }

let inc x = x := !x + 1

(* endpoint is not needed, because no events arrive that are past the end point *)
let process_line time start core corestate freqstate trace cmd_trace cmd_pids events sched_events event_counts
    other_events first_appearance ticks functions leafpid first_last cmd_cores
    interrupt_tbl event_tbl matches_succeeded sleep_trace migrate_stat
    startpoint l =
  match l with
    Parse_line.Sched_process_exec(cmd,oldcmd,pid,oldpid) ->
      pid_transition oldpid pid leafpid;
      if tracking pid
      then
	begin
	  switchfrom pid oldcmd time core corestate trace cmd_trace startpoint
	    first_last matches_succeeded;
	  switchto pid cmd time core corestate freqstate
	    startpoint first_last cmd_cores cmd_pids matches_succeeded
	end
  | Parse_line.Sched_switch(fromcmd,frompid,reason,tocmd,topid) ->
      (if !sleep_types && tracking frompid && time >= startpoint
	  && nas frompid fromcmd matches_succeeded
      then
	match reason with
	  Parse_line.Block s ->
	    Util.hashadd sleep_trace s (time,core,frompid)
	| _ -> ());
      let fail _ =
	(if tracking frompid
	then
	  switchfrom frompid fromcmd time core corestate trace cmd_trace
	    startpoint first_last matches_succeeded);
	(if tracking topid
	then
	  switchto topid tocmd time core corestate freqstate startpoint
	    first_last cmd_cores cmd_pids matches_succeeded) in
      (match Array.get corestate core with (* special cases to reduce merging *)
	(p1,true,_,t1s,t1e,freqtrace)::rest when
	  p1 = topid && tracking topid && frompid = 0 &&
	  time -. t1e < !minsleep ->
	    Array.set corestate core ((p1,true,tocmd,t1s,t1e,freqtrace)::rest)
      | (p,false,_,t,_,f1)::(p1,true,_,t1s,t1e,f2)::rest when
	  t1e = t && topid = p1 &&
	  tracking topid && !relaxed && frompid > 0 ->
	    Array.set corestate core ((p1,true,tocmd,t1s,t1e,f1@f2)::rest)
      | _ -> fail());
      (if tracking frompid && not !waking_actor_only &&
	  reason = Parse_line.Block "D" && time >= startpoint
      then
	begin
	  inc event_counts.sched_switch;
	  sched_events := (time,core,fromcmd,frompid,l) :: !sched_events
	end);
      (if tracking topid && not !no_fork_lines
      then
	begin
	  if not (Hashtbl.mem first_appearance topid)
	  then
            Hashtbl.add first_appearance topid (core,time)
	  else
            let (_,t) = Hashtbl.find first_appearance topid in
            if time < t then
              Hashtbl.replace first_appearance topid (core,time)
	end)
  | Parse_line.Sched_waking(actor_cmd,actor_pid,cmd,pid,cpu,interrupt) ->
      (if !color_by_command && startpoint <= time &&
	(List.exists (Util.matches cmd matches_succeeded) !target || !print_everything)
      then
	let tbl = if interrupt then interrupt_tbl else event_tbl in
	let ncmd = normalize_command cmd matches_succeeded in
	Util.hashinc tbl ncmd 1);
      (if tracking actor_pid && not !io_events
      then sched_events := (time,core,actor_cmd,actor_pid,l) :: !sched_events);
      if tracking pid && not !waking_actor_only && not !io_events
      then sched_events := (time,core,cmd,pid,l) :: !sched_events
  | Parse_line.Sched_wakeup(cmd,pid,prevcpu,cpu) ->
      if tracking pid && not !waking_actor_only && not !io_events
      then sched_events := (time,core,cmd,pid,l) :: !sched_events
  | Parse_line.Sched_process_fork(_,parent_pid,cmd,pid) ->
      pid_transition parent_pid pid leafpid;
      (if !has_requested_pids && Hashtbl.mem requested_pids parent_pid
      then Hashtbl.add requested_pids pid ());
      (match !requested_command with
	Some(rcmd,nth) when rcmd = cmd ->
	  if nth = 0
	  then
	    begin
	      (* found the requested one *)
	      requested_command := Some(rcmd,nth-1);
	      has_requested_pids := true;
	      Hashtbl.add requested_pids pid ()
	    end
	  else
	    (* skip this one *)
	    requested_command := Some(rcmd,nth-1)
      | _ -> ());
      if tracking pid && not !waking_actor_only && not !io_events
      then sched_events := (time,core,cmd,pid,l) :: !sched_events
  | Parse_line.Sched_wakeup_new(cmd,pid,parent,cpu) ->
      if tracking pid
      then
	begin
	  (if !migrations && parent <> cpu && nas pid cmd matches_succeeded
	  then
            (if parent mod !migsockets = core mod !migsockets
            then Util.hashadd events pid (time,New_Same)
            else Util.hashadd events pid (time,New_Different)));
	  if not !waking_actor_only && not !io_events
	  then sched_events := (time,core,cmd,pid,l) :: !sched_events
	end
  | Parse_line.Sched_migrate_task(cmd,pid,oldcpu,newcpu,state) ->
      (if !migrate_stats && tracking pid && time >= startpoint
	  && nas pid cmd matches_succeeded
      then
	match state with
	  Parse_line.Waking _ -> inc migrate_stat.waking
	| Parse_line.Woken -> inc migrate_stat.woken
	| Parse_line.Numa _ -> inc migrate_stat.numa);
      if tracking pid
      then
	begin
	  (if !migrations && nas pid cmd matches_succeeded &&
	      not !waking_actor_only
	  then
	    match state with
              Parse_line.Waking _ ->
		if oldcpu mod !migsockets = newcpu mod !migsockets
		then Util.hashadd events pid (time,Wakeup_Same)
		else Util.hashadd events pid (time,Wakeup_Different)
            | Parse_line.Woken | Parse_line.Numa _ ->
		if oldcpu mod !migsockets = newcpu mod !migsockets
		then Util.hashadd events pid (time,LB_Same)
		else Util.hashadd events pid (time,LB_Different));
	  if not !waking_actor_only && not !io_events
	  then sched_events := (time,core,cmd,pid,l) :: !sched_events
	end
  | Parse_line.Sched_wake_idle_without_ipi cpu ->
      if not !io_events && time >= startpoint
      then
	begin
	  inc event_counts.sched_wakeup_idle_without_ipi;
	  (* most data is not meaningful *)
	  sched_events := (time,core,"",cpu,l) :: !sched_events
	end
  | Parse_line.Sched_tick(freq) ->
      if time >= startpoint
      then
	begin
	  Array.set freqstate core (time,freq);
	  (match Array.get corestate core with
	    (p,isnas,ncmd,t,endtime,freqtrace)::rest when endtime < 0. (* not yet terminated *) ->
	      let freqtrace =
		match freqtrace with
		  [(s,e,-1)] (*dummy value when no frequency*) -> [(time,(-1.),freq)]
		| (s,e,oldfreq)::rest when e < 0.0 -> (time,(-1.),freq)::(s,time,oldfreq)::rest
		| _ -> freqtrace in
	      Array.set corestate core ((p,isnas,ncmd,t,endtime,freqtrace)::rest)
	  | _ -> ());
	  Util.hashadd ticks core (time,freq)
	end
  | Parse_line.Traced_function(function_name) ->
      Util.hashadd functions core (time,function_name)
  | Parse_line.Other(args,pieces) ->
      if !get_other_events && time >= startpoint
      then other_events := (time,core,args) :: !other_events
  | _ -> ()

let compress_and_sort state duration =
  let interval = duration /. 5000. in
  Array.iteri
    (fun i l ->
      let l = List.rev l in
      let rec loop acc = function
	  [] -> List.rev acc
	| (p1,true,cmd,t1s,t1e,f1)::rest ->
	    let rec iloop prevend = function
		(_,false,_,t2s,t2e,f2)::rest when prevend = t2s ->
		  iloop t2e rest
	      | (p3,true,_,t3s,t3e,f2)::rest when p1 = p3 && prevend +. interval >= t3s ->
		  loop acc ((p1,true,cmd,t1s,t3e,f2@f1)::rest)
	      | rest ->
		  loop ((p1,true,cmd,t1s,t1e,f1)::acc) rest in
	    iloop t1e rest
	| x::xs -> loop (x::acc) xs in
      let res =
	if !relaxed
	then loop [] l
	else l in
      Array.set state i res)
    state

let parse_all ((file,after_sleep,interrupt) as ctx) startpoint endpoint =
  let (c,start,i) = Util.skip_front_continue ctx in (* # cpus *)
  (if !cores_info = INFERRED
  then
    begin
      cores := c;
      (match !cores with
	160 | 128 -> numsockets := 4
      | 64 -> numsockets := 2
      | 20 -> numsockets := 1
      | 96 -> numsockets := 16 (* neowise *)
      | _ -> ())
    end);
  let corestate = Array.make !cores [] in
  let freqstate = Array.make !cores ((-9.0),0) in
  let trace = ref [] in
  let cmd_trace = Hashtbl.create 31 in
  let cmd_pids = Hashtbl.create 31 in
  let events = Hashtbl.create 10001 in
  let other_events_tbl = ref [] in (* Parse_line.Other *)
  let sched_events = ref [] in
  let event_counts = make_event_counts() in
  let first_appearance = Hashtbl.create 101 in(*child pid -> (first core,time)*)
  let ticks = Hashtbl.create !cores in (* cpu -> (time,freq) list ref *)
  let functions = Hashtbl.create !cores in (*cpu->(time,function_name)list ref*)
  let leafpid = Hashtbl.create 101 in
  let first_last = Hashtbl.create 101 in
  let cmd_cores = Hashtbl.create 101 in
  let interrupt_tbl = Hashtbl.create 101 in
  let event_tbl = Hashtbl.create 101 in
  let matches_succeeded = ref false in
  let sleep_trace = Hashtbl.create 3 in
  let migrate_stat = make_migrate_stat() in
  let elements =
    if !no_events && !no_fork_lines && !no_migrate_arrows &&
      not (!generate_frequencies_graph || !generate_merged_graph) &&
      not !has_requested_pids && !requested_command = None
    then ["sched_switch";"sched_process_exec"]
    else
      if !generate_frequencies_graph
      then
	let events = ["bprint"] in
	if !show <> [] || !has_requested_pids
	then events @ ["sched_switch";"sched_process_exec";"sched_process_fork"]
	else events
      else if !no_events && !no_fork_lines && !no_migrate_arrows
	  && not (!generate_frequencies_graph || !generate_merged_graph)
	  && (!has_requested_pids || !requested_command <> None)
      then ["sched_switch";"sched_process_exec";"sched_process_exit";
	     "sched_process_fork"]
      else
	["sched_switch";"sched_process_exec";"sched_process_exit";
	  "sched_waking";"sched_wakeup";
          "sched_process_fork";"sched_wakeup_new";
	  "sched_migrate_task";"sched_tick";"function";
	  "sched_wake_idle_without_ipi"] in
  let elements =
    if !color_by_command && not (List.mem "sched_waking" elements)
    then "sched_waking" :: elements
    else elements in
  let elements =
    if !migrate_stats && not (List.mem "sched_migrate_task" elements)
    then "sched_migrate_task" :: elements
    else elements in
  let elements =
    if !get_other_events || !generate_merged_graph
    then ["bprint";"bputs"] @ elements
    else elements in
  (if !debug then Printf.eprintf "considered elements (%d): %s\n" (List.length elements) (String.concat " " elements));
  let rec loop _ =
    let l = input_line i in
    let (core,time,l) = Parse_line.parse_line l true elements in
    let startpoint = startpoint +. start in
    let endpoint = endpoint +. start in
    if time <= endpoint
    then
      begin
	process_line time start core corestate freqstate trace cmd_trace cmd_pids events
	  sched_events event_counts other_events_tbl
	  first_appearance ticks functions leafpid first_last cmd_cores
	  interrupt_tbl event_tbl
	  matches_succeeded sleep_trace migrate_stat startpoint l;
	loop()
      end in
  Hashtbl.iter
    (fun k (s,e) ->
      if !e < 0.0 then e := endpoint +. start)
    first_last;
  let res =
    try (loop();true)
    with
      End_of_file -> true
    | Failure s ->
	Printf.printf "Failure in %s: %s\n" file s; flush stdout;
	false in
  close_in i;
  Array.iteri
    (fun i l ->
      match l with
	(p,isnas,cmd,t,tm,freqtrace) :: rest when tm < 0.0 ->
	  let last_time = endpoint +. start in
	  let freqtrace =
	    match freqtrace with
	      [] -> freqtrace
	    | (t,_,freq)::rest -> (t,last_time,freq)::rest in
	  let entry = (p,isnas,cmd,t,last_time,freqtrace) in
	  Array.set corestate i (entry :: rest);
	  (if !color_by_command
	  then Util.hashadd cmd_trace cmd (t,last_time));
	  trace := (entry,i) :: !trace
      | _ -> ())
    corestate;
  runtime "compress" (fun _-> compress_and_sort corestate endpoint);
  (res,start,corestate,List.rev !trace,cmd_trace,cmd_pids,events,
   List.rev !sched_events,event_counts,List.rev !other_events_tbl,
   first_appearance,ticks,functions,leafpid,first_last,cmd_cores,
   interrupt_tbl,event_tbl,
   matches_succeeded,sleep_trace,migrate_stat)

let clean events =
  Hashtbl.iter (fun k v -> v := List.sort compare !v) events

(* ----------------------------------------------------- *)

let start_graph ysize xlabel ylabel base file ht chosencores start startpoint
    endpoint duration =
  let xlabel =
    Printf.sprintf "%s, duration: %f, time (seconds)%s%s%s" base duration xlabel
      (if !relaxed then ", relaxed" else "")
      (if !top_commands then ", top 25 commands" else "") in
  let (ymin,ymax) =
    if Str.string_match (Str.regexp "core") ylabel 0
    then
      if !hyper
      then (Some 0,Printer.Ymax (float_of_int (!cores/2)))
      else
	match chosencores with
	  Some chosencores ->
	    (Some 0,Printer.Ylist (List.sort compare chosencores))
	| None -> (Some 0,Printer.Ymax (float_of_int !cores))
    else (None,Printer.Nomax) in
  let (startpoint,endpoint) =
    if !requested_something
    then (None,None)
    else (Some startpoint,Some endpoint) in
  Printer.startgraph file !ty start 8 startpoint endpoint xlabel
    ysize ymin ymax ylabel

let evt_to_color pid evt =
  match evt with
    Parse_line.Sched_wakeup(_,_,_,_) ->         Printer.Choose 0 (* blue *)
  | Parse_line.Sched_wakeup_new(_,_,_,_) ->     Printer.Choose 4 (* yellow *)
  | Parse_line.Sched_waking(actor_cmd,actor_pid,cmd,_pid,cpu,_) when pid = actor_pid ->
      Printer.Choose 2 (* red *)
  | Parse_line.Sched_waking(actor_cmd,actor_pid,cmd,pid,cpu,_) ->
      Printer.Choose 8 (* ??? *)
  | Parse_line.Sched_process_fork(_,_,_,_) ->   Printer.Choose 3 (* purple *)
  | Parse_line.Sched_wake_idle_without_ipi _ -> Printer.Choose 1 (* green *)
  | Parse_line.Sched_migrate_task(_,_,_,_,_) -> Printer.Choose 5 (* cyan *)
  | Parse_line.Sched_switch(_,_,_,_,_) ->       Printer.Choose 6 (* pale blue *)
  | Parse_line.Not_supported _ ->               Printer.Choose 7 (* deep purple *)
  | _ ->                                        Printer.Choose 9

let evt_to_string pid evt event_counts =
  match evt with
    Parse_line.Sched_wakeup(_,_,_,_) ->           "wakeup"
  | Parse_line.Sched_wakeup_new(_,_,_,_) ->     "wakeup_new"
  | Parse_line.Sched_waking(actor_cmd,actor_pid,cmd,_pid,cpu,_)
    when pid = actor_pid ->
      "waking (waker)"
  | Parse_line.Sched_waking(actor_cmd,actor_pid,cmd,pid,cpu,_) ->
      "waking (wakee)"
  | Parse_line.Sched_process_fork(_,_,_,_) ->   "fork"
  | Parse_line.Sched_wake_idle_without_ipi _ -> (Printf.sprintf "wake_idle_without_ipi (%d)" !(event_counts.sched_wakeup_idle_without_ipi))
  | Parse_line.Sched_migrate_task(_,_,_,_,_) -> "migrate_task"
  | Parse_line.Sched_switch(_,_,_,_,_) ->       (Printf.sprintf "i/o sleep (%d)" !(event_counts.sched_switch))
  | Parse_line.Not_supported _ ->               "unknown"
  | _ ->                                        "unknown"

let migrate_color oldcpu newcpu = function
    Parse_line.Waking _ ->
      if oldcpu mod !numsockets = newcpu mod !numsockets
      then Printer.RGB(1.0,0.6,0.8)
      else Printer.RGB(0.0,0.75,1.0)
  | Parse_line.Woken    ->
      if oldcpu mod !numsockets = newcpu mod !numsockets
      then Printer.RGB(1.0,0.8,0.0)
      else Printer.RGB(0.0,0.46,0.0)
  | Parse_line.Numa _   -> Printer.RGB(0.2,0.6,0.4)

let __next_color = ref 0 (* for functions *)
let next_color _ =
  let r = !__next_color in
  __next_color := r + 1;
  r

(* ----------------------------------------------------- *)

let newcurve go start isnas cmd leafpid pid time1 time2 core realpid =
  let (ok,color,cmd) =
    if !color_by_pid <> []
    then
      try
	let parent = Hashtbl.find leafpid realpid in
	(true,Printer.Choose parent,string_of_int parent)
      with _ -> (false,Printer.Color "black",cmd)
    else
      if !color_by_command && (!print_everything || isnas)
      then (true,Printer.SChoose cmd,cmd)
      else if not(!color_by_command) && (isnas || !print_everything)
      then (not !others,Printer.Choose pid,cmd)
      else (true,Printer.Color "black",cmd) in
  if ok
  then
    let cmd =
      Printf.sprintf "%s %d" cmd pid in
    if !hyper && core > (!cores/2)
    then
      let core = float_of_int(core - (!cores/2)) +. 0.3 in
      Printer.fedge go [(time1,core);(time2,core)] color Printer.NoLabel
	(Some cmd)
    else
      begin
	Printer.edge go [(time1,core);(time2,core)] color Printer.NoLabel
	  (Some cmd);
	if !print_sizes
	then
	  let xcor = time1 +. ((time2 -. time1) /. 2.) in
	  let ycor = (float_of_int core) +. 0.2 in
	  let str =
	    Printf.sprintf "%d" (int_of_float (1000000. *. (time2 -. time1))) in
	  let str =
	    if String.get str 0 = '0'
	    then String.sub str 1 (String.length str - 1)
	    else str in
	  Printer.print_text go xcor ycor str
      end

let get_cores_with_targets cmd_cores =
  if !cores_with_target
  then
    let l =
      Hashtbl.fold
	(fun k v rest ->
	  if List.mem k !target
	  then
	    List.fold_left
	      (fun prev core ->
		if List.mem core prev then prev else core :: prev)
	      rest !v
	  else rest)
	cmd_cores [] in
    Some l
  else None

let reorder c =
  if !socket_order
  then
    let sockets =
      match !cores with
	160 -> 4
      | 128 -> 4
      | 64 -> 2
      | 20 -> 1
      | 96 -> 16 (* neowise *)
      | _ -> failwith "reorder: unknown number of cores" in
    let cores_per_socket = !cores / sockets in
    let socket_number = c mod sockets in
    let start = socket_number * cores_per_socket in
    let offset = c / sockets in
    start + offset
  else c

let do_trace_data base start startpoint endpoint go gco gso leafpid cmd_cores cwt first_last trace
    corestate cmd_trace cmd_pids interrupt_tbl event_tbl =
  optional go
    (function go ->
      let icmd_pids =
	let tbl = Hashtbl.create 101 in
	Hashtbl.iter
	  (fun pid cmd -> Util.hashinc tbl cmd 1)
	  cmd_pids;
	tbl in
      if !color_by_command
      then
	let infos =
	  Hashtbl.fold
	    (fun cmd infos r ->
	      let relevant_infos =
		List.rev
		  (List.fold_left
		     (fun prev (ts,te) ->
		       if startpoint <= te && endpoint >= ts
		       then (max ts startpoint, min te endpoint) :: prev
		       else prev)
		     [] !infos) in
	      if relevant_infos = []
	      then r
	      else (List.length relevant_infos,cmd,relevant_infos) :: r)
	    cmd_trace [] in
	let infos = List.rev(List.sort compare infos) in
	let labels =
	  List.map
	    (fun (num_events,cmd,events) ->
	      (cmd,
	       try
		 let (f,l) = Hashtbl.find first_last cmd in
		 let cores = Hashtbl.find cmd_cores cmd in
		 let duration =
		   List.fold_left
		     (fun prev (ts,te) -> (te -. ts) +. prev)
		     0.0 events in
		 (true,
		  Printer.Label
		    (Printf.sprintf "%s: %0.4f-%0.4f: %s (%d%s%s)%s" cmd
		       (f -. start) (!l -. start)
		       (if duration > 1.0
		       then Printf.sprintf "%0.2f" duration
		       else Printf.sprintf "%0.4f" duration)
		       num_events
		       (try
			 let ct = !(Hashtbl.find icmd_pids cmd) in
			 if ct = 1
			 then ", 1 pid"
			 else Printf.sprintf ", %d pids" ct
		       with Not_found -> "")
		       (let interrupts =
			 try !(Hashtbl.find interrupt_tbl cmd) with _ -> 0 in
		       let events =
			 try !(Hashtbl.find event_tbl cmd) with _ -> 0 in
		       let tot = interrupts + events in
		       if tot = 0
		       then ""
		       else
			 Printf.sprintf ", %d%% int wakes"
			   ((interrupts * 100) / tot))
		       (if List.length !cores <= 3
		       then (String.concat ", " (List.map string_of_int !cores))
		       else "")))
	       with Not_found -> (false,Printer.Label cmd)))
	    infos in
	let labels =
	  if !top_commands
	  then Util.take 25 labels
	  else labels in
	List.iter
	  (fun (cmd,(colored,lbl)) ->
	    let color =
	      if colored
	      then Printer.SChoose cmd
	      else Printer.Color "black" in
	    Printer.fedge go [] color lbl None)
	  labels);
  let print_curves nas cmd pid time1 time2 core1 =
    optional go
	(function go ->
	  match cwt with
	    None ->
	      newcurve go start nas cmd leafpid pid time1 time2 (reorder core1) pid
	  | Some l ->
	      try
		let core1 = List.assoc core1 l in
		newcurve go start nas cmd leafpid pid time1 time2 (reorder core1) pid
	      with Not_found -> ());
    if nas || !color_by_pid <> []
    then
      begin
	(optional gco
	   (function gco ->
	     newcurve gco start nas cmd leafpid core1 time1 time2 pid pid));
	List.iter2
	  (fun so socket ->
	    newcurve so start nas cmd leafpid (core1 mod socket) time1 time2
	      pid pid)
	  gso !sockets
      end in
  if !relaxed
  then (* core order *)
    Array.iteri
      (fun core l ->
	List.iter
	  (fun (pid,nas,cmd,ts,te,freqtrace) -> print_curves nas cmd pid ts te core)
	  l)
      corestate
  else (* time order *)
    List.iter
      (fun ((pid,nas,cmd,ts,te,freqtrace),core) -> print_curves nas cmd pid ts te core)
      trace

(* ----------------------------------------------------- *)

let get_color_data_by_ticks ticks freqtbl startpoint endpoint start =
  let startpoint = startpoint +. start in
  let endpoint = endpoint +. start in
  let ct = ref 0 in
  let bycolor = Hashtbl.create 101 in
  let res =
    Hashtbl.fold
      (fun core events r ->
        (* events are in reverse order *)
	let events =
	  List.fold_left
	    (fun prev (time,freq) ->
	      if startpoint <= time && time <= endpoint
	      then
		begin
		  let itime = int_of_float(time *. 1000.) in
		  let c = Machines.freq_to_color freq freqtbl in
		  Util.hashincf bycolor c 0.004;
		  ct := !ct + 1;
		  (time,time +. 0.004,itime,itime+4,c,freq) :: prev
		end
	      else prev)
	    [] !events in
        (* now events are in increasing time order *)
        (* color events for the freq graph *)
	let rec loop acc = function
	    [] -> acc
	  | (t1,e1,it1,ie1,c1,freq1)::(t2,e2,it2,ie2,c2,freq2)::rest when ie1 = it2 && c1 = c2 ->
	      loop acc ((t1,e2,it1,ie2,c1,freq1)::rest)
	  | x::xs -> loop (x::acc) xs in
	let color_events = loop [] events in (* now reversed again *)
	let color_events = (* collapsed by color *)
	  List.rev_map (fun (t,e,it,ie,c,freq) -> (t,e,c)) color_events in
        (* frequency events for the freqgraph graph *)
	let rec loop prev prevacc acc = function
	    [] ->
	      if prevacc = []
	      then List.rev acc
	      else List.rev(((List.rev prevacc)::acc))
	  | (t1,e1,it1,ie1,c1,freq)::rest ->
	      if it1 = prev
	      then loop ie1 ((t1,e1,freq)::prevacc) acc rest
	      else if prevacc = []
	      then loop ie1 [(t1,e1,freq)] acc rest
	      else loop ie1 [(t1,e1,freq)] ((List.rev prevacc)::acc) rest in
      (* not collapsed by frequency; identical frequencies should be rare *)
	let freqgraph_events = loop (-1) [] [] events in
	(core,color_events,freqgraph_events)::r)
      ticks [] in
  (res,float_of_int !ct *. 0.004,bycolor)

let get_color_data_by_corestate corestate freqtbl startpoint endpoint start =
  let startpoint = startpoint +. start in
  let endpoint = endpoint +. start in
  let ct = ref 0.0 in
  let bycolor = Hashtbl.create 101 in
  let res = ref [] in
  Array.iteri
    (fun core events ->
      (* events are in reverse order *)
      (* accumulate time for which we have frequency information *)
      List.iter
	(fun (p,_,_,ts,te,freqtrace) ->
	  if ts <= endpoint && te >= startpoint
	  then
	    List.iter
	      (fun (ts,te,f) ->
		if ts <= endpoint && te >= startpoint && f > 0
		then
		  let ts = max startpoint ts in (* maybe not necessary, but safer *)
		  let te = min endpoint te in
		  ct := !ct +. (te -. ts))
	      freqtrace)
	events;
      (* color information *)
      let color_events =
	List.fold_left
	  (fun prev (p,_,_,t,te,freqtrace) -> (* freqtrace is reversed *)
	    if t <= endpoint && te >= startpoint
	    then
	      let freqtrace =
		match freqtrace with
		  [(_,_,x)] when x <= 0 -> [] (* no info *)
		| _ -> freqtrace in
	      let freqtrace =
		List.rev
		  (List.fold_left
		     (fun prev (ts,te,freq) ->
		       if ts <= endpoint && te >= startpoint
		       then
			 begin
			   let c = Machines.freq_to_color freq freqtbl in
			   let ts = max startpoint ts in (* maybe not necessary, but safer *)
			   let te = min endpoint te in
			   Util.hashincf bycolor c (te -. ts);
			   (ts,te,c)::prev
			 end
		       else prev)
		     [] freqtrace) in
	      let rec loop prev = function
		  [] -> prev
		| (t3,t4,c2)::(t1,t2,c1)::rest when c1 = c2 ->
		    loop prev ((t1,t4,c1)::rest)
		| x::rest -> loop (x::prev) rest in
	      loop prev freqtrace
	    else prev)
	  [] events in
      let freqgraph_events = [] in (* not sure we want this case *)
      res := (core,color_events,freqgraph_events) :: !res)
    corestate;
  (List.rev !res,!ct,bycolor)

let do_color_by_frequency base go xlabel start startpoint endpoint last_time
    ticks corestate =
  let freqtbl = Machines.get_freqtbl base in
  let gfo =
    if !generate_merged_graph
    then
      (match go with Some go -> go | None -> failwith "bad output")
    else
      let foutfile = base ^ "_freq"  in
      let ylabel =
	if !socket_order
	then "core (socket order)"
	else "core" in
      start_graph 3 (", freq"^xlabel) ylabel base foutfile 4 None start
	startpoint endpoint (last_time -. start) in
  (* frequencies of all cores *)
  let gfco =
    if not !has_requested_pids
    then
      let xlabel =
	Printf.sprintf "%s, duration: %f, time (seconds)%s%s" base
	  (last_time -. start) (", freq"^xlabel)
	  (if !relaxed then " relaxed" else "") in
      let (startpoint,endpoint) =
	if !requested_something
	then (None,None)
	else (Some startpoint,Some endpoint) in
      let mf = (float_of_int (fst freqtbl)) /. 1000000. in
      let fcoutfile = base ^ "_freqgraph"  in
      Some
	(Printer.startgraph fcoutfile !ty start 8 startpoint endpoint xlabel 3
	   (Some 2) (Printer.Ymax mf) "frequency")
    else None in
  (* collect data *)
  let (color_data,color_time,bycolor) =
    if not !has_requested_pids
    then get_color_data_by_ticks ticks freqtbl startpoint endpoint start
    else get_color_data_by_corestate corestate freqtbl startpoint endpoint start in
  (* print legend *)
  (if color_data = []
  then
    Printf.eprintf
      "%s: No color information, check for \"arch_scale_freq_tick\" in the trace output\n"
      base
  else
    begin
      let onm =
	let base =
	  match Str.split (Str.regexp "_sockorder") base with
	    [a;b] -> a^b
	  | [a] -> a
	  | _-> failwith "sockorder should not appear more than once in a name" in
	if !outdir = ""
	then Printf.sprintf "%s.freq" base
	else Printf.sprintf "%s/%s.freq" !outdir (Filename.basename base) in
      let ofile = open_out onm in
      Printf.fprintf ofile "low,high,count,pct\n";
      List.iter
	(function (low,up,_,_,c) ->
	  let count_c = try !(Hashtbl.find bycolor c) with Not_found -> 0. in
	  let pct_c = (count_c *. 100.) /. color_time in
	  Printf.fprintf ofile "%d,%d,%f,%f\n" low up count_c pct_c;
	  let print gfo print data =
	    print gfo data c
	      (Printer.Label
		 (Printf.sprintf "(%d.%d %d.%d] GHz (%0.2f = %0.2f%%)"
		    (low/1000000) ((low/100000) mod 10) (up/1000000)
		    ((up/100000) mod 10) count_c pct_c))
	      None in
	  print gfo Printer.edge [];
	  let lf = (float_of_int low) /. 1000000. in
	  optional gfco
	    (function gfco ->
	      print gfco Printer.fdots
		(if low > 0
		then [(startpoint+.start,lf);(endpoint+.start,lf)]
		else [])))
	(snd freqtbl);
      close_out ofile
    end);
  (* print data *)
  (if !ty <> Printer.Nograph
  then
    List.iter
      (fun (core,color_events,freqgraph_events) ->
        (* color graph *)
        let core2 =
	  let core = reorder core in
	  let core = float_of_int core in
	  if !generate_merged_graph
	  then core +. 0.3
	  else core in
        List.iter
	  (function (time,e,freqc) ->
	    Printer.fedge gfo [(time,core2);(e,core2)] freqc
	      Printer.NoLabel None)
	  color_events;
        (* line for each core with absolute values *)
        optional gfco
	  (fun gfco ->
	    List.iter
	      (function l ->
	        let (_,ep,freq) = List.hd(List.rev l) in (* endpoint *)
	        let l =
		  List.rev
		    (List.fold_left
		       (fun prev (time,e,freq) ->
		         (time,(float_of_int freq) /. 1000000.) :: prev)
		       [(ep,(float_of_int freq) /. 1000000.)] l) in
	        Printer.fedge gfco l (Printer.Choose core)
		  Printer.NoLabel None)
	      freqgraph_events))
      color_data);
  (if !generate_frequencies_graph
  then Printer.endgraph gfo !save_tmp !tmp);
  optional gfco (fun gfco -> Printer.endgraph gfco !save_tmp !tmp)

(* ----------------------------------------------------- *)

let table_iterate tbl fn =
  Hashtbl.iter
    (fun index events ->
      List.iter (fun data -> fn index data) (List.rev !events))
    tbl

let migration_and_events gco events =
  (* Migration events for core graph *)
  (* Put this first so the execution line goes on top of it. *)
  (* This may be redundant... *)
  Hashtbl.iter
    (fun pid events ->
      let events = List.rev !events in
      let (new_same,new_different,wakeup_same,wakeup_different,
	   lb_same,lb_different) =
	List.fold_left
	  (fun (new_same,new_different,wakeup_same,wakeup_different,
		lb_same,lb_different) ->
	    function
		(time,New_Same) ->
		  (time::new_same,new_different,wakeup_same,wakeup_different,
		   lb_same,lb_different)
	      | (time,New_Different) ->
		  (new_same,time::new_different,wakeup_same,wakeup_different,
		   lb_same,lb_different)
	      | (time,Wakeup_Same) ->
		  (new_same,new_different,time::wakeup_same,wakeup_different,
		   lb_same,lb_different)
	      | (time,Wakeup_Different) ->
		  (new_same,new_different,wakeup_same,time::wakeup_different,
		   lb_same,lb_different)
	      | (time,LB_Same) ->
		  (new_same,new_different,wakeup_same,wakeup_different,
		   time::lb_same,lb_different)
	      | (time,LB_Different) ->
		  (new_same,new_different,wakeup_same,wakeup_different,
		   lb_same,time::lb_different))
	  ([],[],[],[],[],[])
	  events in
      let level =
	match !migsockets with
	  160 -> "core"
	| 80 -> "smt"
	| 4 -> "socket"
	| _ -> "unknown" in
      let newcurve color label mark data =
	Printer.mark mark gco (List.map (fun time -> (time,pid)) data) color
	  (Printer.Label label) None in
      if not !others
      then
	begin
	  newcurve (Printer.RGB(0.75,0.75,0.75)) ("new same "^level)
	    "box cfill 1 1 1" new_same;
	  newcurve (Printer.RGB(0.75,0.75,0.75)) ("new different "^level)
	    "box" new_different;
	  newcurve (Printer.RGB(1.0,0.6,1.0)) ("wake same "^level)
	    "box cfill 1 1 1" wakeup_same;
	  newcurve (Printer.RGB(1.0,0.6,1.0)) ("wake different "^level)
	    "box" wakeup_different;
	  newcurve (Printer.RGB(0.6,0.6,1.0)) ("LB same "^level)
	    "box cfill 1 1 1" lb_same;
	  newcurve (Printer.RGB(0.6,0.6,1.0)) ("LB different "^level)
	    "box" lb_different
	end)
    events

let do_sleep_types startpoint endpoint trace coreo pido =
  let do_one o infos get =
    optional o
      (function o ->
	List.iter
	  (fun (ct,reason,entries) ->
	    let str = Printf.sprintf "%s (%d)" reason ct in
      	    Printer.markonly "x" o (List.rev_map get entries)
	      (Printer.SChoose reason) (Printer.Label str) (Some str))
	  infos) in
  let infos =
    Hashtbl.fold
      (fun reason entries r ->
	let entries =
	  List.filter
	    (function (time,_,_) ->
	      startpoint <= time && time <= endpoint)
	    !entries in
	(List.length entries,reason,entries)::r)
      trace [] in
  let infos = List.rev(List.sort compare infos) in
  List.iter (fun o -> do_one o infos (fun (time,core,frompid) -> (time,reorder core))) coreo;
  List.iter (fun o -> do_one o infos (fun (time,core,frompid) -> (time,frompid))) pido

let do_migrate_stats base migrate_stat =
  let ofile = open_out (base ^ ".mig") in
  Printf.fprintf ofile "waking %d\n" !(migrate_stat.waking);
  Printf.fprintf ofile "load balance %d\n" !(migrate_stat.woken);
  Printf.fprintf ofile "numa balance %d\n" !(migrate_stat.numa);
  close_out ofile

let main (env,(endstr,startpoint,endpoint)) (base,file,drop) =
  (if !color_by_command then Hashtbl.clear Printer.command_count);
(*  List.iteri (fun i tgt -> Printer.set_command_count tgt i) (List.rev !target);*)
  let base = Printf.sprintf "%s%s" base endstr in
  let ranges =
    if !show = []
    then [(startpoint,endpoint,base,[],0)]
    else Util.interpret_show_commands !show file startpoint endpoint in
  (if !generate_frequencies_graph || !generate_merged_graph
  then
    List.iter
      (function pid ->
        has_requested_pids := true;
	Hashtbl.add requested_pids pid ())
      (List.concat (List.map (fun (_,_,_,pids,_) -> pids) ranges)));
  let (success,start,corestate,trace,cmd_trace,cmd_pids,events,
       sched_events,event_counts,other_events,first_appearance,ticks,functions,
       leafpid,first_last,cmd_cores,interrupt_tbl,event_tbl,matches_succeeded,sleep_trace,
       migrate_stat) =
    runtime ("parse_all "^base)
      (fun _ ->
	try parse_all file startpoint endpoint
	with e -> begin (if drop then Util.remove file); raise e end) in
  (if drop then Util.remove file);
  (if !color_by_command then Printer.assign_colors());
  let last_time =
    match env with
      None -> endpoint +. start
    | Some env -> List.assoc file env in
  clean events;
  let xlabel = if success then "" else ", FAILED" in
  (* time x pid *)
  let unspace s = String.concat "" (Str.split (Str.regexp " ") s) in
  List.iter
    (function (startpoint,endpoint,cmd,_pid,index) ->
      let base =
	if cmd = base
	then base
	else Printf.sprintf "%s_%s_%d" base (unspace cmd) index in
      let cwt = (* cores with target *)
	match get_cores_with_targets cmd_cores with
	  None -> None
	| Some l ->
	    let l = List.sort compare l in
	    Some(List.mapi (fun i x -> (x,i)) l) in
      let go =
	if !generate_frequencies_graph
	then None (* no events traced *)
	else
	  let ysize =
	    if !generate_merged_graph then 9
	    else if !hyper then 6
	    else 3 in
	  let cwt =
	    match cwt with
	      Some l -> Some(List.map fst l)
	    | None -> None in
	  let ylabel =
	    if !socket_order
	    then "core (socket order)"
	    else "core" in
	  Some
	    (start_graph ysize xlabel ylabel base base 4 cwt start
	       startpoint endpoint (last_time -. start)) in
      (* time x core *)
      let gco =
	if !others || (Hashtbl.mem requested_pids 0 && Hashtbl.length requested_pids = 1) ||
	(!color_by_pid=[] && (!print_everything || not !matches_succeeded))
	then None (* dummy value *)
	else
	  let coutfile = base ^ "_core"  in
	  Some
	    (start_graph 3 (", core"^xlabel) "pid" base coutfile
	       (if !migrations then 10 else 4)
	       None start startpoint endpoint
	       (if !show = [] then last_time -. start else endpoint -. startpoint)) in
      (* time x pid, cores colored by socket *)
      let gso =
	let soutfiles =
	  List.map
	    (function s -> Printf.sprintf "%s_sock%d" base s)
	    !sockets in
	List.map2
	  (fun soutfile sockets ->
	    let xlabel = Printf.sprintf ", core mod %d%s" sockets xlabel in
	    start_graph 3 xlabel "pid" base soutfile 4 None
	      start startpoint endpoint (last_time -. start))
	  soutfiles !sockets in
      runtime ("build graph "^base) (fun _ ->
	(if other_events <> []
	then
	  optional go
	    (function go ->
	      let safe_map f l = List.rev(List.rev_map f l) in
	      Printer.markonly "x" go
		(safe_map (function (time,core,args) -> (time,reorder core))
		   other_events)
		(Printer.RGB(1.0,0.0,0.0))
		(Printer.Label "event") (Some "event")));
	(if !migrations
	then
	  optional gco
	    (function gco -> migration_and_events gco events));
        (* traces *)
	do_trace_data base start (startpoint +. start) (endpoint +. start)
	  go gco gso leafpid cmd_cores cwt first_last trace
	  corestate cmd_trace cmd_pids interrupt_tbl event_tbl;
	optional go
	  (function go ->
	    if !highlight_non_targets
	    then
	      List.iter
		(fun ((pid,nas,cmd,ts,te,freqtrace),core) ->
		  if not(List.exists (Util.matches cmd matches_succeeded) !target)
		  then
		    match cwt with
		      None ->
			let color = Printer.SChoose cmd in
			Printer.mark "x" go [(ts,reorder core)] color (Printer.Label cmd)
			  (Some cmd)
		    | Some l ->
			try
			  let core = List.assoc core l in
			  let color = Printer.SChoose cmd in
			  Printer.mark "x" go [(ts,reorder core)] color (Printer.Label cmd)
			    (Some cmd)
			with Not_found -> ())
		trace);
        (* displays events *)
	(if not !generate_frequencies_graph &&
	  (not !no_events || not !no_fork_lines || not !no_migrate_arrows)
	then
	  List.iter
	    (fun (time,core,cmd,pid,l) ->
	      (if not !no_events
	      then
		begin
		  let core =
		    match l with
		      Parse_line.Sched_wakeup_new(cmd,pid,parent,cpu) -> cpu
		    | _ -> core in
		  (optional go
		     (function go ->
		       Printer.notch go [(time,reorder core)] (evt_to_color pid l)
			 (Printer.Label (evt_to_string pid l event_counts))
			 (Some (Printf.sprintf "event: %s" cmd))));
		  (optional gco
		     (function gco ->
		       Printer.smallnotch gco [(time,pid)] (evt_to_color pid l)
			 (Printer.Label (evt_to_string pid l event_counts))
			 (Some (Printf.sprintf "event: %s" cmd))))
		end);
	      if not !nas_events || nas pid cmd (ref false) then
		match l with
		  Parse_line.Sched_process_fork(_,_,_,child_pid) ->
		    if not !no_fork_lines
		    then
		      (optional go
			 (function go ->
			   try
			     let (child_core,child_time) =
			       Hashtbl.find first_appearance child_pid in
			     let (color,msg) =
			       if !hyperfork
			       then
				 if abs(core - child_core) = 80
				 then ("red","process hyperfork")
				 else ("black","process fork")
			       else ("black","process fork") in
			     Printer.dots go [(time,reorder core);(child_time,reorder child_core)]
			       (Printer.Color color) (Printer.Label msg)
			       (Some
				  (Printf.sprintf "child_pid: %d child_core: %d child_time: %f"
				     child_pid child_core child_time))
			   with _ -> ()))
		| Parse_line.Sched_migrate_task(cmd,mpid,oldcpu,newcpu,state) ->
		    let compatible_migration state =
		      !migrate_type = [] ||
		      (match state with
			Parse_line.Woken -> List.mem LB !migrate_type
		      | Parse_line.Waking _ -> List.mem Unblock !migrate_type
		      | Parse_line.Numa _ -> List.mem Numa !migrate_type) in
		    (optional go
		       (function go ->
			 if not !no_migrate_arrows && compatible_migration state && (not !nas_events || nas pid cmd (ref false))
			 then
			   begin
			     let message =
			       Printer.Label
				 (match state with
				   Parse_line.Woken ->
				     if oldcpu mod !numsockets = newcpu mod !numsockets
				     then "on-socket \\\nload balance"
				     else "off-socket \\\nload balance"
				 | Parse_line.Waking _ ->
				     if oldcpu mod !numsockets = newcpu mod !numsockets
				     then "on-socket \\\nunblock placement"
				     else "off-socket \\\nunblock placement"
				 | Parse_line.Numa _ -> "numa balancing") in
			     let comment =
			       Some(Printf.sprintf "migrate pid %d : core %d -> core %d on core %d"
				      mpid oldcpu newcpu core) in
			     Printer.arrow go [(time,reorder oldcpu);(time,reorder newcpu)]
			       (migrate_color oldcpu newcpu state)
			       message comment;
			     if nas pid cmd (ref false)
			     then
			       begin
				 (optional gco
				    (function gco ->
				      Printer.mark "x" gco [(time,pid)]
					(migrate_color oldcpu newcpu state) message comment));
				 List.iter
				   (function gso ->
				     Printer.mark "x" gso [(time,pid)]
				       (migrate_color oldcpu newcpu state) message comment)
				   gso
			       end
			   end))
		| _ -> ())
	    sched_events);
	(if not !no_events
	then
	  (optional go
	     (function go ->
	       table_iterate ticks
		 (fun core (time,freq) ->
		   Printer.mark "circle" go [(time,reorder core)] (Printer.Color "black")
		     (Printer.Label "tick") None);
	       let function_names = Hashtbl.create 101 in
	       table_iterate functions
		 (fun core (time,function_name) ->
		   if !target_functions == [] || List.mem function_name !target_functions
		   then
		     let color =
		       try Hashtbl.find function_names function_name
		       with Not_found ->
			 begin
			   let r = next_color() in
			   Hashtbl.add function_names function_name r;
			   r
			 end in
		     Printer.mark "x" go [(time,reorder core)] (Printer.Choose color)
		       (Printer.Label function_name) None))));
	(if !generate_frequencies_graph || !generate_merged_graph
	then do_color_by_frequency base go xlabel start startpoint endpoint last_time ticks corestate);
	(if !sleep_types
	then
	  do_sleep_types startpoint endpoint sleep_trace [go]
	    (gco::(List.map (fun x -> Some x) gso)));
	(if !migrate_stats
	then do_migrate_stats base migrate_stat));
      runtime ("end graph "^base) (fun _ ->
	(optional go
	   (function go -> Printer.endgraph go !save_tmp !tmp));
	(optional gco (function gco -> Printer.endgraph gco !save_tmp !tmp));
	List.iter (fun gso -> Printer.endgraph gso !save_tmp !tmp) gso))
    ranges
    
(* ----------------------------------------------------- *)
    
let endpoint = ref ("",0.0,-1.0)
let files = ref []

let update_migrate_type s =
  no_migrate_arrows := false;
  match s with
    "load" -> migrate_type := LB :: !migrate_type
  | "unblock" -> migrate_type := Unblock :: !migrate_type
  | "numa" -> migrate_type := Numa :: !migrate_type
  | _ -> failwith "expected load|unblock|numa"

let options =
  ["--debug", Arg.Set debug, "  giving timing info";
    "--target", Arg.String (fun s -> target := s :: !target),
    "  programs of interest";
    "--others",
    Arg.Unit
      (fun _ ->
	let (str,mn,mx) = !endpoint in
	endpoint := (Printf.sprintf "_others%s" str,mn,mx);
	others := true),
    "  everything except the target";
    "--idle",
    Arg.Unit
      (fun _ ->
	let (str,mn,mx) = !endpoint in
	endpoint := (Printf.sprintf "_idle%s" str,mn,mx);
	requested_something := true;
	has_requested_pids := true;
	Hashtbl.add requested_pids 0 ()),
    "  only idle";
    "--target-functions",
    Arg.String
      (fun s ->
        let l = Str.split (Str.regexp ",") s in
        List.iter (fun s -> target_functions := s :: !target_functions) l),
    "  functions of interest (if empty, all functions are displayed)";
    "--min",
    Arg.String
      (fun s ->
	let (str,mn,mx) = !endpoint in
	endpoint := (Printf.sprintf "_from_%s%s" s str,float_of_string s,mx)),
    "  min time to show on the x axis";
    "--max",
    Arg.String
      (fun s ->
	let (str,mn,mx) = !endpoint in
	endpoint := (Printf.sprintf "%s_upto_%s" str s,mn,float_of_string s)),
    "  max time to show on the x axis";
    "--sockets", Arg.Int (fun x -> sockets := x :: !sockets),
    "  number of sockets (#cores/2 for SMP)";
    "--save-tmp", Arg.Set save_tmp, "  save .jgr files";
    "--no-events", Arg.Set no_events, "  doesn't display scheduling events";
    "--other-events", Arg.Set get_other_events, "  trace_printk events";
    "--io-events",
    Arg.Unit (fun _ -> io_events := true; no_fork_lines := true; no_migrate_arrows := true),
    "  sched switch io events only";
    "--target-events", Arg.Set nas_events, "  display scheduling events for target commands";
    "--no-fork-lines", Arg.Set no_fork_lines, "  doesn't display lines between a fork and the birth of the child";
    "--generate-frequencies-graph", Arg.Set generate_frequencies_graph, "  generates the graph with the frequency of each core";
    "--freq", Arg.Set generate_frequencies_graph, "  generates the graph with the frequency of each core";
    "--mfreq", Arg.Set generate_merged_graph, "  generates the graph with the occupying thread and the frequency of each core";
    "--no-migrate-arrows", Arg.Set no_migrate_arrows, "  doesn't display arrows between cores of a migration";
    "--migrate-type", Arg.String update_migrate_type, "  load|unblock|numa (cumulative)";
    "--waking-actor-only", Arg.Set waking_actor_only, "  waking actor only";
    "-q", Arg.Unit
      (fun _ -> no_events := true; no_fork_lines := true;
	no_migrate_arrows := true; generate_frequencies_graph := false),
    "  minimal output";
    "--migration-level",
    Arg.Int
      (fun n ->
	migrations := true;
	match n with
	  160 -> migsockets := 160
	| 80 -> migsockets := 80
	| 4 -> migsockets := 4
	| _ -> failwith "unknown"),
    "  migration level to report (160, 80, or 4)";
    "--print-everything", Arg.Set print_everything,
    "  color all edges, no core graphs";
    "--color-by-command",
    Arg.Unit
      (fun _ ->
	color_by_command := true;
	let (str,mn,mx) = !endpoint in
        endpoint := (Printf.sprintf "_color%s" str,mn,mx)),
    "  color by command";
    "--color-by-top-command",
    Arg.Unit
      (fun _ ->
	color_by_command := true;
	top_commands := true;
	let (str,mn,mx) = !endpoint in
        endpoint := (Printf.sprintf "_color%s" str,mn,mx)),
    "  color by 25 most frequently activated commands";
    "--color-by-pid", Arg.Int (fun n -> color_by_pid := n :: !color_by_pid),
    "  pid that is the starting point of a color";
    "--ps", Arg.Unit (fun _ -> ty := Printer.JgraphPs),
    "  generate graphs in postscript (faster, but bigger files)";
    "--latex", Arg.Unit (fun _ -> ty := Printer.Latex),
    "  make graphs with LaTeX";
    "--picture", Arg.Unit (fun _ -> ty := Printer.Latex2),
    "  make graphs with LaTeX picture";
    "--python", Arg.Unit (fun _ -> ty := Printer.Python),
    "  make graphs with Python";
    "--nograph", Arg.Unit (fun _ -> ty := Printer.Nograph),
    "  no output graph";
    "--cores", Arg.Int (fun i -> cores := i; cores_info := SPECIFIED), "  number of cores";
    "--numsockets", Arg.Set_int numsockets, "  number of sockets";
    "--relaxed", Arg.Set relaxed, "  merge in non-target executions";
    "--minsleep",
    Arg.Int (fun n -> relaxed := true; minsleep := (float_of_int n) /. 1000000.),
    "  ignore pauses of less than N microseconds (implies relaxed)";
    "--pid",
    Arg.Int
      (function n ->
	requested_something := true;
	has_requested_pids := true;
	Hashtbl.add requested_pids n ();
	let (str,mn,mx) = !endpoint in
        endpoint := (Printf.sprintf "_%d%s" n str,mn,mx)),
    "  requested pid";
    "--cmd",
    Arg.String
      (fun s ->
	match Str.split (Str.regexp_string "---") s with
	  [cmd;nth] ->
	    requested_something := true;
	    requested_command := Some(cmd,int_of_string nth)
	| _ -> failwith "bad --cmd value"),
    "  requested command---nth";
    "--hyper", Arg.Set hyper, "  renumber cores to put hyperthreads adjacent";
    "--hyperfork", Arg.Set hyperfork,
    "  red edge for fork to hyperthread";
    "--colors", Arg.Int Printer.ncolors, "  number of colors";
    "--print-sizes", Arg.Set print_sizes, "  print sizes of edges";
    "--show", Arg.String (fun s -> show := s :: !show),
    "  focus on the execution parallel with a specific command - when used with freq only shows that command and its children, when used without freq shows everything in the time range";
    "--show-list", Arg.String (fun s -> show := (Util.cmd_to_list ("cat "^s)) @ !show),
    "  focus on the execution parallel with the list of commands in a file";
    "--cores-with-target", Arg.Set cores_with_target,
    "  only show cores with the target commands";
    "--highlight-non-target", Arg.Set highlight_non_targets,
    "  put a mark for a switch to a non target";
    "--after-sleep", Arg.Set after_sleep,
    "  skip the first sleep in the execution";
    "--sleep-types", Arg.Set sleep_types,
    "  record the reason when a thread sleeps (blocks)";
    "--socket-order",
    Arg.Unit
      (fun _ ->
	socket_order := true;
	let (str,mn,mx) = !endpoint in
        endpoint := (Printf.sprintf "_sockorder%s" str,mn,mx)),
    "  print cores by socket";
    "--migrate-stats", Arg.Set migrate_stats,
    "  count the number of various kinds of migrations";
    "--no-normalize", Arg.Set no_normalize,
    "  don't normalize command names";
    "--tmp", Arg.String (fun s -> tmp := Some s),
    "  directory for temporary files";
    "--outdir", Arg.Set_string outdir, "  output directory (currently only used for freq info)"
  ]

let anonymous s = files := Util.cmd_to_list (Printf.sprintf "ls %s" s) @ !files

let usage = "dat2graph file.dat [options]"

let _ =
  Arg.parse (Arg.align options) anonymous usage;
  (if !color_by_command && !target = [] then print_everything := true);
  (if not(!color_by_command || !cores_with_target) then no_normalize := true);
  (if !show <> [] && !migrate_stats
  then failwith "show and migrate_stats are not compatible");
  Util.choose_target target !files;
  let events =
    if !generate_frequencies_graph
    then
      let events = ["bprint"] in
      let events =
	if !migrate_stats
	then "sched_migrate_task"::events
	else events in
      if !show <> [] || !has_requested_pids
      then events @ ["sched_switch";"sched_process_exec";"sched_process_fork"]
      else events
    else if !no_events && !no_fork_lines && !no_migrate_arrows && not !color_by_command
    then
      let events =
	if !generate_merged_graph then ["sched_switch";"bprint"] else ["sched_switch"] in
      if !show <> [] || !has_requested_pids
      then events @ ["sched_process_exec";"sched_process_fork"]
      else events
    else [] in
  let files =
    runtime "get_files"
      (fun _ -> Util.get_files !tmp !files !endpoint !after_sleep true events) in
  let endpoint =
    runtime "get_endpoint" (fun _ -> Util.get_endpoint files !endpoint) in
  List.iter (main endpoint) files
