(* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * file license.txt for more details.
 *)

type ty = Jgraph | JgraphPs | Latex | Latex2 | Python | Nograph
type graph =
    out_channel * string * ty * float *
      float option * float option * string list ref
type color = RGB of float * float * float | Choose of int | SChoose of string | Color of string

val ncolors : int -> unit

type ymax = Nomax | Ymax of float | Ylist of int list

val startgraph :
    string ->  ty -> float ->
      int -> float option -> float option -> string -> (* x information *)
	int -> int option -> ymax -> string -> (* y information *)
	  graph

val endgraph : graph -> bool -> string option -> unit

type label = NoLabel | Label of string | FontLabel of int * string

type edgetype =
    graph -> (float * int) list -> color -> label -> string option -> unit

type fedgetype =
    graph -> (float * float) list -> color -> label -> string option -> unit

val jgraphcolor : color -> string
val latexcolor : color -> string
val pythoncolor : color -> float * float * float

val text  : graph -> (float * string) list -> unit

val edge  : edgetype
val fedge : fedgetype
val dots  : edgetype
val fdots : fedgetype
val arrow : edgetype
val notch : edgetype
val smallnotch : edgetype
val mark  : string -> edgetype
val fmark  : string -> fedgetype
val markonly : string -> edgetype
val fmarkonly : string -> fedgetype

val print_text : graph -> float -> float -> string -> unit

val command_color : (string,color)   Hashtbl.t
val command_count : (string,int * int ref) Hashtbl.t
val assign_colors : unit -> unit
val command_to_color : string -> bool -> color
val inc_command_count : string -> unit
val set_command_count : string -> int -> unit
val get_command_count : string -> int

(* -------------------------------------- *)
(* TikZ *)

val acm_latex_starter : out_channel -> unit
val latex_starter : out_channel -> unit
val latex_ender : out_channel -> unit

type width = BIG | MIDDLE of int | SMALL | TINY
val start_tikz_graph :
    out_channel -> string list -> string -> int -> string -> bool -> float -> float -> width -> float -> bool -> int -> bool -> bool -> float -> bool -> string -> unit
val tikz_quiet_bar_plot : string -> out_channel -> (int * float * float * string) list -> unit
val tikz_bar_plot : out_channel -> (int * float * float * string) list -> unit
val tikz_edge_plot : out_channel -> string -> (int * float) list -> int -> float -> bool -> unit
val tikz_mark_plot : out_channel -> string -> string -> (int * float) list -> unit
val tikz_mark_fplot : out_channel -> string ->string -> (float * float) list -> unit
val end_tikz_graph : out_channel -> int -> float -> float -> string list -> bool -> bool -> unit
