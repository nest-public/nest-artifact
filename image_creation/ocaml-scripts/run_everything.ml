(* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * file license.txt for more details.
 *)

(* Run tests on multiple kernels.

This requires sudo.

The --config command line argument describes the tests and the kernels.
It contains the infomation illustrated by the following example.
The different entries can appear any number of times.
Entries must be separated by a blank line.

The location is the directory from which both the preparation and the
test are run.  If the preparation changes directory, then a hyperfine
test doesn't see that.

[global]
  SRC = /home/julia/linux2 // define any abbreviations: all caps
  dependency = autoconf    // stuff to install with apt

[machine]
  name = i80               // hostname -s, up to first hyphen
  SRC = /home/julia/linux2 // define any abbreviations: all caps
  dependency = autoconf    // stuff to install with apt

[kernel]
  name = 5.9.0
  abbreviation = cfs       // default is name
  governor = powersave     // default is schedutil
  mode = active            // default is passive
  taskset_required = false // default is true, number allowed
  perfcom = /home/julia/bin/linux-perf-5.9 // required for using perf, default is ""
  preparation = echo something // default is nothing
  automatic = false        // default is true, false if should not be in the rotation

[test]
  home = /some/file        // if location doesn't exist then cp -r from here
  preparation = make distclean defconfig
  test = make -j 160 net/ipv4/ip_tunnel.o // multiple possible, to run in parallel
  abbreviation = iptunnel
  location = /home/julia/linux2 // no default, this is required
  warmups = 2              // default is 0
  trace_cmd = 3            // default is 1, 0 to disable
  trace_cmd_options = -e sched // default is "", ie no extra options
  hyperfine = 30           // default is 10, 0 to disable
  turbostat = 30           // default is 0, ie disabled
  perf = 5                 // default is 0, ie disabled
  perfevents = instructions,cycles // instructions per cycle, requires perf > 0, doesn't accumulate
  show_output = false      // default is false (put all output on the screen)
  automatic = false        // default is true, false if requires --test
  initial = echo start     // operation to perform before all tests
  final = echo done        // operation to perform after all tests
  machines = dahu-1,yeti-2 // allowed machines names, ignored if no intersection
  save_test_output = true  // default is false, save stderr of tests (not warmup)

[output]
  subdir = traces          // default is .
*)

type debug_level = PRINT | FULL | LIGHT | COMMANDS | NODEBUG

let default_location = ref "BAD LOCATION"
let debug = ref NODEBUG
let continue = ref false
let tracecmdmax = ref (-1)
let hypermax = ref (-1)
let perfmax = ref (-1)
let turbomax = ref (-1)
let trace_cmd_args = ref ""
let tracecmdfixed = ref (-1)
let hyperfixed = ref (-1)
let perffixed = ref (-1)
let turbofixed = ref (-1)
let cores = ref 0
let (realhost,fullhost,host) =
  let h = List.hd(Util.cmd_to_list "hostname -s") in
  (List.hd (Str.split (Str.regexp_string "-") h),h,"_"^h)
let quick = ref false
let interrupts = ref false
let turbofrom = ref 0
let force_machine = ref false

let env_vars = ref []

let intel h =
  not (List.mem h ["gruss";"neowise"])

let resolve_env skip env l =
  if env = []
  then Str.split (Str.regexp "[ \t]+") l
  else
    let pieces = Str.split (Str.regexp "\\b") l in
    let changed = ref false in
    let get x =
      try
	let res = List.assoc x env in
	changed := true;
	res
      with Not_found ->
	begin
	  (if List.mem x !env_vars then skip := true);
	  x
	end in
    let pieces = List.map get pieces in
    let l =
      if !changed
      then String.concat "" pieces
      else l in
    Str.split (Str.regexp "[ \t]+") l

type machine_info = {
    machine  : string;
    env : (string * string) list;
    dependencies : string list }

let default_env _ =
    try
      begin
	let key = "USER" in
	let name = Sys.getenv "LC_MYNAME" in
	(if not(List.mem key !env_vars) then env_vars := key :: !env_vars);
        env_vars := key :: !env_vars;
        [(key,name)]
      end
    with _ -> []

let read_machine_config global i machines =
  let name = ref "" in
  let env = ref (default_env()) in
  let dep = ref [] in
  let alldone _ =
    (if !name = ""
    then failwith "name required\n");
    let res =
      { machine = !name;
        env = !env;
        dependencies = !dep } in
    machines := res :: !machines in
  let rec loop _ =
    let l = input_line i in
    let l = String.trim l in
    match l with
      "" -> alldone()
    | _ ->
	(if String.get l 0 <> '#'
	then match Str.split (Str.regexp "[ \t]+") l with
	  ["name";"=";nm] when not global -> name := nm
	| ["dependency";"=";nm] -> dep := nm :: !dep
	| key::"="::vl   ->
	    let skip = ref false in
	    let vl = String.concat " " vl in
	    let vl = resolve_env skip !env vl in
	    let vl = String.concat " " vl in
	    if not !skip
	    then
	    begin
	      (if not(List.mem key !env_vars) then env_vars := key :: !env_vars);
	      env := (key,vl) :: !env
	    end
	| l -> failwith ("unexpected machine/global tag: "^(String.concat " " l)));
	loop() in
  try loop() with End_of_file -> alldone()

type kernel_info = {
    kernel   : string;
    abbrv    : string;
    prep     : string;
    governor : string;
    mode     : string;
    taskset_required : int;
    perfcom  : string;
    kautomatic : bool }

let read_kernel_config i env kernels =
  let name = ref "" in
  let abbreviation = ref "" in
  let prep = ref "" in
  let governor = ref "schedutil" in
  let mode = ref "passive" in
  let taskset_required = ref 1 in
  let perfcom = ref "" in
  let automatic = ref true in
  let skip = ref false in
  let alldone _ =
    if !skip
    then skip := false
    else
      begin
        (if !name = ""
        then failwith "name required\n");
        let abbreviation =
          if !abbreviation = ""
          then !name
          else !abbreviation in
        let res =
          { kernel = !name;
    	abbrv = abbreviation;
    	prep = !prep;
    	governor = !governor;
    	mode = !mode;
    	taskset_required = !taskset_required;
    	perfcom = !perfcom;
    	kautomatic = !automatic } in
        kernels := res :: !kernels
      end in
  let rec loop _ =
    let l = input_line i in
    let l = String.trim l in
    match l with
      "" -> alldone()
    | _ ->
	(if String.get l 0 <> '#'
	then
  	  (let str = resolve_env skip env l in
	  match str with
	    ["name";"=";nm]                  -> name := nm
	  | ["abbreviation";"=";ab]          -> abbreviation := ab
	  | "preparation"::"="::rest         -> prep  := String.concat " " rest
	  | ["governor";"=";gv]              -> governor := gv
	  | ["mode";"=";md]                  -> mode := md
	  | ["perfcom";"=";pc]               -> perfcom := pc
	  | ["taskset_required";"=";"true"]  -> taskset_required := 1
	  | ["taskset_required";"=";"false"] -> taskset_required := 0
	  | ["taskset_required";"=";n]       -> taskset_required := int_of_string n
	  | ["automatic";"=";"true"]         -> automatic := true
	  | ["automatic";"=";"false"]        -> automatic := false
	  | _ -> failwith ("unexpected kernel tag: "^(String.concat "|" str))));
	loop() in
  try loop() with End_of_file -> alldone()

type test_info =
    { prep      : string;
      test      : string list;
      original_test : string list;
      abbreviation : string;
      home      : string;
      location  : string;
      trace_cmd : int;
      trace_cmd_options : string;
      hyperfine : int;
      turbostat : int;
      perf      : int;
      perfevents: string list;
      warmups   : int;
      automatic : bool;
      show_output : bool;
      save_test_output : bool;
      machines  : string list;
      initial   : string;
      final     : string }

let bug_report = ref ""

let silence n show_output save_output save_test_output test =
  if show_output
  then test
  else
    let dest =
      if save_output (* for warmups, etc *)
      then " >> " ^ !bug_report ^ " 2>&1"
      else
	match save_test_output with
	  None -> " > /dev/null 2>&1"
	| Some s -> " > /dev/null 2>> " ^ s in
    let do_silence sep test =
      let pieces =
	Str.split (Str.regexp sep) test in
      let pieces = List.map String.trim pieces in
      let rec loop = function
	  [] -> []
	| [x] -> [x]
	| x::xs ->
	    let last = String.get x (String.length x - 1) in
	    let contains_gtr = List.length (Str.split (Str.regexp_string ">") x) > 1 in
	    let newx =
	    if last = ')'
	    then x^sep
	    else if contains_gtr
	    then Printf.sprintf "(%s)%s%s" x dest sep
	    else (x ^ dest ^ sep) in
	    newx :: loop xs in
      String.concat "" (loop pieces) in
    let front = do_silence " || " (do_silence " & " (do_silence " && " test)) in
    let contains_gtr = List.length (Str.split (Str.regexp_string ">") front) > 1 in
    if contains_gtr
    then Printf.sprintf "(%s) %s" front dest
    else front ^ dest

let update_taskset taskset_required test =
  match taskset_required with
    0 -> test
  | 1 -> Printf.sprintf "taskset -c 0-%d %s" (!cores - 1) test
  | n -> Printf.sprintf "taskset -c 0-%d %s" n test

let uncore s =
  let pieces = Str.split_delim (Str.regexp "CORES") s in
  String.concat (string_of_int !cores) pieces

let read_test_config i env default_test tests =
  let prep = ref default_test.prep in
  let test = ref default_test.test in
  let abbreviation = ref default_test.abbreviation in
  let location = ref default_test.location in
  let home = ref default_test.home in
  let trace_cmd = ref default_test.trace_cmd in
  let trace_cmd_options = ref default_test.trace_cmd_options in
  let hyperfine = ref default_test.hyperfine in
  let turbostat = ref default_test.turbostat in
  let perf = ref default_test.perf in
  let perfevents = ref default_test.perfevents in
  let warmups = ref default_test.warmups in
  let show_output = ref default_test.show_output in
  let save_test_output = ref default_test.save_test_output in
  let machines = ref [] in
  let initial = ref default_test.initial in
  let final = ref default_test.final in
  let automatic = ref default_test.automatic in
  let skip = ref false in
  let alldone _ =
    if !skip
    then skip := false
    else
      let prep = silence 1 (!show_output || !prep = "" || !prep = default_test.prep) true None !prep in
      let initial = silence 2 (!show_output || !initial = "" || !initial = default_test.initial) true None !initial in
      let final = silence 3 (!show_output || !final = "" || !final = default_test.final) true None !final in
      let cmin x y = if x < 0 then y else min x y in
      let override x y = if x >= 0 then x else y in
      let res =
        { prep = prep;
          test = !test;
          original_test = !test;
          abbreviation = !abbreviation;
          location  = !location;
          home  = !home;
          trace_cmd  = override !tracecmdfixed (cmin !tracecmdmax !trace_cmd);
	  trace_cmd_options = !trace_cmd_options;
          hyperfine = override !hyperfixed (cmin !hypermax !hyperfine);
          turbostat = override !turbofixed (cmin !turbomax !turbostat);
          perf = override !perffixed (cmin !perfmax !perf);
	  perfevents = !perfevents;
          warmups = !warmups;
          automatic = !automatic;
          show_output = !show_output;
	  save_test_output = !save_test_output;
	  machines = !machines;
          initial = initial;
          final = final } in
      tests := res :: !tests in
  let rec loop _ =
    let l = input_line i in
    let l = String.trim l in
    match l with
      "" -> alldone()
    | _ ->
	(if String.get l 0 <> '#'
	then
	  match resolve_env skip env l with
	    "preparation"::"="::rest    -> prep  := uncore(String.concat " " rest)
	  | "test"::"="::rest           -> test  := uncore(String.concat " " rest) :: !test
	  | "initial"::"="::rest        -> initial := uncore(String.concat " " rest)
	  | "final"::"="::rest          -> final := uncore(String.concat " " rest)
	  | ["abbreviation";"=";ab]     -> abbreviation := ab
	  | ["location";"=";l]          -> location := l
	  | ["home";"=";h]              -> home := h
	  | ["trace_cmd";"=";rs]        -> trace_cmd := int_of_string rs
	  | "trace_cmd_options"::"="::rest -> trace_cmd_options := uncore(String.concat " " rest)
	  | ["hyperfine";"=";rs]        -> hyperfine := int_of_string rs
	  | ["turbostat";"=";rs]        -> turbostat := int_of_string rs
	  | ["perf";"=";rs]             -> perf := int_of_string rs
	  | ["perfevents";"=";events]   -> perfevents := Str.split (Str.regexp ", *") events
	  | ["warmups";"=";wm]          -> warmups := int_of_string wm
	  | ["show_output";"=";"true"]  -> show_output := true
	  | ["show_output";"=";"false"] -> show_output := false
	  | ["save_test_output";"=";"true"]  -> save_test_output := true
	  | ["save_test_output";"=";"false"] -> save_test_output := false
	  | ["automatic";"=";"true"]    -> automatic := true
	  | ["automatic";"=";"false"]   -> automatic := false
	  | "machines"::"="::rest       ->
	      let rest = String.concat " " rest in
	      machines := (Str.split (Str.regexp " *, *") rest) @ !machines
	  | _ -> failwith ("unexpected test tag: "^l));
	loop() in
  try loop() with End_of_file -> alldone()

let read_output_config i env output =
  let l = input_line i in
  let l = String.trim l in
  let skip = ref false in
  let dest = resolve_env skip env l in
  (if !skip then failwith "destination must be provided");
  match dest with
    ["subdir";"=";sd] ->
      let sd =
	if String.get sd 0 = '/'
	then sd 
	else Sys.getcwd() ^ "/" ^ sd in
      ignore(Sys.command ("mkdir -p "^sd));
      output := sd ^ "/"
  | [] -> output := Sys.getcwd() ^ "/"
  | _ -> failwith "unexpected output tag"

let skip_config i =
  let rec loop _ =
    let l = input_line i in
    let l = String.trim l in
    if not(l = "") then loop() in
  loop ()

let read_machines_config i =
  let machines = ref [] in
  let globals = ref [] in
  let rec loop i =
    let l = input_line i in
    let l = String.trim l in
    match l with
      "[machine]" -> read_machine_config false i machines; loop i
    | "[global]"  -> read_machine_config true i globals; loop i
    | "[kernel]"  -> skip_config i; loop i
    | "[test]"    -> skip_config i; loop i
    | "[default test]" -> skip_config i; loop i
    | "[output]"  -> skip_config i; loop i
    | "" -> loop i
    | _ ->
	if String.get l 0 = '#'
	then
	  match Str.split (Str.regexp "[ \t]+") l with
	    ["#include";inc] ->
	      let i1 = open_in inc in
	      (try loop i1 with End_of_file -> (close_in i1; loop i))
	  | _ -> loop i
	else failwith ("unrecognized configuration information: "^l) in
  try loop i
  with End_of_file ->
    let machines = List.rev !machines in
    let globals = List.rev !globals in
    List.map
      (function machine ->
	List.fold_left
	  (fun prev cur ->
	    {prev with
	      env = cur.env @ prev.env;
	      dependencies = cur.dependencies @ prev.dependencies})
	  machine globals)
      machines

let read_config env i =
  let kernels = ref [] in
  let tests   = ref [] in
  let output  = ref "" in
  let default_test =
    ref
      { prep = "";
	test = [];
	original_test = [];
	abbreviation = "";
	home = "";
	location = !default_location;
	trace_cmd = 1;
	trace_cmd_options = "";
	hyperfine = 10;
	turbostat = 0;
	perf = 0;
	perfevents = [];
	warmups = 0;
	automatic = true;
	show_output = (!debug = FULL);
	save_test_output = false;
	machines = [];
	initial = "";
	final = "" } in
  let rec loop i =
    let l = input_line i in
    let l = String.trim l in
    match l with
      "[machine]" -> skip_config i; loop i
    | "[global]"  -> skip_config i; loop i
    | "[kernel]"  -> read_kernel_config i env kernels; loop i
    | "[test]"    -> read_test_config i env !default_test tests; loop i
    | "[default test]" ->
	let res = ref [] in
	read_test_config i env !default_test res;
	(match !res with
	  [res] -> default_test := res
	| _ -> ());
	loop i
    | "[output]"  -> read_output_config i env output; loop i
    | "" -> loop i
    | _ ->
	if String.get l 0 = '#'
	then
	  match Str.split (Str.regexp "[ \t]+") l with
	    ["#include";inc] ->
	      let i1 = open_in inc in
	      (try loop i1 with End_of_file -> (close_in i1; loop i))
	  | _ -> loop i
	else failwith ("unrecognized configuration information: "^l) in
  try loop i
  with End_of_file ->
    (if !output = "" then failwith "output destination needed");
    (List.rev !kernels, List.rev !tests, !output)

(* ----------------------------------------------------- *)

let get_kernel _ =
  List.hd (Util.cmd_to_list "uname -r")

let pre home location =
  if Sys.file_exists location
  then false
  else
    begin
      (if home = ""
      then ignore(Sys.command("mkdir "^location))
      else ignore(Sys.command(Printf.sprintf "cp -r %s %s" home location)));
      true
    end

let post cleanup location =
  if cleanup
  then ignore(Sys.command("/bin/rm -rf "^location))

let set_location home location fn =
  let cleanup = pre home location in
  fn();
  post cleanup location

let failo = ref stderr

let check n location cmd =
  let cmd = Printf.sprintf "cd %s; %s" location cmd in
  (if List.mem !debug [PRINT;FULL;COMMANDS] then (Printf.eprintf "%d: running %s\n" n cmd; flush stderr));
  if !debug <> PRINT
  then
    match Sys.command cmd with
      0 -> ()
    | n ->
	Printf.fprintf !failo "\n\n\n\n===============\n";
	Printf.fprintf !failo "command '%s' failed\n" cmd; flush !failo;
	List.iter
	  (function x -> Printf.fprintf !failo "%s\n" x)
	  (Util.cmd_to_list ("cat " ^ !bug_report));
	Printf.fprintf !failo "===============\n\n\n\n";
	flush !failo;
        failwith (Printf.sprintf "cmd: returns exit code %d" n)

let set_governor kernel_info = (* governor and any other preparation *)
  let gov = kernel_info.governor in
  let mode = kernel_info.mode in
  let prep = kernel_info.prep in
  (if intel realhost
  then
    check 1 (Sys.getcwd())
      (Printf.sprintf
         "echo %s | tee /sys/devices/system/cpu/intel_pstate/status"
         mode));
  check 2 (Sys.getcwd())
    (Printf.sprintf
       "for i in /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor ; do echo %s > $i; done"
       gov);
  (if prep <> ""
  then check 3 (Sys.getcwd()) prep)

let warmups from test_info =
  let prep = test_info.prep in
  let test = List.hd (test_info.test) in
  let show_output = test_info.show_output in
  let location = test_info.location in
  let home = test_info.home in
  let warmups = test_info.warmups in
  set_location home location
    (fun _ ->
      for i = 1 to warmups do
	(if prep <> ""
	then check 4 location prep);
	check from location (silence 4 show_output true None test)
      done)

let mean l =
  (List.fold_left (+.) 0.0 l) /. (float_of_int (List.length l))

let stddev l =
  let lenm1 = float_of_int(List.length l - 1) in
  let mean = mean l in
  let squares =
    List.fold_left
      (fun prev xi ->
	let tmp = xi -. mean in
	(tmp *. tmp) +. prev)
      0.0 l in
  sqrt(squares /. lenm1)

let check_continue runs output =
  if runs > 0 && !continue && Sys.file_exists output
  then 0
  else runs

let mkcsv output abbreviation kernel gov =
  Printf.sprintf "%s%s%s_%s_%s.csv" output abbreviation host kernel gov

let mkjson output abbreviation kernel gov =
  Printf.sprintf "%s%s%s_%s_%s.json" output abbreviation host kernel gov

let mkdat output abbreviation kernel gov n =
  Printf.sprintf "%s%s%s_%s_%s_%d.dat" output abbreviation host kernel gov n

let mkturbo output abbreviation kernel gov n =
  Printf.sprintf "%s%s%s_%s_%s_%d.turbo" output abbreviation host kernel gov n

let mkperf output abbreviation kernel gov =
  Printf.sprintf "%s%s%s_%s_%s.perf" output abbreviation host kernel gov

let mkstderr save_test_output file =
  if save_test_output
  then
    let so = file ^ ".stderr" in
    (if Sys.file_exists so then Sys.remove so);
    Some so
  else None

type numbers = English | French | Unknown
let numbers = ref Unknown

let perf warmed_up kernel_info test_info output =
  let kernel = kernel_info.abbrv in
  let gov = kernel_info.governor in
  let perfcom = kernel_info.perfcom in
  let prep = test_info.prep in
  let test = List.hd (test_info.test) in (* only one element due to wrapping *)
  let save_test_output = test_info.save_test_output in
  let show_output = test_info.show_output in
  let abbreviation = test_info.abbreviation in
  let location = test_info.location in
  let home = test_info.home in
  let perfs = test_info.perf in
  let perfs = check_continue perfs (mkperf output abbreviation kernel gov) in
  let perfevents = test_info.perfevents in
  if perfs > 0 && perfcom <> ""
  then
    begin
      let kernel = List.hd (Str.split (Str.regexp_string "-") kernel) in
      let file = mkperf output abbreviation kernel gov in
      (if not !warmed_up then warmups 200 test_info);
      warmed_up := true;
      let res = Hashtbl.create 101 in
      let outfile = Filename.temp_file "re" ".txt" in
      set_location home location
	(fun _ ->
	  let stderr = mkstderr save_test_output file in
	  for i = 1 to perfs do
	    (if prep <> ""
	    then check 6 location prep);
	    let cmd =
	      Printf.sprintf "%s stat -e %s -o %s %s"
		perfcom (String.concat "," perfevents) outfile
		(silence 5 show_output false stderr test) in
	    check 7 location cmd;
	    let i = open_in outfile in
	    (let rec loop _ =
	      let l = input_line i in
	      match List.rev(Str.split (Str.regexp " +") l) with
		"cycle"::"per"::"insn"::v::_ ->
		  Util.hashadd res "insn per cycle" (float_of_string(String.concat "." (Str.split (Str.regexp ",") v)))
	      | name::v when List.mem name perfevents ->
	          let v = String.concat "" v in
		  let rec fixnum v =
		    match !numbers with
		      Unknown ->
		        let pieces = Str.split (Str.regexp ",") v in
			(match pieces with
			  [v] -> float_of_string v
			| _::_::_::_ -> numbers := English; fixnum v
			| [a;b] ->
			    if List.length (Str.split (Str.regexp_string ".") a) >=2
			    then (numbers := French; fixnum v)
			    else if List.length (Str.split (Str.regexp_string ".") b) >=2
			    then (numbers := English; fixnum v)
			    else failwith "number system unclear"
			| [] -> failwith "no number")
		    | English ->
		        float_of_string(String.concat "" (Str.split (Str.regexp ",") v))
		    | French ->
			let v = String.concat "" (Str.split (Str.regexp_string ".") v) in
			float_of_string(String.concat "." (Str.split (Str.regexp ",") v)) in
		  Util.hashadd res name (fixnum v); (* not very safe... *)
		  loop()
	      | _ -> loop() in
	    try loop() with End_of_file -> ())
	  done);
	  let o = open_out file in
	  Hashtbl.iter
	    (fun k v ->
	      let res = !v in
	      let mean = mean res in
	      let stddev = stddev res in
	      let mx = List.fold_left max (List.hd res) (List.tl res) in
	      let mn = List.fold_left min (List.hd res) (List.tl res) in
	      Printf.fprintf o "info,mean perf,stddev perf,max perf,min perf,samples\n";
	      Printf.fprintf o "%s,%f,%f,%f,%f,%d" k mean stddev mx mn perfs;
	      Printf.fprintf o "%s\n" (String.concat "," (List.map string_of_float res)))
	    res;
	  close_out o
    end

let trace_cmd warmed_up kernel_info test_info output =
  let kernel = kernel_info.abbrv in
  let gov = kernel_info.governor in
  let taskset_required = kernel_info.taskset_required in
  let prep = test_info.prep in
  let test = List.hd (test_info.test) in (* only one element due to wrapping *)
  let show_output = test_info.show_output in
  let abbreviation = test_info.abbreviation in
  let location = test_info.location in
  let home = test_info.home in
  let tcruns = test_info.trace_cmd in
  let options = test_info.trace_cmd_options in
  let kernel = List.hd (Str.split (Str.regexp_string "-") kernel) in
  let tcruns =
    check_continue tcruns (mkdat output abbreviation kernel gov tcruns) in
  if tcruns > 0
  then
    begin
      (if not !warmed_up then warmups 201 test_info);
      warmed_up := true;
      let test = silence 6 show_output false None (update_taskset taskset_required test) in
      let (tmpfile,o) = Filename.open_temp_file "tmp" ".sh" in
      Printf.fprintf o "#!/bin/bash\nsleep 1\n%s\n" test;
      close_out o;
      ignore(Sys.command (Printf.sprintf "chmod 755 %s" tmpfile));
      set_location home location
	(fun _ ->
	  let tmp_output = Filename.temp_file "trace_cmd" "dat" in
	  for i = 1 to tcruns do
	    (if prep <> ""
	    then check 8 location prep);
	    let events =
	      if !quick
	      then "-e sched_switch"
	      else if !interrupts
	      then "-e irq -e sched -v -e sched_stat_runtime"
	      else "-e sched -v -e sched_stat_runtime" in
	    let events = events ^ " " ^ options in
	    let cmd =
	      Printf.sprintf
	        "trace-cmd record %s -q %s -s 1000000000 -b 250000 -o %s %s"
	        !trace_cmd_args (* -e power:cpu_frequency -e power:pstate_sample *)
		events tmp_output tmpfile in
	    check 9 location cmd;
	    let dest = mkdat output abbreviation kernel gov i in
	    check 900 location (Printf.sprintf "cp %s %s" tmp_output dest)
	  done);
      if !debug = NODEBUG then Sys.remove tmpfile
    end

let turbo warmed_up kernel_info test_info output =
  let kernel = kernel_info.abbrv in
  let gov = kernel_info.governor in
  let taskset_required = kernel_info.taskset_required in
  let prep = test_info.prep in
  let test = List.hd (test_info.test) in (* only one element due to wrapping *)
  let save_test_output = test_info.save_test_output in
  let show_output = test_info.show_output in
  let abbreviation = test_info.abbreviation in
  let location = test_info.location in
  let home = test_info.home in
  let tcruns = test_info.turbostat in
  let kernel = List.hd (Str.split (Str.regexp_string "-") kernel) in
  let tcruns =
    check_continue tcruns (mkturbo output abbreviation kernel gov tcruns) in
  if tcruns > 0
  then
    begin
      (if not !warmed_up then warmups 202 test_info);
      warmed_up := true;
      let stderr = mkstderr save_test_output (mkturbo output abbreviation kernel gov 0) in
      let test = silence 7 show_output false stderr (update_taskset taskset_required test) in
      let (tmpfile,o) = Filename.open_temp_file "tmp" ".sh" in
      Printf.fprintf o "#!/bin/bash\n%s\n" test;
      close_out o;
      ignore(Sys.command (Printf.sprintf "chmod 755 %s" tmpfile));
      set_location home location
	(fun _ ->
	  for i = 1 to tcruns do
	    (if prep <> ""
	    then check 10 location prep);
	    let cmd =
	      Printf.sprintf
		"turbostat --interval 0.5 --Joules --quiet --out %s %s"
		(mkturbo output abbreviation kernel gov (i + !turbofrom)) tmpfile in
	    check 11 location cmd
	  done);
      if !debug = NODEBUG then Sys.remove tmpfile
    end
      
let hyperfine warmed_up kernel_info test_info output =
  let kernel = kernel_info.abbrv in
  let gov = kernel_info.governor in
  let taskset_required = kernel_info.taskset_required in
  let prep = test_info.prep in
  let prep =
    if prep = ""
    then "sleep 1"
    else prep ^ " && sleep 1" in
  let test = List.hd (test_info.test) in (* only one element due to wrapping *)
  let save_test_output = test_info.save_test_output in
  let original_test = test_info.original_test in
  let show_output = test_info.show_output in
  let abbreviation = test_info.abbreviation in
  let location = test_info.location in
  let home = test_info.home in
  let hyperruns = test_info.hyperfine in
  let warmups = test_info.warmups in
  let kernel = List.hd (Str.split (Str.regexp_string "-") kernel) in
  (* same code for warmup and real run, so can't get output *)
  let hyperruns =
    check_continue hyperruns (mkjson output abbreviation kernel gov) in
  if hyperruns > 0
  then
    begin
      warmed_up := true;
      let csv_file = mkjson output abbreviation kernel gov in
      let so = mkstderr save_test_output csv_file in
      let test = silence 8 show_output false so (update_taskset taskset_required test) in
      let (tmpfile,o) = Filename.open_temp_file "tmp" ".sh" in
      Printf.fprintf o "#!/bin/bash\n%s\n" test;
      close_out o;
      ignore(Sys.command (Printf.sprintf "chmod 755 %s" tmpfile));
      let cmd =
	Printf.sprintf
	  "hyperfine %s %s %s --export-json %s %s '%s'"
	  (if !debug = NODEBUG then "" else "--show-output")
	  (if warmups > 0 then Printf.sprintf "-w%d" warmups else "")
	  (if hyperruns <> 10 then Printf.sprintf "-m%d" hyperruns else "")
	  csv_file
	  (if prep = "" then "" else Printf.sprintf "-p '%s'" prep)
	  tmpfile in
      set_location home location (fun _ -> check 12 location cmd);
      (if Sys.file_exists csv_file
      then
	let lines = Util.cmd_to_list ("cat "^csv_file) in
	let rec loop = function
	    [] -> []
	  | x::xs ->
	      match Str.bounded_split (Str.regexp ": ") x 2 with
		[bef;aft] ->
		  if String.trim bef = "\"command\""
		  then
		  let test =
		    let test =
		      match original_test with
			[t] -> t
		      | t::_ -> t ^ ", etc."
		      | _ -> failwith "test required" in
		    let pieces = Str.split (Str.regexp " ") test in
		    let rec loop = function
			">"::br::rest
			when Str.string_match (Str.regexp "/tmp/bug_report") br 0 ->
			  loop rest
		      | "2>&1"::rest -> loop rest
		      | ">"::"/dev/null"::rest -> loop rest
		      | x :: xs -> x :: loop xs
		      | [] -> [] in
		    String.concat " " (loop pieces) in
		  (Printf.sprintf "%s: \"%s\"," bef test) :: xs
		  else x :: loop xs
	      | _ -> x :: loop xs in
	let lines = loop lines in
	let o = open_out csv_file in
	List.iter (fun l -> Printf.fprintf o "%s\n" l) lines;
	close_out o);
      if !debug = NODEBUG then Sys.remove tmpfile
    end

let startup test_info =
  let initial = test_info.initial in
  if initial <> ""
  then
    begin
      let cleanup = pre test_info.home test_info.location in
      check 13 test_info.location initial;
      cleanup
    end
  else false

let cleanup c test_info =
  let final = test_info.final in
  (if final <> "" then check 14 test_info.location final);
  post c test_info.location

(* ----------------------------------------------------- *)

let get_kernels kernels =
  let rec group = function
      k::rest ->
	let name = k.kernel in
	let (same,diff) =
	  List.partition (function k1 -> name = k1.kernel)
	    rest in
	let first = k :: same in
	let auto = List.exists (fun k -> k.kautomatic) first in
	(name,auto,first) :: group diff
    | [] -> [] in
  let kernels = group kernels in
  let k = get_kernel() in
  let rec next_kernel = function
      (name,true,_)::rest -> Some name
    | _ -> None in
  let rec loop = function
      [] -> (None,next_kernel kernels)
    | (name,auto,kern1)::rest ->
	if k = name
	then (Some kern1,next_kernel rest)
	else loop rest in
  loop kernels

(* ----------------------------------------------------- *)

let resolve_tests avail = function
    [] ->
      List.filter (function test_info -> test_info.automatic) avail
  | wanted ->
      List.filter
	(function test_info -> List.mem test_info.abbreviation wanted)
	avail

let resolve_kernels avail = function
    [] -> avail
  | wanted ->
      let wanted =
	List.map
	  (function k ->
	    match Str.split (Str.regexp_string "---") k with
	      [ker] -> ((ker,None),(k,false))
	    | [ker;g] -> ((ker,Some g),(k,true))
	    | _ -> failwith "invalid kernel specification")
	  wanted in
      let seen = ref [] in
      let res =
	  List.filter
	    (function kernel_info ->
	      try
	        let (k,_) =
		  List.assoc (kernel_info.abbrv,None) wanted in
	        seen := k :: !seen;
	        true
	      with Not_found ->
	        try
		  let (k,_) =
		    List.assoc (kernel_info.abbrv,Some kernel_info.governor) wanted in
		  seen := k :: !seen;
		  true
	        with Not_found -> false)
	    avail in
        List.iter
	  (function (w,withgov) ->
	    if not (List.mem w !seen)
	    then
	      begin
	        Printf.eprintf "Missing: %s\n" w;
	        Printf.eprintf "Available: %s\n"
		  (String.concat " "
		     (List.map
		        (fun kernel_info ->
			  if withgov
			  then Printf.sprintf "%s---%s" kernel_info.abbrv kernel_info.governor
			  else kernel_info.abbrv)
		        avail));
	        failwith "missing a required kernel"
	      end)
	  (List.map snd wanted);
      res

let resolve_machine avail =
  try
    let chosen = List.find (fun x -> x.machine = realhost) avail in
    List.iter
      (fun x ->
	ignore(Sys.command (Printf.sprintf "apt-get install -y %s" x)))
      chosen.dependencies;
    chosen.env
  with Not_found -> []

let compatible_machine = function
    [] -> true
  | allowed ->
      if !force_machine || List.mem fullhost allowed
      then true
      else
	let get_front s = List.hd(Str.split (Str.regexp "-") s) in
	let allowed_fronts = List.map get_front allowed in (* inefficient *)
	not(List.mem realhost allowed_fronts)

let test2script kernel s runs =
  let unsafe test =
    let pieces = Str.split (Str.regexp_string "2>&1") test in
    List.exists
      (fun x ->
        try ignore(String.index_from x 0 '&'); true
        with Not_found ->
          try ignore(String.index_from x 0 '|'); true
          with Not_found -> false)
      pieces in
  let make_script test =
    let (nm,o) = Filename.open_temp_file "run" ".sh" in
    Printf.fprintf o "#!/bin/bash\n%s\n" test;
    close_out o;
    ignore(Sys.command ("chmod 755 "^nm));
    nm in
  let test =
    List.map
      (fun test ->
        if unsafe test
	then
	  let nm = make_script test in
	  (nm,[nm])
	else (test,[]))
      s.test in
  let (tests,files) = List.split test in
  let files = List.concat files in
  match tests with
    [test] -> ({s with test = [test]},files)
  | _ ->
    let test =
      String.concat "\n"
        (List.map (fun x -> x ^ " &") tests) in
    let test = Printf.sprintf "%s\nwait\n" test in
    let test = make_script test in
    ({s with test = [test]},test :: files)

(* ----------------------------------------------------- *)

let make_systemd o cmd =
    let env =
    	if List.mem "USER" !env_vars
	then
	  let name = Sys.getenv "LC_MYNAME" in
	  Printf.sprintf "Environment=LC_MYNAME=%s\n" name
	else "" in
    Printf.fprintf o
"[Unit]
Description=Test reboot
Requires=autofs.service
After=autofs.service

[Service]
Type=oneshot
WorkingDirectory=/tmp
%s
ExecStart=%s ; /bin/rm /etc/systemd/system/run_everything.service

[Install]
WantedBy=multi-user.target\n" env cmd

let setup_systemd cmd =
  let (fl,o) = Filename.open_temp_file "run_everything" ".service" in
  make_systemd o cmd;
  close_out o;
  ignore(Sys.command ("mv "^fl^" /etc/systemd/system/run_everything.service"));
  ignore(Sys.command "chmod 644 /etc/systemd/system/run_everything.service");
  ignore(Sys.command ("systemctl enable run_everything.service"))

let setup_command config =
  let home = ref "" in
  let cmdline = Array.to_list Sys.argv in
  let cmd = List.hd cmdline in
  let cmd =
    if String.get cmd 0 = '.'
    then Sys.getcwd() ^ (String.sub cmd 1 (String.length cmd - 1))
    else cmd in
  let cmdline = cmd :: (List.tl cmdline) in
  let cmdline =
    List.map
      (function x ->
        let pieces = Str.split_delim (Str.regexp "/") x in
        let pieces =
          List.map
            (fun x ->
              if x = "~"
              then
                begin
                  (if !home = ""
                  then home := List.hd(Util.cmd_to_list "ls -d ~"));
                  !home
                end
              else x)
            pieces in
        String.concat "/" pieces)
      cmdline in
  let cmdline =
    List.map
      (function x ->
        if x = config && String.get config 0 <> '/'
        then Sys.getcwd() ^ "/" ^ x
        else x)
      cmdline in
  setup_systemd (String.concat " " cmdline)

(*
let delete_cron _ =
  let cur = Util.cmd_to_list "crontab -l" in
  match cur with
    [] -> ()
  | [l] ->
      (match Str.split (Str.regexp "[/ \t]+") l with
	"@reboot"::rest ->
	  if List.mem "run_everything" rest
	  then (* seems safe enough *)
	    ignore(Sys.command "crontab -r")
	  else
	    Printf.eprintf "unknown cron line, manual cleanup required"
      | _-> ())
  | _ -> Printf.eprintf "multiple cron lines, manual cleanup required"

let create_cron config =
  let cur = Util.cmd_to_list "crontab -l" in
  match cur with
    [] ->
      let home = List.hd(Util.cmd_to_list "ls -d ~") in
      let cmdline = Array.to_list Sys.argv in
      let cmdline =
	List.map
	  (function x ->
	    let pieces = Str.split_delim (Str.regexp "/") x in
	    let pieces =
	      List.map (fun x -> if x = "~" then home else x) pieces in
	    String.concat "/" pieces)
	  cmdline in
      let cmdline =
	List.map
	  (function x ->
	    if x = config && String.get config 0 <> '/'
	    then Sys.getcwd() ^ "/" ^ x
	    else x)
	  cmdline in
      let (t,o) = Filename.open_temp_file "cron" ".txt" in
      Printf.fprintf o "@reboot echo starting > /home/jlawall/restart_indicator\n";
      Printf.fprintf o "@reboot %s\n" (String.concat " " cmdline);
      close_out o;
      ignore(Sys.command (Printf.sprintf "crontab %s" t));
      if !debug = NODEBUG then Sys.remove t
  | [l] ->
      (match Str.split (Str.regexp "[/ \t]+") l with
	"@reboot"::rest ->
	  if List.mem "run_everything" rest && List.mem config rest
	  then () (* seems safe enough *)
	  else
	    Printf.eprintf "unknown cron line, manual update required"
      | _ -> Printf.eprintf "unknown cron line, manual update required")
  | _ -> Printf.eprintf "multiple cron lines, manual update required"
*)

(* ----------------------------------------------------- *)

let config = ref []
let tests = ref []
let kernels = ref []
let noboot = ref true

let options = [
  "--test", Arg.String (fun s -> noboot := true; tests := s :: !tests), "  desired test";
  "--kernel", Arg.String (fun s -> noboot := true; kernels := s :: !kernels), "  desired kernel (can be kernel---governor)";
  "--debug", Arg.Unit (fun _ -> debug := COMMANDS), "  partial debug (no command output)";
  "--full-debug", Arg.Unit (fun _ -> debug := FULL), "  full debug (show command output)";
  "--light-debug", Arg.Unit (fun _ -> debug := LIGHT), "  light debug (show test names)";
  "--print-debug", Arg.Unit (fun _ -> debug := PRINT), "  print debug (don't run commands)";
  "--continue", Arg.Set continue, "  don't redo tests";
  "--trace-cmd-only", Arg.Unit (fun _ -> hypermax := 0; perfmax := 0; turbomax := 0), "  trace-cmd tests only";
  "--hyperfine-only", Arg.Unit (fun _ -> tracecmdmax := 0; perfmax := 0; turbomax := 0), "  hyperfine tests only";
  "--perf-only", Arg.Unit (fun _ -> tracecmdmax := 0; hypermax := 0; turbomax := 0), "  perf tests only";
  "--turbo-only", Arg.Unit (fun _ -> tracecmdmax := 0; hypermax := 0; perfmax := 0), "  turbostat tests only";
  "--trace-cmd", Arg.Set_int tracecmdfixed, "  trace-cmd runs";
  "--trace-cmd-args", Arg.Set_string trace_cmd_args, "  arguments for trace-cmd";
  "--hyperfine", Arg.Set_int hyperfixed, "  hyperfine runs";
  "--perf", Arg.Set_int perffixed, "  perf runs";
  "--turbo", Arg.Set_int turbofixed, "  turbostat runs";
  "--turbofrom", Arg.Set_int turbofrom, "  increment counter in resulting turbo files";
  "--quick", Arg.Set quick, "  few events for trace-cmd";
  "--interrupts", Arg.Set interrupts, "  interrupt events for trace-cmd";
  "--force-machine", Arg.Set force_machine, "  ignore the machine constraints";
  "--reboot", Arg.Clear noboot, "  reboot to next OS in the series"
]

let anonymous s = config := s :: !config

let usage = "run_everything config... [options]"

let _ =
  Arg.parse (Arg.align options) anonymous usage;
  bug_report := Filename.temp_file "bug_report" ".txt";
  cores := int_of_string(List.hd (Util.cmd_to_list "nproc"));
  let ak = ref [] in
  List.iter
    (function config ->
      let i = open_in config in (* get all machines first *)
      let availmachines = read_machines_config i in
      let machine = resolve_machine availmachines in
      close_in i;
      let i = open_in config in
      let (availkernels,availtests,output) = read_config machine i in
      close_in i;
      let rec loop akrest = function
        [] -> akrest
      | k::ks ->
        let akrest =
	  List.filter (fun k1 -> k <> k1) akrest in
	k :: loop akrest ks in
      ak := loop availkernels !ak;
      let tests = resolve_tests availtests !tests in
      let kernels = resolve_kernels availkernels !kernels in
      (if tests = [] || kernels = []
      then failwith "No tests or no kernels");
      let (current,nextkernel) = get_kernels kernels in
      failo := open_out (output^fullhost^".err");
      let last_kernel = ref "unknown" in
      (match current with
        None -> ()
      | Some current ->
          List.iter
            (function kernel ->
	      last_kernel := kernel.abbrv;
              set_governor kernel;
              List.iter
                (function test ->
                  if compatible_machine test.machines
                  then
                    begin
                      let (test,clean) = test2script kernel test i in
                      (if test.location = "BAD LOCATION"
                      then failwith ("A location must be specified for test: "^test.abbreviation));
                      Printf.eprintf "%s: %s\n" (List.hd (Util.cmd_to_list "date")) test.abbreviation;
                      flush stderr;
                      let c = startup test in
                      (try
                        let warmed_up = ref false in
                        trace_cmd warmed_up kernel test output;
                        hyperfine warmed_up kernel test output;
                        perf warmed_up kernel test output;
                        turbo warmed_up kernel test output
                      with Failure s -> Printf.eprintf "Test %s failed: %s\n" test.abbreviation s);
                      cleanup c test;
                      if !debug = NODEBUG then List.iter Sys.remove clean
                    end)
                tests)
            current);
      Printf.fprintf !failo "done with %s at %s\n" !last_kernel (List.hd (Util.cmd_to_list "date"));
      close_out !failo)
    !config;
  Sys.remove !bug_report;
  let (current,nextkernel) = get_kernels !ak in
  if not !noboot
  then
    match nextkernel with
      None -> ()
    | Some next ->
	setup_command (String.concat " " !config);
	let cmd =
	  Printf.sprintf "/boot/reboot_kexec.sh %s" next in
	ignore(Sys.command cmd)
