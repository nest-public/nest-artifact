(* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * file license.txt for more details.
 *)

let process_output_to_list2 = fun command ->
  let chan = Unix.open_process_in command in
  let res = ref ([] : string list) in
  let rec process_otl_aux () =
    let e = input_line chan in
    res := e::!res;
    process_otl_aux() in
  try process_otl_aux ()
  with End_of_file ->
    let stat = Unix.close_process_in chan in (List.rev !res,stat)
let cmd_to_list command =
  let (l,_) = process_output_to_list2 command in l
let process_output_to_list = cmd_to_list
let cmd_to_list_and_status = process_output_to_list2

let hashadd tbl k v =
  let cell =
    try Hashtbl.find tbl k
    with Not_found ->
      let cell = ref [] in
      Hashtbl.add tbl k cell;
      cell in
  cell := v :: !cell

let hashadd_set tbl k v =
  let cell =
    try Hashtbl.find tbl k
    with Not_found ->
      let cell = ref [] in
      Hashtbl.add tbl k cell;
      cell in
  if not (List.mem v !cell)
  then cell := v :: !cell

let hashadd_unique tbl k v =
  let cell =
    try Hashtbl.find tbl k
    with Not_found ->
      let cell = ref [] in
      Hashtbl.add tbl k cell;
      cell in
  if List.mem v !cell
  then failwith (Printf.sprintf "non unique value\n")
  else cell := v :: !cell

let hashinc tbl k v =
  let cell =
    try Hashtbl.find tbl k
    with Not_found ->
      let cell = ref 0 in
      Hashtbl.add tbl k cell;
      cell in
  cell := v + !cell

let hashincf tbl k v =
  let cell =
    try Hashtbl.find tbl k
    with Not_found ->
      let cell = ref 0. in
      Hashtbl.add tbl k cell;
      cell in
  cell := v +. !cell

let mean l =
  (List.fold_left (+.) 0.0 l) /. (float_of_int (List.length l))

(* thanks to rosetta code *)
let nthroot n a ?(tol=0.001) () =
   let nf = float n in let nf1 = nf -. 1.0 in
   let rec iter x =
      let x' = (nf1 *. x +. a /. (x ** nf1)) /. nf in
      if tol > abs_float (x -. x') then x' else iter x' in
   iter 1.0
(*;;
 
let () =
  Printf.printf "%g\n" (nthroot 10 (7131.5 ** 10.0) ());
  Printf.printf "%g\n" (nthroot 5 34.0 ());*)

let geomean l =
  let l = List.filter (fun x -> not(Float.is_nan x)) l in
  nthroot (List.length l) (List.fold_left ( *. ) 1.0 l) ()

let geosd l =
  let l = List.filter (fun x -> not(Float.is_nan x)) l in
  let gmean = geomean l in
  let sumlogs =
    List.fold_left
      (fun prev cur ->
	let n = log (cur /. gmean) in
	n *. n)
      0.0 l in
  exp(sqrt(sumlogs /. (float_of_int (List.length l))))

let stddev l = (* sample standard deviation *)
  let lenm1 = float_of_int(List.length l - 1) in
  let mean = mean l in
  let squares =
    List.fold_left
      (fun prev xi ->
	let tmp = xi -. mean in
	(tmp *. tmp) +. prev)
      0.0 l in
  sqrt(squares /. lenm1)

let pearson l =
  let l1 = List.fold_left (fun prev (x,_) -> x :: prev) [] l in
  let l2 = List.fold_left (fun prev (_,y) -> y :: prev) [] l in
  let avgl1 = mean l1 in
  let avgl2 = mean l2 in
  let num =
    List.fold_left2
      (fun prev x y ->
	prev +. ((x -. avgl1) *. (y -. avgl2)))
      0. l1 l2 in
  let denmaker l avg =
    sqrt
      (List.fold_left
	 (fun prev x ->
	   let v = x -. avg in
	   prev +. (v *. v))
	 0. l) in
  num /. ((denmaker l1 avgl1) *. (denmaker l2 avgl2))

let speedup_and_std lower_is_better refavg values =
  if lower_is_better
  then
    let speedup = refavg /. (mean values) in
    let slowdowns =
      List.map (fun v -> v /. refavg) values in
    let invstdslowdowns = 1. /. ((mean slowdowns) -. (stddev slowdowns)) in
    (speedup,invstdslowdowns -. speedup)
  else
    let speedup = (mean values) /. refavg in
    let speedups =
      List.map (fun v -> v /. refavg) values in
    let stdspeedups = stddev speedups in
    (speedup,stdspeedups)

(* ----------------------------------------------------- *)

type file_context = string * bool * bool

(* minimal trace parsing *)
let get_front c s =
  try String.sub s 0 (String.index s c)
  with _ -> s

let rec skip_to_sleep i interrupt =
  let l = input_line i in
  let (core,time,l) = Parse_line.parse_line l interrupt ["sched_switch"] in
  match l with
    Parse_line.Sched_switch("sleep",_,Parse_line.Terminate,_,_) -> time
  | _ -> skip_to_sleep i interrupt

let rec skip_front_continue (file,after_sleep,interrupt) =
  let i = open_in file in
  let i1 = open_in file in
  let rec loop _ =
    let l = input_line i in
    let _ = input_line i1 in
    if get_front '=' l = "cpus"
    then
      let cpus = List.nth (Str.split (Str.regexp "=") l) 1 in
      let cpus = int_of_string cpus in
      let time =
	if after_sleep
	then
	  begin
	    close_in i1;
	    skip_to_sleep i interrupt
	  end
	else
	  begin
	    let l =
	      try input_line i1
	      with End_of_file ->
		failwith "no data - did you ask for --freq but have no frequency info?" in
	    close_in i1;
	    if interrupt
	    then
	      let front = List.hd (Str.split (Str.regexp ": ") l) in
	      let time = List.hd(List.rev(Str.split (Str.regexp "[ \t]+") front)) in
	      float_of_string time
	    else
	      match Str.split (Str.regexp_string "]") l with
		_::rest::_ ->
		  (match Str.split (Str.regexp_string ":") rest with
		    time::_ -> float_of_string(String.trim time)
		  | _ -> failwith "no time?")
	      | _ -> failwith "no time?"
	  end in
      (cpus,time)
    else loop() in
  let (cpus,time) = loop() in
  (cpus,time,i)

let skip_front file =
  let (cpus,time,i) = skip_front_continue file in
  close_in i;
  (cpus,time)

let parse_all_for_time file =
  let (_,_,interrupt) = file in
  let get_time l =
    match Parse_line.parse_line l interrupt [] with
      (_,_,Parse_line.Not_supported(_,time,_,_)) ->
       float_of_string time
    | _ -> failwith "not possible" in
  let last_time = ref 0.0 in
  let (_,start,i) = skip_front_continue file in (* # cpus *)
  let l = input_line i in
  let last_line = ref l in
  let rec loop _ =
    let l = input_line i in
    last_line := l;
    loop() in
  (try loop() with End_of_file -> ());
  last_time := get_time !last_line;
  close_in i;
  start,!last_time

(* ----------------------------------------------------- *)

(* get text traces *)
let get_files tmp files endpoint after_sleep interrupt events =
  List.map
    (function file ->
      let base = Filename.chop_extension file in
      if Filename.extension file = ".dat"
      then
	begin
	  let b = String.concat "_" (Str.split (Str.regexp "/") base) in
	  let fl =
	    match tmp with
	      Some tmp -> Filename.temp_file ~temp_dir:tmp b ".txt"
	    | None -> Filename.temp_file b ".txt" in
	  let arg = if interrupt then "-l" else "" in
	  let cmd =
	    let (_,_,e) = endpoint in
	    let extra =
	      if events = []
	      then ""
	      else
		let events =
		  if List.mem "sched_process_exec" events
		  then "sched_process_fork" :: events (* see parse_line.ml *)
		  else events in
		let events =
		  String.concat " "
		    (List.map (fun x -> "-F "^x) events) in
		if after_sleep
		then "-F '.*:prev_comm == \"sleep\"' "^events
		else events in
	    if e < 0.0
	    then Printf.sprintf "trace-cmd report %s %s %s > %s" arg extra file fl
	    else
	      let e = if after_sleep then e +. 1.5 else e in
	      let e = int_of_float(e *. 1000.) + 1 in
	      let ofl = Filename.temp_file b ".dat"  in
	      let c1 = Printf.sprintf "trace-cmd split -m %d -i %s -o %s > /dev/null 2>&1" e file ofl in
	      let c2 = Printf.sprintf "trace-cmd report %s %s %s.1 > %s" arg extra ofl fl in
	      let c3 = Printf.sprintf "/bin/rm %s.1" ofl in
	      Printf.sprintf "%s ; %s ; %s" c1 c2 c3 in
	  let res = Sys.command cmd in
	  (if res <> 0 then (Printf.eprintf "failure on %s: %d\n" file res; flush stderr));
	  (base,(fl,after_sleep,interrupt),true)
	end
      else (base,(file,after_sleep,interrupt),false))
    files

(* ----------------------------------------------------- *)

let remove (file,_,_) = Sys.remove file

(* get common endpoint if none is provided *)
let get_endpoint files endpoint =
  let (str,mn,mx) = endpoint in
  if mx < 0.0
  then
    let (maxtime,env) =
      List.fold_left
	(fun (maxtime,env) (base,file,drop) ->
	  let (start,last_time) =
	    try parse_all_for_time file
	    with e ->
	      begin
		(if drop then remove file);
		raise e
	      end in
	  (max maxtime (last_time -. start), (file,last_time)::env))
	(0.0,[]) files in
    (Some env,(str,mn,maxtime))
  else (None,endpoint)

(* ----------------------------------------------------- *)

let parse_loop ((file, after_sleep, interrupt) as ctx) startpoint endpoint
    elements structure process_line =
  let last = ref (-1.0) in
  let (cores,start,i) = skip_front_continue ctx in (* # cpus *)
  let rec loop _ =
    let l = input_line i in
    let (core,time,l) = Parse_line.parse_line l interrupt elements in
    last := time;
    let startpoint = startpoint +. start in
    let endpoint = if endpoint <= 0.0 then endpoint else endpoint +. start in
    if endpoint <= 0.0 || time <= endpoint
    then
      begin
	process_line time core structure startpoint l;
	loop()
      end in
  let res =
    try (loop();true)
    with
      End_of_file -> true
    | Failure s ->
	Printf.printf "Failure in %s: %s\n" file s; flush stdout;
	false in
  close_in i;
  (res,start,!last,structure)

(* ----------------------------------------------------- *)

let choose_target target files =
  match !target with
    [] ->
      let fronts =
	List.map
	  (function file ->
	    let file = get_front '_' file in
	    match Str.split (Str.regexp_string ".") file with
	      [_;_;"x"] -> file (* NAS *)
	    | ["parsec";x] | ["splash2";x] | ["splash2x";x] -> x
	    | _ -> file)
	  files in
      let fronts =
	List.fold_left
	  (fun prev x -> if List.mem x prev then prev else x::prev)
	  [] fronts in
      let fronts =
	List.map
	  (function file ->
	    if String.length file > 15
	    then String.sub file 0 15
	    else file)
	  fronts in
      target := fronts
  | _-> ()

let take n l =
  let rec loop n = function
      [] -> []
    | x::xs ->
	if n = 0
	then []
	else x :: loop (n-1) xs in
  loop n l

let mask2cores msk =
  let newmask = Str.split (Str.regexp ",") msk in
  List.concat
    (List.map
       (fun l ->
	 match Str.split (Str.regexp_string "-") l with
	   [a;b] ->
	     let mx = int_of_string b in
	     let rec loop n =
	       if n = mx then [n]
	       else n :: loop (n+1) in
	     loop (int_of_string a)
	 | [a] -> [int_of_string a]
	 | _ -> failwith "bad mask range")
       newmask)

(* ----------------------------------------------------- *)
(* finding phase boundaries by command name *)

let matches s2 matches_succeeded s1 = (* is s1 a prefix of s2 *)
  let n1 = String.length s1 in
  let n2 = String.length s2 in
  let res = n2 >= n1 && (String.sub s2 0 n1 = s1) in
  (if res then matches_succeeded := true);
  res

let process_line time core (commands,live,livepids,tbl,start,index) startpoint l =
  let check_relevant cmd pid commands =
    if List.exists (matches cmd (ref false)) commands
    then
      begin
	live := Some cmd;
	Hashtbl.replace livepids pid ();
	let i = !index in
	index := !index + 1;
	let s = time -. start in
	hashadd tbl cmd (s,s,cmd,[pid],i)
      end in
  if time >= startpoint
  then
    match l with
      Parse_line.Sched_process_exec(cmd,_,pid,oldpid) ->
	let cmd = Filename.basename cmd in
	if !live = None
	then check_relevant cmd pid commands
	else if pid <> oldpid && Hashtbl.mem livepids oldpid
	then
	  begin
	    Hashtbl.remove livepids oldpid;
	    Hashtbl.replace livepids pid ()
	  end
    | Parse_line.Sched_process_fork(_,parent_pid,cmd,pid) ->
	if !live = None
	then check_relevant cmd pid commands
	else if Hashtbl.mem livepids parent_pid
	then Hashtbl.replace livepids pid ()
    | Parse_line.Sched_switch(fromcmd,frompid,Parse_line.Terminate,tocmd,topid) ->
	(match !live with
	  Some cmd ->
	    Hashtbl.remove livepids frompid;
	    if Hashtbl.length livepids = 0
	    then
	      begin
		live := None;
		let cell = Hashtbl.find tbl cmd in
		(match !cell with
		  (s,e,cmd,pid,index)::rest -> cell := (s,time -. start,cmd,pid,index)::rest
		| _ -> failwith "not possible")
	      end
	| _ -> ());
	(if !live = None
	then check_relevant tocmd topid commands)
    | _ -> ()

(* return a table of start and end points that match a command *)
let interpret_show_commands commands file startpoint endpoint =
  let commands =
    let commands = List.map String.trim commands in
    List.filter (fun x -> x <> "" && String.get x 0 <> '#') commands in
  let events = ["sched_switch";"sched_process_exec";"sched_process_fork"] in
  let tbl = Hashtbl.create 101 in
  let live = ref None in
  let livepids = Hashtbl.create 101 in
  let index = ref 0 in
  let (_cores,start) = skip_front file in (* # cpus *)
  let (file,after_sleep,interrupt) = file in
  let file = (file,false,interrupt) in (* start point may be before after_sleep *)
  let (success,start,last,(commands,live,livepids,tbl,_,_)) =
    parse_loop file startpoint endpoint events (commands,live,livepids,tbl,start,index)
      process_line in
  (match !live with
    None -> ()
  | Some cmd ->
      let cell = Hashtbl.find tbl cmd in
      (match !cell with
	(s,e,cmd,pid,index)::rest -> cell := (s,last -. start,cmd,pid,index)::rest
      | _ -> failwith "not possible"));
  let res = Hashtbl.fold (fun k v r -> !v @ r) tbl [] in
  let res =
    List.rev
      (List.fold_left
        (fun prev (s,e,cmd,pid,index) ->
	  if e <= 0.
	  then prev
	  else (max 0. s,e,cmd,pid,index) :: prev)
	[] res) in
  List.sort compare res
