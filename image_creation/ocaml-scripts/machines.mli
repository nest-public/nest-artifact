(* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * file license.txt for more details.
 *)

type color_table = int * (int * int * int * int * Printer.color) list

val i80_freq_intervals : color_table
val yeti_freq_intervals : color_table
val troll_freq_intervals : color_table
val yeti_freq_intervals2 : color_table
val troll_freq_intervals2 : color_table
val w2155_freq_intervals : color_table

val get_freqtbl : string -> color_table

val freq_to_color : int -> int * (int * int * int * int * Printer.color) list -> Printer.color
