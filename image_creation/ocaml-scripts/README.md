# Dependencies

The various OCaml programs expect the existence of a program called `jg2pdf` in the path that contains the following code:

```
jgraph $1.jgr | ps2eps -q -l -r 600  | epstopdf --filter > $1.pdf
```

It thus requires the installation of the following packages (Ubuntu):

```
jgraph
ps2eps
texlive-font-utils
```

OCaml should also be installed.  Opam (OCaml's package manager) is not necessary.

Each program has a `--help` option that gives information about the other options that it supports.
