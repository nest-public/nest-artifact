(* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * file license.txt for more details.
 *)

val make_do : bool ref (* if true, doesn't try to create new files *)

type input = RE of string | PHORONIX of string * string

val mkphoronixtrace : string -> string

type csv_data = { command : string; app : string option; runtime : float; std : float; values : float list; lower_is_better : bool; position : int; partial : int option; high_std : bool; csv_file : string }

type auc_data  = { auc : float; aoc : float; aoc2 : float; window : int; running : int; other : int; auc_file : string }

type perf_data = { event : string; evavg : float; evstd : float; evvalues : float list; perf_file : string }

type freq_data = { low : int;  high : int;  count : float;  pct : float; freq_file : string }

type turbo_data = { cpu_joules_avg : float; cpu_joules_std : float; cpu_joules_values : float list;
		    ram_joules_avg : float; ram_joules_std : float; ram_joules_values : float list;
		    runs : int; turbo_file : string }

val unquote : string -> string
val uncomma : string -> string

val get_csv_data   : input -> string -> bool -> csv_data option
val get_perf_data  : input -> int -> perf_data list
val get_auc_data   : input -> int -> string -> auc_data option
val get_freq_data  : input -> int -> string -> freq_data list option
val get_event_data : input -> int -> (string * int) list option
val get_mig_data   : input -> int -> (string * int) list option
val get_turbo_data : input -> turbo_data option
type dac = JSON | TURBO | BOTH
val get_dacapo_stderr : input -> string -> dac -> csv_data option
