(* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * file license.txt for more details.
 *)

type column = Left | Center | Right | RightLeft | RightCenterLeft | Sized of float
type value =
    Color of string * value
  | Bold of value
  | FloatV2 of float
  | FloatV4 of float
  | IntV of int
  | StringV of string
  | Pct of value * value option
  | PM of value * float * value

val print_table : out_channel -> column list -> string list list -> value list list -> bool -> unit

val clean : string -> string
