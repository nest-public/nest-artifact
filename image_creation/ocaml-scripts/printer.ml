(* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * file license.txt for more details.
 *)

type ty = Jgraph | JgraphPs | Latex | Latex2 | Python | Nograph
type graph =
    out_channel * string * ty * float *
      float option * float option * string list ref

let latexclean s =
  String.concat "\\_" (Str.split (Str.regexp "_") s)

let tostring s = Printf.sprintf "\"%s\"" s

let latex2colornames = Hashtbl.create 101 (* used later *)
let namectr = ref 0

type ymax = Nomax | Ymax of float | Ylist of int list

let startgraph file ty time xsize xmin xmax xlabel ysize ymin ymax ylabel =
  let fl =
    match ty with
      Jgraph | JgraphPs -> file ^ ".jgr"
    | Latex | Latex2 -> file ^ ".tex"
    | Python -> file ^ ".json"
    | Nograph -> "" in
  let o =
    if ty = Nograph then stdout else open_out fl in
  let obj = (o,file,ty,time,xmin,xmax,ref []) in
  (match ty with
    Nograph -> ()
  | Jgraph | JgraphPs ->
      let xmin =
	match xmin with
	  None -> ""
	| Some xmin -> Printf.sprintf " min %f" xmin in
      let xmax =
	match xmax with
	  None -> ""
	| Some xmax -> Printf.sprintf " max %f" xmax in
      let ymin =
	match ymin with
	  None -> ""
	| Some m -> Printf.sprintf " min %d" (m-1) in
      let ymaxv =
	match ymax with
	  Nomax -> ""
	| Ymax m -> Printf.sprintf " max %f" m
	| Ylist l -> Printf.sprintf " max %d" (List.length l) in
      Printf.fprintf o
	"newgraph xaxis size %din%s%s label : %s\nyaxis size %din%s%s label : %s\n\n"
	xsize xmin xmax xlabel ysize ymin ymaxv ylabel;
      (match ymax with
	Ylist l ->
	  Printf.fprintf o "no_auto_hash_labels\n";
	  List.iteri (fun i x -> Printf.fprintf o "hash_label at %d : %d\n" i x) l;
	  Printf.fprintf o "\n"
      | _ -> ())
  | Latex ->
      let xmin =
	match xmin with
	  None -> ""
	| Some xmin -> Printf.sprintf ",xmin=%f" xmin in
      let xmax =
	match xmax with
	  None -> ""
	| Some xmax -> Printf.sprintf ",xmax=%f" xmax in
      let ymin =
	match ymin with
	  None -> ""
	| Some m -> Printf.sprintf ",ymin=%d" (m-1) in
      let ymax =
	match ymax with
	  Nomax -> ""
	| Ymax m -> Printf.sprintf ",ymax=%f" m
	| _ -> failwith "not supported" in
      let xlabel = latexclean xlabel in
      let ylabel = latexclean ylabel in
      Printf.fprintf o "\\documentclass{article}\n";
      Printf.fprintf o "\\usepackage{xcolor}\n";
      Printf.fprintf o "\\usepackage[hidelinks]{hyperref}\n";
      Printf.fprintf o "\\usepackage{fullpage}\n";
      Printf.fprintf o "\\usepackage{pgfplots}\n";
      Printf.fprintf o "\\pgfplotsset{compat=newest}\n";
      Printf.fprintf o "\\usetikzlibrary{positioning, decorations.markings}\n";
      Printf.fprintf o "\\hypersetup{hidelinks}\n";
      Printf.fprintf o "\\pagestyle{empty}\n";
      Printf.fprintf o "\\begin{document}\n";
      Printf.fprintf o "\\noindent\\begin{tikzpicture}[define rgb/.code={\\definecolor{mycolor}{RGB}{#1}},rgb color/.style={define rgb={#1},mycolor}]\n";
      Printf.fprintf o "\\begin{axis}[xlabel={%s},ylabel={%s},clip=false\n"
	xlabel ylabel;
      Printf.fprintf o "     %s%s%s%s,width=%fin,height=%din]\n"
	xmin xmax ymin ymax 6.5(*xsize*) ysize
  | Latex2 ->
      (match xmax with
	Some xmax ->
	  Hashtbl.clear latex2colornames;
	  namectr := 0;
(*        let xlabel = latexclean xlabel in
          let ylabel = latexclean ylabel in*)
	  Printf.fprintf o "\\documentclass{article}\n";
	  Printf.fprintf o "\\usepackage{xcolor}\n";
	  Printf.fprintf o "\\usepackage[hidelinks]{hyperref}\n";
	  Printf.fprintf o "\\usepackage{fullpage}\n";
	  Printf.fprintf o "\\usepackage[landscape]{geometry}\n";
	  Printf.fprintf o "\\hypersetup{hidelinks}\n";
	  Printf.fprintf o "\\pagestyle{empty}\n";
	  Printf.fprintf o "\\begin{document}\n"; (* width * height *)
	  Printf.fprintf o "\\noindent\\begin{picture}(%f,%f)\n"
	    (xmax *. 1000.)
	    (match ymax with
	      Nomax -> 100.
	    | Ymax x -> x
	    | _ -> failwith "not supported")
      | None -> failwith "xmax required")
  | Python ->
      Printf.fprintf o "{\"setup\": { \"XLabel\": %s,\"YLabel\": %s,\"Width\": %d,\"Height\": %d,"
	(tostring xlabel) (tostring ylabel) xsize ysize;
      (match (xmin,xmax) with
	(None,None) -> Printf.fprintf o " \"Xmin\": -1, \"Xmax\": -1 },\n"
      | (Some xmin,Some xmax) -> Printf.fprintf o "\"Xmin\": %f,\"Xmax\": %f }\n" xmin xmax
      | _ -> failwith "only xmin or only xmax not supported");
      (match (ymin,ymax) with
	(None,Nomax) -> Printf.fprintf o " \"Ymin\": -1, \"Ymax\": -1 },\n"
      | (Some ymin,Ymax ymax) -> Printf.fprintf o "\"Ymin\": %d,\"Ymax\": %f }\n" ymin ymax
      | _ -> failwith "only ymin or only ymax not supported");
      Printf.fprintf o "\"data\": [\n");
  obj

let endgraph (o,file,ty,_,_,_,labelled) save_tmp tmpdir =
  (match ty with
    Nograph | Jgraph | JgraphPs -> ()
  | Latex ->
      Printf.fprintf o "\\legend{%s}\n"
	(String.concat "," (List.rev !labelled));
      Printf.fprintf o "\\end{axis}\n";
      Printf.fprintf o "\\end{tikzpicture}\n";
      Printf.fprintf o "\\end{document}\n"
  | Latex2 ->
      Printf.fprintf o "\\end{picture}\n";
      Printf.fprintf o "\\end{document}\n"
  | Python ->
      Printf.fprintf o "{ \"LineType\": \"none\",\n";
      Printf.fprintf o "  \"MarkType\": \"none\",\n";
      Printf.fprintf o "  \"Arrow\": false,\n";
      Printf.fprintf o "  \"Red\": 0,\n";
      Printf.fprintf o "  \"Green\": 0,\n";
      Printf.fprintf o "  \"Blue\": 0,\n";
      Printf.fprintf o "  \"Label\": \"none\",\n";
      Printf.fprintf o "  \"HoverLabel\": \"none\",\n";
      Printf.fprintf o "  \"X\": [],\n";
      Printf.fprintf o "  \"Y\": [] }\n";
      Printf.fprintf o "]}\n");
  (if ty = Nograph then () else close_out o);
  (match ty with
    Nograph -> ()
  | Jgraph ->
      let cmd =
	match tmpdir with
	  None -> "jg2pdf " ^ file
	| Some td -> Printf.sprintf "TMPDIR=%s jg2pdf %s" td file in
      ignore (Sys.command cmd);
      let jg = file ^ ".jgr" in
      if save_tmp
      then Printf.eprintf "%s\n" jg
      else Sys.remove jg
  | JgraphPs ->
      ignore
	(Sys.command
	   (Printf.sprintf "jgraph %s.jgr > %s.ps" file file));
      let jg = file ^ ".jgr" in
      if save_tmp
      then Printf.eprintf "%s\n" jg
      else Sys.remove jg
  | Latex | Latex2 ->
      ignore (Sys.command ("pdflatex " ^ file));
      if not save_tmp
      then ignore (Sys.command ("/bin/rm " ^ file ^ ".tex"))
  | Python ->
(*      let front = Filename.dirname(Array.get Sys.argv 0) in
      let cmd =
        Printf.sprintf "python3 %s/print_graph.py %s.json"
	  front file in
      ignore (Sys.command cmd);*)
      if not save_tmp
      then ignore (Sys.command ("/bin/rm " ^ file ^ ".json")))

(* ------------------------------------------------------- *)

type color =
    RGB of float * float * float | Choose of int | SChoose of string
  | Color of string

let colors =
  ref
  [RGB(0.0,0.0,1.0);RGB(0.0,1.0,0.0);RGB(1.0,0.0,0.0);
    RGB(1.0,0.0,1.0);RGB(0.7,0.3,0.0);RGB(0.0,1.0,1.0);
    RGB(0.8,0.0,0.8);RGB(0.0,0.6,0.6);RGB(0.4,0.4,0.0);
    RGB(1.0,0.5,0.0)]

let command_color = Hashtbl.create 101
let command_count : (string,int * int ref) Hashtbl.t = Hashtbl.create 101
let cc = ref 0

let set_command_count cmd n =
  Hashtbl.add command_count cmd (n,ref 0)

let inc_command_count cmd =
  let cell =
    try Hashtbl.find command_count cmd
    with Not_found ->
      let cell = (0,ref 0) in
      Hashtbl.add command_count cmd cell;
      cell in
  snd cell := 1 + !(snd cell)

let get_command_count cmd =
  try !(snd(Hashtbl.find command_count cmd))
  with Not_found -> 0

let command_to_color s assigning =
  try Hashtbl.find command_color s
  with Not_found ->
    let c = (List.nth !colors (!cc mod (List.length !colors))) in
    Hashtbl.add command_color s c;
    cc := !cc + 1;
    c

let assign_colors _ =
  let l = Hashtbl.fold (fun k v r -> ((fst v,!(snd v)),k)::r) command_count [] in
  let l = List.rev(List.sort compare l) in
  let mx = List.length !colors in
  let rec loop n = function
      [] -> ()
    | ((_,k)::rest) as all ->
	if n > 0
	then (ignore(command_to_color k true); loop (n-1) rest)
	else
	  List.iter
	    (fun (_,k) -> Hashtbl.add command_color k (Color "black"))
	    all in
  loop mx l

let ncolors n =
  let rec loop x =
    if (x * x * x) - 1 >= n
    then x
    else loop (x+1) in
  let count = loop 2 in
Printf.eprintf "count: %d\n" count;
  let options =
    let rec loop x =
      if x = count then [] else x :: loop (x+1) in
    loop 0 in
Printf.eprintf "options %d\n" (List.length options);
  let increment =
    1. /. (float_of_int(count - 1)) in
  let rec perm = function
      [] -> [[]]
    | x::xs ->
	let xs = perm xs in
	let rec loop acc = function
	    [] -> acc
	  | y::ys ->
	      let acc =
		List.fold_left
		  (fun prev xs -> (y::xs)::prev)
		  acc xs in
	      loop acc ys in
	loop [] x in
  let res = perm [options;options;options] in
  let res =
    List.filter (fun x -> x <> [0;0;0]) res in
  let rec take n = function
      x::xs -> if n = 0 then [] else x :: (take (n-1) xs)
    | [] -> [] in
  let res = take n res in
  colors :=
    (List.map
       (function
	   [r;g;b] ->
	     let r = increment *. (float_of_int r) in
	     let g = increment *. (float_of_int g) in
	     let b = increment *. (float_of_int b) in
	     RGB(r,g,b)
	 | _ -> failwith "not possible")
       res)

(* ------------------------------------------------------- *)
(* Jgraph configuration *)

let rec jgraphcolor = function
    RGB(r,g,b) -> Printf.sprintf "color %0.2f %0.2f %0.2f" r g b
  | Choose n -> jgraphcolor (List.nth !colors (n mod (List.length !colors)))
  | SChoose s -> jgraphcolor (command_to_color s false)
  | Color "black" -> ""
  | Color "red" -> jgraphcolor(RGB(1.0,0.0,0.0))
  | Color _ -> failwith "only black supported"

(* ------------------------------------------------------- *)
(* LaTeX configuration *)

let rec latexcolor = function
    RGB(r,g,b) ->
      Printf.sprintf "rgb color={%0.2f,%0.2f,%0.2f}"
	(r *. 255.) (g *. 255.) (b *. 255.)
  | Choose n -> latexcolor (List.nth !colors (n mod (List.length !colors)))
  | SChoose s -> latexcolor (command_to_color s false)
  | Color s -> "color="^s

let rec latex2color o = function
    RGB(r,g,b) ->
      (try Hashtbl.find latex2colornames (o,r,g,b)
      with Not_found ->
	begin
	  let nc = !namectr in
	  namectr := !namectr + 1;
	  let col = Printf.sprintf "color%d" nc in
	  Printf.fprintf o "\\definecolor{%s}{rgb}{%f,%f,%f}\n" col r g b;
	  Hashtbl.add latex2colornames (o,r,g,b) col;
	  col
	end)
  | Choose n -> latex2color o (List.nth !colors (n mod (List.length !colors)))
  | SChoose s -> latex2color o (command_to_color s false)
  | Color s -> s

let latexline = function
    "solid" -> ""
  | "dotted" -> "dotted,"
  | "longdash" -> "dashed,"
  | _ -> failwith "unknown line type"

let latexmark = function
    "circle" -> "circle"
  | "box" -> "square"
  | "box cfill 1 1 1" -> "square,fill=white"
  | "diamond" -> "diamond"
  | "triangle" -> "triangle"
  | "x" -> "*"
  | "cross" -> "cross"
  | "ellipse" -> "ellipse"
  | "none" -> "none"
  | _ -> failwith "marktype not supported"

(* ------------------------------------------------------- *)
(* Python configuration *)

let rec pythoncolor = function
    RGB(r,g,b) -> (r,g,b)
  | Choose n -> pythoncolor (List.nth !colors (n mod (List.length !colors)))
  | SChoose s -> pythoncolor (command_to_color s false)
  | Color "black" -> (0.0,0.0,0.0)
  | Color _ -> failwith "only black supported"

let pythonline = function
    "solid" -> "\"solid\""
  | "dotted" -> "\"dotted\""
  | "longdash" -> "\"dashed\""
  | _ -> failwith "unknown line type"

let pythonmark = function
    "circle" -> "o"
  | "box" -> "s"
  | "box cfill 1 1 1" -> "p" (* should be a hollow square, but not sure how to do that *)
  | "diamond" -> "d"
  | "triangle" -> "^"
  | "x" -> "x"
  | "cross" -> "+"
  | "ellipse" -> failwith "not supported"
  | "none" -> "\"None\""
  | _ -> failwith "marktype not supported"

(* ------------------------------------------------------- *)

let lt x endpoint =
  match endpoint with
    None -> true
  | Some endpoint -> x <= endpoint

let gt x startpoint =
  match startpoint with
    None -> true
  | Some startpoint -> startpoint <= x

let truncrange x startpoint endpoint =
  let x =
    match startpoint with
      None -> x
    | Some startpoint -> max startpoint x in
  let x =
    match endpoint with
      None -> x
    | Some endpoint -> min x endpoint in
  x

let safe_map fn l =
  List.rev (List.fold_left (fun prev cur -> (fn cur) :: prev) [] l)

let text g points =
  let (o,file,ty,time,startpoint,endpoint,labelled) = g in
  Printf.fprintf o "\n(*\n";
  List.iter
    (fun (n,s) ->
      Printf.fprintf o "%f = %f: %s\n" (n -. time) n s)
    points;
  Printf.fprintf o "*)\n\n"

type label = NoLabel | Label of string | FontLabel of int * string

let doedge linetype marktype arrow g points color edgelabel comment =
  let (o,file,ty,time,startpoint,endpoint,labelled) = g in
  let tpts = safe_map (fun (x,y) -> (x -. time,y)) points in
  let tpts =
    let rec loop acc = function
	[] -> acc
      | [(x,y)] -> (* we actually have no points beyond the endpoint *)
	  if gt x startpoint
	  then (x,y)::acc else acc
      | (x1,y1)::(((x2,y2)::_) as r) ->
	  if lt x1 endpoint && gt x2 startpoint
	  then loop ((x1,y1)::acc) r
	  else loop acc r in
    List.rev(loop [] tpts) in
  let unnl s =
    String.concat " "
      (Str.split (Str.regexp "\\\\\n") s) in
  let tpts =
    safe_map (fun (x,y) -> (truncrange x startpoint endpoint,y)) tpts in
  let edgelabel =
    match edgelabel with
      NoLabel -> ""
    | Label s | FontLabel (_,s) ->
	if List.mem s !labelled
	then ((if ty = Latex then labelled := "" :: !labelled);
	      let flat = unnl s in
	      if flat = ""
	      then ""
	      else Printf.sprintf " (* %s *)" flat)
	else
	  begin
	    labelled := s :: !labelled;
	    match edgelabel with
	      FontLabel (n,s) ->
		(* the following doesn't work, no idea how to change label font size *)
		Printf.sprintf " label fontsize %d : %s\nlegend custom" n s
	    | _ -> " label : "^s
	  end in
  let label =
    match (points,List.rev points) with
      ((x1,_)::_,(x2,_)::_) ->
	Printf.sprintf "%f-%f = %f%s" x1 x2 (x2 -. x1)
	  (match comment with None -> "" | Some s -> ": "^s)
    | _ -> "" in
  match ty with
    Nograph -> ()
  | Jgraph | JgraphPs ->
      Printf.fprintf o "newcurve linetype %s marktype %s %s%s\npts\n"
	linetype marktype (jgraphcolor color) edgelabel;
      (if List.length tpts <= 2
      then
	Printf.fprintf o "%s (* %s *)\n"
	  (String.concat " "
	     (safe_map (fun (x,y) -> Printf.sprintf "%f %s" x y) tpts))
	  label
      else
	begin
	  List.iter
	    (fun (x,y) -> Printf.fprintf o "%f %s (* %f *)\n" x y (x +. time))
	    tpts;
	  Printf.fprintf o "(* %s *)\n" label
	end
	);
      if arrow then Printf.fprintf o "rarrows\n";
      Printf.fprintf o "\n"
  | Latex ->
      Printf.fprintf o "\\addplot [%s,mark=%s%s%s] coordinates { %s };\n"
	(latexcolor color) (latexmark marktype)
	(latexline linetype) (if arrow then ",->" else "")
	(String.concat " "
	   (safe_map (fun (x,y) -> Printf.sprintf "(%f,%s)" x y) tpts));
      let at (x,y) =
	Printf.fprintf o "\\node at (axis cs:%f,%s){\\href{%s}{\\phantom{x}}}"
	  x y label in
      List.iter at tpts
  | Latex2 ->
      (match tpts with
	[(x1,y1);(x2,y2)] ->
	  let operator = if arrow then "\\vector" else "\\line" in
	  let orient =
	    if x2 > x1
	    then "(1,0)"
	    else "(0,1)" in
	  let x1 = x1 *. 1000. in
	  let x2 = x2 *. 1000. in
	  let dist = max(x2 -. x1) ((float_of_string y2) -. (float_of_string y1)) in (* FIX! *)
	  let cmd = Printf.sprintf "%s%s{%f}" operator orient dist in
	  let color = latex2color o color in
	  Printf.fprintf o "\\put(%f,%s){\\color{%s}{%s}}\n" x1 y1 color cmd;
	  let href = Printf.sprintf "\\href{%s}{\\phantom{x}}" label in
	  Printf.fprintf o "\\put(%f,%s){%s}\n" x1 y1 href
      | _ -> ())
  | Python ->
      Printf.fprintf o "{ \"LineType\": %s,\n" (pythonline linetype);
      Printf.fprintf o "  \"MarkType\": %s,\n" (pythonmark marktype);
      Printf.fprintf o "  \"Arrow\": %b,\n" arrow;
      let (r,g,b) = pythoncolor color in
      Printf.fprintf o "  \"Red\": %0.2f,\n" r;
      Printf.fprintf o "  \"Green\": %0.2f,\n" g;
      Printf.fprintf o "  \"Blue\": %0.2f,\n" b;
      Printf.fprintf o "  \"Label\": \"%s\",\n" edgelabel;
      Printf.fprintf o "  \"HoverLabel\": \"%s\",\n" label;
      Printf.fprintf o "  \"X\": [%s],\n"
	(String.concat ","
	   (safe_map (fun x -> Printf.sprintf "%f" x)
	      (safe_map fst tpts)));
      Printf.fprintf o "  \"Y\": [%s] },\n"
	(String.concat "," (safe_map snd tpts))

let floatify l = safe_map (fun (x,y) -> (x,string_of_int y)) l
let ffloatify l = safe_map (fun (x,y) -> (x,Printf.sprintf "%f" y)) l

type edgetype =
    graph -> (float * int) list -> color -> label -> string option -> unit

type fedgetype =
    graph -> (float * float) list -> color -> label -> string option -> unit

let edge g points   = doedge "solid" "none" false g (floatify points)
let fedge g points  = doedge "solid" "none" false g (ffloatify points)
let dots g points   = doedge "dotted" "none" false g (floatify points)
let fdots g points  = doedge "dotted" "none" false g (ffloatify points)
let arrow g points  = doedge "longdash" "none" true g (floatify points)
let mark m g points = doedge "solid" m false g (floatify points)
let fmark m g points = doedge "solid" m false g (ffloatify points)
let markonly m g points = doedge "none" m false g (floatify points)
let fmarkonly m g points = doedge "none" m false g (ffloatify points)

let notch g points =
  match points with
    [(x,y)] ->
      let y1 = if y > 0 then Printf.sprintf "%d.5" (y-1) else "0" in
      let y2 = Printf.sprintf "%d.5" y in
      doedge "solid" "none" false g [(x,y1);(x,y2)]
  | _ -> failwith "wrong number of points"

let smallnotch g points =
  match points with
    [(x,y)] ->
      let y1 = if y > 0 then Printf.sprintf "%d.75" (y-1) else "0" in
      let y2 = Printf.sprintf "%d.25" y in
      doedge "solid" "none" false g [(x,y1);(x,y2)]
  | _ -> failwith "wrong number of points"

let print_text g x y text =
  let (o,file,ty,time,startpoint,endpoint,labelled) = g in
  match ty with
    Jgraph | JgraphPs ->
      Printf.fprintf o
	"newstring hjc vjb x %f y %f fontsize 2 : %s\n\n"
	(truncrange (x -. time) startpoint endpoint) y text
  | _ -> () (* text not supported *)

(* ------------------------------------------------------------------------ *)
(* TikZ graphs *)

let latex_clean s =
  let s = String.concat "\\_" (Str.split (Str.regexp "_") s) in
  let s = String.concat "\\&" (Str.split (Str.regexp "&") s) in
  let s = String.concat "$>$" (Str.split (Str.regexp ">") s) in
  let s = String.concat "$\\mid$" (Str.split (Str.regexp "|") s) in
  s

let acm_latex_starter o =                                                            
  Printf.fprintf o                                                              
"\\documentclass[sigplan,review,dvipsnames,anonymous,10pt]{acmart}
\\renewcommand\\footnotetextcopyrightpermission[1]{}
\\pagestyle{plain}
\\usepackage{pgfplots}
\\pgfplotsset{compat=newest}
\\usepackage{graphicx}
\\usepackage{url}
\\usepackage{multirow}
\\usepackage{array}
\\usepackage{amsmath,amsthm}
\\usepackage{subfig}
\\newcommand{\\snest}{\\textsc{Nest}}
\\definecolor{gr}{rgb}{0.22,0.753,0.08}
\\definecolor{color1}{rgb}{0.549,0.815,0.960}
\\definecolor{color2}{rgb}{0.607,0.937,0.498}
\\definecolor{color3}{rgb}{0.839,0.929,0.427}
\\definecolor{color4}{rgb}{0.878,0.619,0.141}
\\definecolor{color5}{rgb}{0.396,0.165,0.055}
\\definecolor{color6}{rgb}{0.643,0.368,0.898}
\\definecolor{colortop}{rgb}{0.878,0.141,0.141}
\\title{OS Scheduling with Nest: Keeping Threads Close Together on Warm Cores}
\\copyrightyear{2021}
\\acmYear{2021}
\\setcopyright{acmlicensed}
\\acmConference[EuroSys '22]{EuroSys 2022: The European Conference on Computer Systems}{April 5--8, 2022}{Rennes, France}
\\acmSubmissionID{Paper ???}
\\begin{document}
\\settopmatter{printfolios=true}
\\maketitle\n"

let latex_starter o =
  Printf.fprintf o
"\\documentclass[landscape]{article}
\\usepackage{fullpage}
\\usepackage{pdflscape}
\\usepackage{xcolor}
\\newcommand{\\snest}{\\textsc{Nest}}
\\definecolor{gr}{rgb}{0.22,0.753,0.08}
\\usepackage{pgfplots}
\\pgfplotsset{compat=newest}
\\definecolor{color1}{rgb}{0.549,0.815,0.960}
\\definecolor{color2}{rgb}{0.607,0.937,0.498}
\\definecolor{color3}{rgb}{0.839,0.929,0.427}
\\definecolor{color4}{rgb}{0.878,0.619,0.141}
\\definecolor{color5}{rgb}{0.396,0.165,0.055}
\\definecolor{color6}{rgb}{0.643,0.368,0.898}
\\definecolor{colortop}{rgb}{0.878,0.141,0.141}
\\begin{document}\\small\n"

let latex_ender o =
  Printf.fprintf o "\\end{document}\n"

type width = BIG | MIDDLE of int | SMALL | TINY

let start_tikz_graph o xlabels ylabels xrotation ylabel ynums largest smallest width height stacked legcols quiet center limits legendright extra =
  let xlabels = List.map latex_clean xlabels in
  let center = if center then "\\[" else "" in
  let (ylabel,ypct) =
    if String.contains ylabel '%'
    then ("",true)
    else (ylabel,false) in
  Printf.fprintf o "%s\\begin{tikzpicture}\\begin{axis}[%sybar %s, ymax = %f, ymin = %f,enlarge x limits=%f,\nwidth = %f\\textwidth, height = %fin,\nxtick={%s},xtick pos=left,\nxticklabels={%s},%s\n"
    center ylabels
    (if stacked then "stacked" else "=0pt")
    largest smallest (if width = SMALL then 0.06 else limits)
    (match width with BIG -> (if ylabel = "" && not ypct then 1.05 else 1.) | MIDDLE _ -> (if ylabel = "" then 0.55 else 0.5) | SMALL -> 0.28 | TINY -> 0.15)
    (if width = SMALL then 1.2 else height)
    (String.concat "," (List.mapi (fun i x -> string_of_int i) xlabels))
    (String.concat "," xlabels)
    (if xlabels = [] then "xmajorticks=false," else "");
  (if ypct
  then
    (if ynums
    then Printf.fprintf o "yticklabel={$\\pgfmathprintnumber{\\tick}\\%%$},"
    else Printf.fprintf o "yticklabel={$\\phantom{\\pgfmathprintnumber{\\tick}\\%%}$},")
  else Printf.fprintf o "     ylabel={%s},%s\n" ylabel (if ynums then "" else "yticklabels={},"));
  (match width with SMALL -> Printf.fprintf o "yticklabel style={font=\\footnotesize}," | _ -> ());
  Printf.fprintf o "     xticklabel style={rotate=%d,anchor=%s},legend style={legend columns=%d,at={(%f,1.01)},anchor=%s},bar width=%dpt%s]\n"
    xrotation (if xrotation = 90 then "east" else "north east")
    legcols (if legendright then 1.0 else 0.5) (if legendright then "south east" else "south") (match width with MIDDLE n -> n | SMALL -> 2 | _ -> 3) extra

let end_tikz_graph o len ymax ymin legend quiet center =
  (if not quiet
  then
    begin
      let ln color y =
	Printf.fprintf o "\\addplot[%sdotted,sharp plot,update limits=false] coordinates {(%d,%d) (%d,%d)};\n"
	  color (-1 * len) y (2 * len) y in
      ln "" 0;
      (if ymax >= 5. then ln "red," 5);
      (if ymin <= (-5.) then ln "red," (-5))
    end);
  Printf.fprintf o "\\legend{%s}\n" (String.concat "," legend);
  let center = if center then "\\]" else "" in
  Printf.fprintf o "\\end{axis}\\end{tikzpicture}%s\n\n" center

let tikz_quiet_bar_plot color o data =
  Printf.fprintf o "\\addplot+[%s,error bars/.cd, y dir=both, y explicit] table[x index=0, y index=1, y error index=2] {\n" color;
  List.iter (function (x,v,std,file) -> Printf.fprintf o "%d %f %f\n%% %s\n" x v std file) data;
  Printf.fprintf o "};\n"

let tikz_bar_plot o data =
  Printf.fprintf o "\\addplot+[error bars/.cd, y dir=both, y explicit] table[x index=0, y index=1, y error index=2] {\n";
  List.iter (function (x,v,std,file) -> Printf.fprintf o "%d %f %f\n%% %s\n" x v std file) data;
  Printf.fprintf o "};\n"

let tikz_edge_plot o color data defaultx defaulty forget =
  let data = if data = [] then [(defaultx,defaulty)] else data in
  Printf.fprintf o "\\addplot[%smark=none,color=%s] coordinates {\n"
    (if forget then "forget plot," else "") color;
  List.iter (function (x,v) -> Printf.fprintf o "(%d, %f)\n" x v) data;
  Printf.fprintf o "};\n"

let tikz_mark_plot o color mark data =
  Printf.fprintf o "\\addplot[forget plot,only marks,mark=%s,color=%s%s] coordinates {\n" mark color (if mark = "x" then ",mark size=5" else ",mark size=1");
  List.iter (function (x,v) -> Printf.fprintf o "(%d, %f)\n" x v) data;
  Printf.fprintf o "};\n"

let tikz_mark_fplot o color mark data =
  Printf.fprintf o "\\addplot[forget plot,only marks,mark=%s,color=%s] coordinates {\n" mark color;
  List.iter (function (x,v) -> Printf.fprintf o "(%f, %f)\n" x v) data;
  Printf.fprintf o "};\n"
