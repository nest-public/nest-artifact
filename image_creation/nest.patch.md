# The kernel name

``` diff
diff --git a/.scmversion b/.scmversion
new file mode 100644
index 000000000000..d8d1aeb1f69e
--- /dev/null
+++ b/.scmversion
@@ -0,0 +1,1 @@
+Nest
```
# Printing the current frequency

``` diff
diff --git a/arch/x86/kernel/smpboot.c b/arch/x86/kernel/smpboot.c
index f5ef689dd62a..1f0d45a2c1cc 100644
--- a/arch/x86/kernel/smpboot.c
+++ b/arch/x86/kernel/smpboot.c
@@ -2091,5 +2091,7 @@ void arch_scale_freq_tick(void)
 	this_cpu_write(arch_prev_aperf, aperf);
 	this_cpu_write(arch_prev_mperf, mperf);
 
+	trace_printk("freq %lld\n", div64_u64((cpu_khz * acnt), mcnt));
+
 	if (check_shl_overflow(acnt, 2*SCHED_CAPACITY_SHIFT, &acnt))
 		goto error;
```

# Nest structure

``` diff
diff --git a/include/linux/sched.h b/include/linux/sched.h
index afe01e232935..1f7da827800a 100644
--- a/include/linux/sched.h
+++ b/include/linux/sched.h
@@ -628,6 +628,17 @@ struct wake_q_node {
 	struct wake_q_node *next;
 };
 
+struct expand_mask {
+	spinlock_t			lock;
+	cpumask_t			expand_mask, reserve_mask;
+	int				start;
+	int				count;
+};
+
+extern void init_expand_mask(void);
+extern void clear_expand_mask(int cpu);
+extern void reset_expand_mask(int cpu);
+
 struct task_struct {
 #ifdef CONFIG_THREAD_INFO_IN_TASK
 	/*
```

# Task_struct fields for Nest

``` diff
@@ -671,6 +682,9 @@ struct task_struct {
 	 */
 	int				recent_used_cpu;
 	int				wake_cpu;
+	int				use_expand_mask;
+	int				patience;
+	int				attached;
 #endif
 	int				on_rq;
 
```

# Scheduling domain fields for wakeup work conservation

``` diff
diff --git a/include/linux/sched/topology.h b/include/linux/sched/topology.h
index 820511289857..62f752732fbb 100644
--- a/include/linux/sched/topology.h
+++ b/include/linux/sched/topology.h
@@ -65,6 +65,8 @@ struct sched_domain_shared {
 	atomic_t	ref;
 	atomic_t	nr_busy_cpus;
 	int		has_idle_cores;
+	int		has_idle_threads;
+	int		left_off;
 };
 
 struct sched_domain {
```

# By default, Nest is not used

``` diff
diff --git a/kernel/sched/core.c b/kernel/sched/core.c
index 2d95dc3f4644..4bbeb2522adb 100644
--- a/kernel/sched/core.c
+++ b/kernel/sched/core.c
@@ -1838,6 +1838,8 @@ void do_set_cpus_allowed(struct task_struct *p, const struct cpumask *new_mask)
 {
 	struct rq *rq = task_rq(p);
 	bool queued, running;
+	
+	p->use_expand_mask = 0;
 
 	lockdep_assert_held(&p->pi_lock);
 
```

# Clear the taken flag when the chosen cpu is invalid

``` diff
@@ -2371,8 +2373,11 @@ int select_task_rq(struct task_struct *p, int cpu, int sd_flags, int wake_flags)
 	 * [ this allows ->select_task() to simply return task_cpu(p) and
 	 *   not worry about this generic constraint ]
 	 */
-	if (unlikely(!is_cpu_allowed(p, cpu)))
+	if (unlikely(!is_cpu_allowed(p, cpu))) {
+		if (p->use_expand_mask)
+			atomic_set(&cpu_rq(cpu)->taken,0);
 		cpu = select_fallback_rq(task_cpu(p), p);
+	}
 
 	return cpu;
 }
```

# Close a hole where there are threads on the core but no indication of that

``` diff
@@ -2565,12 +2570,14 @@ void sched_ttwu_pending(void *arg)
 	if (!llist)
 		return;
 
+#ifdef NO_NEST_TTWU
 	/*
 	 * rq::ttwu_pending racy indication of out-standing wakeups.
 	 * Races such that false-negatives are possible, since they
 	 * are shorter lived that false-positives would be.
 	 */
 	WRITE_ONCE(rq->ttwu_pending, 0);
+#endif
 
 	rq_lock_irqsave(rq, &rf);
 	update_rq_clock(rq);
@@ -2586,6 +2593,14 @@ void sched_ttwu_pending(void *arg)
 	}
 
 	rq_unlock_irqrestore(rq, &rf);
+#ifndef NO_NEST_TTWU
+	/*
+	 * rq::ttwu_pending racy indication of out-standing wakeups.
+	 * Races such that false-negatives are possible, since they
+	 * are shorter lived that false-positives would be.
+	 */
+	WRITE_ONCE(rq->ttwu_pending, 0);
+#endif
 }
 
 void send_call_function_single_ipi(int cpu)
```

# Clear the taken flag once the thread reaches the run queue

``` diff
@@ -2976,6 +2991,8 @@ try_to_wake_up(struct task_struct *p, unsigned int state, int wake_flags)
 #endif /* CONFIG_SMP */
 
 	ttwu_queue(p, cpu, wake_flags);
+	if (p->use_expand_mask)
+		atomic_set(&cpu_rq(cpu)->taken,0);
 unlock:
 	raw_spin_unlock_irqrestore(&p->pi_lock, flags);
 out:
```

# Initialize a new task

``` diff
@@ -3333,7 +3350,7 @@ unsigned long to_ratio(u64 period, u64 runtime)
 void wake_up_new_task(struct task_struct *p)
 {
 	struct rq_flags rf;
-	struct rq *rq;
+	struct rq *rq = cpu_rq(smp_processor_id());
 
 	raw_spin_lock_irqsave(&p->pi_lock, rf.flags);
 	p->state = TASK_RUNNING;
@@ -3348,12 +3365,16 @@ void wake_up_new_task(struct task_struct *p)
 	 */
 	p->recent_used_cpu = task_cpu(p);
 	rseq_migrate(p);
+	p->attached = -1;
+	p->patience = INIT_PATIENCE/*cpumask_weight(&p->expand_cpus_mask->mask)*/ + 1;
 	__set_task_cpu(p, select_task_rq(p, task_cpu(p), SD_BALANCE_FORK, 0));
 #endif
 	rq = __task_rq_lock(p, &rf);
 	update_rq_clock(rq);
 	post_init_entity_util_avg(p);
 
+	if (p->use_expand_mask)
+		atomic_set(&cpu_rq(p->cpu)->taken,0);
 	activate_task(rq, p, ENQUEUE_NOCLOCK);
 	trace_sched_wakeup_new(p);
 	check_preempt_curr(rq, p, WF_FORK);
```

# Start the counter for nest compaction if the core becomes idle

``` diff
@@ -3546,3 +3567,9 @@ static inline void
 prepare_task_switch(struct rq *rq, struct task_struct *prev,
 		    struct task_struct *next)
 {
+	if (prev->use_expand_mask && next->pid == 0) {
+		smp_mb__before_atomic();
+		start_spinning(rq->cpu);
+		atomic_set(&rq->drop_expand_ctr, EXPAND_DELAY); // drop_expand is 0
+		smp_mb__after_atomic();
+	}
```

# Clear the counter for nest compaction if the core gets a Nest task

``` diff
+	if (next->use_expand_mask) {
+		smp_mb__before_atomic();
+		atomic_set(&rq->drop_expand_ctr,0);
+		smp_mb__after_atomic();
+		rq->drop_expand = 0;
+	}
 	kcov_prepare_switch(prev);
 	sched_info_switch(rq, prev, next);
 	perf_event_task_sched_out(prev, next);
```

# Trigger nest compaction if the counter has run out

``` diff
@@ -4005,6 +4038,11 @@ void scheduler_tick(void)
 	perf_event_task_tick();
 
 #ifdef CONFIG_SMP
+	smp_mb__before_atomic();
+	atomic_dec_if_positive(&rq->should_spin);
+	if (atomic_fetch_add_unless(&rq->drop_expand_ctr,-1,0) == 1)
+		rq->drop_expand = 1;
+	smp_mb__after_atomic();
 	rq->idle_balance = idle_cpu(cpu);
 	trigger_load_balance(rq);
 #endif
```

# Remove a core from the nest if the Nest task on the core has terminated

``` diff
@@ -4523,6 +4561,15 @@ static void __sched notrace __schedule(bool preempt)
 
 		trace_sched_switch(preempt, prev, next);
 
+		if (prev->use_expand_mask && prev_state & TASK_DEAD && available_idle_cpu(cpu)) {
+			smp_mb__before_atomic();
+			atomic_set(&rq->should_spin,0); // clean up early
+			atomic_set(&rq->drop_expand_ctr,0);
+			smp_mb__after_atomic();
+			rq->drop_expand = 0; // prepare for next time
+			//trace_printk("%d terminated so clear core %d\n",prev->pid,cpu);
+			clear_expand_mask(cpu);
+		}
 		/* Also unlocks the rq: */
 		rq = context_switch(rq, prev, next, &rf);
 	} else {
```

# Initiate Nest on a taskset that covers all the cores of the machine

``` diff
@@ -5997,18 +6044,35 @@ static int get_user_cpu_mask(unsigned long __user *user_mask_ptr, unsigned len,
  *
  * Return: 0 on success. An error code otherwise.
  */
+
 SYSCALL_DEFINE3(sched_setaffinity, pid_t, pid, unsigned int, len,
 		unsigned long __user *, user_mask_ptr)
 {
 	cpumask_var_t new_mask;
+	cpumask_t tmp;
+	struct task_struct *p;
 	int retval;
 
+	rcu_read_lock();
+	p = find_process_by_pid(pid);
+	if (!p) {
+		rcu_read_unlock();
+		return -ESRCH;
+	}
+	rcu_read_unlock();
 	if (!alloc_cpumask_var(&new_mask, GFP_KERNEL))
 		return -ENOMEM;
 
 	retval = get_user_cpu_mask(user_mask_ptr, len, new_mask);
 	if (retval == 0)
 		retval = sched_setaffinity(pid, new_mask);
+
+	cpumask_xor(&tmp,new_mask,cpu_possible_mask);
+	if (cpumask_weight(&tmp) == 0) {
+		int cpu = task_cpu(p);
+		reset_expand_mask(cpu);
+		p->use_expand_mask = 1;
+	}
 	free_cpumask_var(new_mask);
 	return retval;
 }
```

# Initialize Nest on startup

``` diff
@@ -7128,6 +7192,7 @@ void __init sched_init(void)
 	autogroup_init(&init_task);
 #endif /* CONFIG_CGROUP_SCHED */
 
+	init_expand_mask();
 	for_each_possible_cpu(i) {
 		struct rq *rq;
 
@@ -7182,6 +7247,10 @@ void __init sched_init(void)
 		rq->avg_idle = 2*sysctl_sched_migration_cost;
 		rq->max_idle_balance_cost = sysctl_sched_migration_cost;
 
+		atomic_set(&rq->should_spin,0);
+		atomic_set(&rq->taken,0);
+		atomic_set(&rq->drop_expand_ctr,0);
+		rq->drop_expand = 0;
 		INIT_LIST_HEAD(&rq->cfs_tasks);
 
 		rq_attach_root(rq, &def_root_domain);
```

# Functions for nest management

``` diff
diff --git a/kernel/sched/fair.c b/kernel/sched/fair.c
index 1a68a0536add..c190a7221548 100644
--- a/kernel/sched/fair.c
+++ b/kernel/sched/fair.c
@@ -3321,6 +3321,52 @@ static inline void update_tg_load_avg(struct cfs_rq *cfs_rq, int force)
  * caller only guarantees p->pi_lock is held; no other assumptions,
  * including the state of rq->lock, should be made.
  */
+
+static struct expand_mask expand_mask;
+
+void init_expand_mask(void) {
+	spin_lock_init(&expand_mask.lock);
+}
+
+static inline void set_expand_mask(int cpu) {
+	spin_lock(&expand_mask.lock);
+	cpumask_set_cpu(cpu, &expand_mask.expand_mask);
+	if (cpumask_test_cpu(cpu,&expand_mask.reserve_mask)) {
+		cpumask_clear_cpu(cpu, &expand_mask.reserve_mask);
+		expand_mask.count--;
+	}
+	spin_unlock(&expand_mask.lock);
+}
+
+void clear_expand_mask(int cpu) { // cpu is set in expand mask
+	spin_lock(&expand_mask.lock);
+	cpumask_clear_cpu(cpu, &expand_mask.expand_mask);
+	if (!cpumask_test_cpu(cpu,&expand_mask.reserve_mask) && expand_mask.count < RESERVE_MAX) {
+		cpumask_set_cpu(cpu, &expand_mask.reserve_mask);
+		expand_mask.count++;
+	}
+	spin_unlock(&expand_mask.lock);
+}
+
+static inline void set_reserve_mask(int cpu) {
+	spin_lock(&expand_mask.lock);
+	if (!cpumask_test_cpu(cpu,&expand_mask.expand_mask) && !cpumask_test_cpu(cpu,&expand_mask.reserve_mask) && expand_mask.count < RESERVE_MAX) {
+		cpumask_set_cpu(cpu, &expand_mask.reserve_mask);
+		expand_mask.count++;
+	}
+	spin_unlock(&expand_mask.lock);
+}
+
+void reset_expand_mask(int cpu) {
+	spin_lock(&expand_mask.lock);
+	cpumask_clear(&expand_mask.expand_mask);
+	cpumask_set_cpu(cpu, &expand_mask.expand_mask);
+	cpumask_clear(&expand_mask.reserve_mask);
+	expand_mask.start = cpu;
+	expand_mask.count = 0;
+	spin_unlock(&expand_mask.lock);
+}
+
 void set_task_rq_fair(struct sched_entity *se,
 		      struct cfs_rq *prev, struct cfs_rq *next)
 {
@@ -6002,6 +6048,46 @@ static inline bool test_idle_cores(int cpu, bool def)
 	return def;
 }
 
+static inline void set_idle_threads(int cpu, int val)
+{
+	struct sched_domain_shared *sds;
+
+	sds = rcu_dereference(per_cpu(sd_llc_shared, cpu));
+	if (sds)
+		WRITE_ONCE(sds->has_idle_threads, val);
+}
+
+static inline bool test_idle_threads(int cpu, bool def)
+{
+	struct sched_domain_shared *sds;
+
+	sds = rcu_dereference(per_cpu(sd_llc_shared, cpu));
+	if (sds)
+		return READ_ONCE(sds->has_idle_threads);
+
+	return def;
+}
+
+static inline void set_left_off(int cpu, int val)
+{
+	struct sched_domain_shared *sds;
+
+	sds = rcu_dereference(per_cpu(sd_llc_shared, cpu));
+	if (sds)
+		WRITE_ONCE(sds->left_off, val);
+}
+
+static inline int get_left_off(int cpu, int def)
+{
+	struct sched_domain_shared *sds;
+
+	sds = rcu_dereference(per_cpu(sd_llc_shared, cpu));
+	if (sds)
+		return READ_ONCE(sds->left_off);
+
+	return def;
+}
+
 /*
  * Scans the local SMT mask to see if the entire core is idle, and records this
  * information in sd_llc_shared->has_idle_cores.
```

# Cache some information to help wakeup work conservation

``` diff
@@ -6018,6 +6104,9 @@ void __update_idle_core(struct rq *rq)
 	if (test_idle_cores(core, true))
 		goto unlock;
 
+	if (!test_idle_cores(core, true))
+		set_idle_threads(core, 1);
+
 	for_each_cpu(cpu, cpu_smt_mask(core)) {
 		if (cpu == core)
 			continue;
```

# Remember where an idle thread was observed, or record the fact that there are no idle threads, for wakeup work conservation

``` diff
@@ -6036,10 +6125,10 @@ void __update_idle_core(struct rq *rq)
  * there are no idle cores left in the system; tracked through
  * sd_llc->shared->has_idle_cores and enabled through update_idle_core() above.
  */
-static int select_idle_core(struct task_struct *p, struct sched_domain *sd, int target)
+static int select_idle_core(struct task_struct *p, struct sched_domain *sd, int target, int *new_target)
 {
 	struct cpumask *cpus = this_cpu_cpumask_var_ptr(select_idle_mask);
-	int core, cpu;
+	int core, cpu, isidle = -1;
 
 	if (!static_branch_likely(&sched_smt_present))
 		return -1;
@@ -6057,6 +6146,7 @@ static int select_idle_core(struct task_struct *p, struct sched_domain *sd, int
 				idle = false;
 				break;
 			}
+			isidle = cpu;
 		}
 		cpumask_andnot(cpus, cpus, cpu_smt_mask(core));
 
@@ -6068,6 +6158,10 @@ static int select_idle_core(struct task_struct *p, struct sched_domain *sd, int
 	 * Failed to find an idle core; stop looking for one.
 	 */
 	set_idle_cores(target, 0);
+	if (isidle == -1)
+		set_idle_threads(target, false);
+	else
+		*new_target = isidle;
 
 	return -1;
 }
```

# Remember where the search ended, to optimize wakeup work conservation

``` diff
@@ -6146,9 +6240,12 @@ static int select_idle_cpu(struct task_struct *p, struct sched_domain *sd, int t
 
 	cpumask_and(cpus, sched_domain_span(sd), p->cpus_ptr);
 
+	target = get_left_off(target, target);
 	for_each_cpu_wrap(cpu, cpus, target) {
-		if (!--nr)
+		if (!--nr) {
+			set_left_off(target, cpu);
 			return -1;
+		}
 		if (available_idle_cpu(cpu) || sched_idle_cpu(cpu))
 			break;
 	}
```

# Iterate over groups to implement wakeup work conservation

``` diff
@@ -6198,8 +6295,10 @@ select_idle_capacity(struct task_struct *p, struct sched_domain *sd, int target)
  */
 static int select_idle_sibling(struct task_struct *p, int prev, int target)
 {
-	struct sched_domain *sd;
+	struct sched_domain *sd, *sd1;
+	struct sched_group *group, *group0;
 	int i, recent_used_cpu;
+	int otarget = target;
 
 	/*
 	 * For asymmetric CPU capacity systems, our domain of interest is
@@ -6266,19 +6365,53 @@ static int select_idle_sibling(struct task_struct *p, int prev, int target)
 	if (!sd)
 		return target;
 
-	i = select_idle_core(p, sd, target);
+	i = select_idle_core(p, sd, target, &target);
 	if ((unsigned)i < nr_cpumask_bits)
 		return i;
 
+#ifndef NO_NEST_WC
 	i = select_idle_cpu(p, sd, target);
+#else
+	i = select_idle_cpu(p, sd, otarget);
+#endif
 	if ((unsigned)i < nr_cpumask_bits)
 		return i;
 
-	i = select_idle_smt(p, target);
+	i = select_idle_smt(p, otarget);
 	if ((unsigned)i < nr_cpumask_bits)
 		return i;
 
-	return target;
+#ifndef NO_NEST_WC
+	group0 = group = sd->parent->groups;
+	do {
+		struct cpumask *m = sched_group_span(group);
+
+		if (cpumask_test_cpu(otarget, m))
+			goto next;
+
+		sd1 = rcu_dereference(per_cpu(sd_llc, group_first_cpu(group)));
+		if (!sd1 || !READ_ONCE(sd1->shared->has_idle_threads))
+			goto next;
+
+		target = cpumask_next_wrap(otarget,m,otarget,true);
+
+		i = select_idle_core(p, sd1, target, &target);
+		if ((unsigned)i < nr_cpumask_bits)
+			return i;
+
+		i = select_idle_cpu(p, sd1, target);
+		if ((unsigned)i < nr_cpumask_bits)
+			return i;
+
+		i = select_idle_smt(p, target);
+		if ((unsigned)i < nr_cpumask_bits)
+			return i;
+next:
+		group = group->next;
+	} while (group != group0);
+#endif
+
+	return otarget;
 }
 
 /**
```

# The main Nest code

The ifdefs, turning off individual features, help indicate the purpose of each part of the code.  The calls to trace_printk should also be helpful.

``` diff
@@ -6674,6 +6807,7 @@ select_task_rq_fair(struct task_struct *p, int prev_cpu, int sd_flag, int wake_f
 	int cpu = smp_processor_id();
 	int new_cpu = prev_cpu;
 	int want_affine = 0;
+	int impatient = 0;
 	int sync = (wake_flags & WF_SYNC) && !(current->flags & PF_EXITING);
 
 	if (sd_flag & SD_BALANCE_WAKE) {
@@ -6690,6 +6824,164 @@ select_task_rq_fair(struct task_struct *p, int prev_cpu, int sd_flag, int wake_f
 	}
 
 	rcu_read_lock();
+
+	if (p->use_expand_mask) {
+		struct sched_domain *this_sd;
+		cpumask_t *mask_sd;
+		int i;
+#ifdef NEST_HEAVY_DEBUG
+{
+char buf[500];
+cpumap_print_to_pagebuf(true,buf,&expand_mask.expand_mask);
+trace_printk("mask size: %d: %s\n",cpumask_weight(&expand_mask.expand_mask),buf);
+}
+#else
+//trace_printk("mask size: %d\n",cpumask_weight(&expand_mask.expand_mask));
+#endif
+		if (sd_flag & SD_BALANCE_EXEC) {
+			new_cpu = prev_cpu; // why move???
+			goto out;
+		}
+
+#ifndef NO_NEST_ATTACHED_PREV_PATIENCE
+#ifndef NO_NEST_ATTACHED
+		if (p->attached >= 0) {
+			if (cpumask_test_cpu(p->attached,&expand_mask.expand_mask) && cpumask_test_cpu(p->attached,p->cpus_ptr) && available_idle_cpu(p->attached) && !atomic_cmpxchg(&cpu_rq(p->attached)->taken,0,1)) {
+				new_cpu = p->attached;
+				//trace_printk("attached\n");
+				if (new_cpu == prev_cpu)
+					p->patience = INIT_PATIENCE;
+				goto out;
+			}
+			if (p->attached != prev_cpu || !cpumask_test_cpu(p->attached,&expand_mask.expand_mask))
+				p->attached = -1;
+		}
+		// if the thread is not attached somewhere and it is placed outside the mask, then this is a good core for the thread and the core should be in the mask
+		else
+#endif
+#endif
+#ifndef NO_NEST_ATTACHED_PREV_PATIENCE
+#ifndef NO_NEST_PREV
+		if (!cpumask_test_cpu(prev_cpu,&expand_mask.expand_mask) && cpumask_test_cpu(prev_cpu,p->cpus_ptr) && available_idle_cpu(prev_cpu) && !atomic_cmpxchg(&cpu_rq(prev_cpu)->taken,0,1)) {
+			p->attached = new_cpu = prev_cpu;
+			p->patience = INIT_PATIENCE;
+			set_expand_mask(prev_cpu);
+			goto out;
+		}
+#else /* NO_NEST_PREV */
+		{}
+#endif
+#else /* NO_NEST_ATTACHED_PREV_PATIENCE */
+		{}
+#endif
+		this_sd = rcu_dereference(*per_cpu_ptr(&sd_llc,prev_cpu));
+		mask_sd = sched_domain_span(this_sd);
+		for_each_cpu_wrap(i, &expand_mask.expand_mask, prev_cpu) {
+			if (
+#ifndef NO_NEST_EXPAND_MASK_BY_SOCKET
+				cpumask_test_cpu(i,mask_sd) &&
+#endif
+				cpumask_test_cpu(i,p->cpus_ptr) && available_idle_cpu(i)) {
+				struct rq *rq = cpu_rq(i);
+				if (rq->drop_expand) {
+					//trace_printk("dropping %d from expand mask (same socket)\n",i);
+					clear_expand_mask(i);
+					rq->drop_expand = 0;
+					smp_mb__before_atomic();
+					atomic_set(&rq->drop_expand_ctr,0);
+					smp_mb__after_atomic();
+					continue;
+				}
+				if (!atomic_cmpxchg(&cpu_rq(i)->taken,0,1)) {
+					new_cpu = i;
+					//trace_printk("in mask same socket\n");
+					if (new_cpu == prev_cpu) {
+						p->attached = new_cpu;
+						p->patience = INIT_PATIENCE;
+					}
+					goto out;
+				}
+			}
+#ifndef NO_NEST_ATTACHED_PREV_PATIENCE
+#ifndef NO_NEST_PATIENCE
+			if (i == prev_cpu) {
+				if (p->patience)
+					p->patience--;
+				else { // leave expand mask - too small
+					impatient = 1;
+					p->patience = INIT_PATIENCE;
+					//trace_printk("impatient\n");
+					goto reserve;
+				}
+			}
+#endif
+#endif
+		}
+#ifndef NO_NEST_EXPAND_MASK_BY_SOCKET
+#ifndef NO_NEST_OFFSOCKET
+		for_each_cpu_wrap(i, &expand_mask.expand_mask, prev_cpu) {
+			if (!cpumask_test_cpu(i,mask_sd) && cpumask_test_cpu(i,p->cpus_ptr) && available_idle_cpu(i)) {
+				struct rq *rq = cpu_rq(i);
+				if (rq->drop_expand) {
+					//trace_printk("dropping %d from expand mask (other socket)\n",i);
+					clear_expand_mask(i);
+					rq->drop_expand = 0;
+					smp_mb__before_atomic();
+					atomic_set(&rq->drop_expand_ctr,0);
+					smp_mb__after_atomic();
+					continue;
+				}
+				if (!atomic_cmpxchg(&cpu_rq(i)->taken,0,1)) {
+					new_cpu = i;
+					//trace_printk("in mask other socket\n");
+					if (new_cpu == prev_cpu) {
+						p->attached = new_cpu;
+						p->patience = INIT_PATIENCE;
+					}
+					goto out;
+				}
+			}
+		}
+#endif
+#endif
+reserve:
+#ifndef NO_NEST_RESERVE_MASK
+		for_each_cpu_wrap(i, &expand_mask.reserve_mask, expand_mask.start) {
+			if (cpumask_test_cpu(i,mask_sd) && cpumask_test_cpu(i,p->cpus_ptr) && available_idle_cpu(i)) {
+				if (!atomic_cmpxchg(&cpu_rq(i)->taken,0,1)) {
+					new_cpu = i;
+					//trace_printk("reserve same socket\n");
+					if (new_cpu == prev_cpu) {
+						p->attached = new_cpu;
+						p->patience = INIT_PATIENCE;
+					}
+					set_expand_mask(new_cpu);
+					goto out;
+				}
+			}
+		}
+#ifndef NO_NEST_OFFSOCKET
+		for_each_cpu_wrap(i, &expand_mask.reserve_mask, expand_mask.start) {
+			if (!cpumask_test_cpu(i,mask_sd) && cpumask_test_cpu(i,p->cpus_ptr) && available_idle_cpu(i)) {
+				if (!atomic_cmpxchg(&cpu_rq(i)->taken,0,1)) {
+					new_cpu = i;
+					//trace_printk("reserve other socket\n");
+					if (new_cpu == prev_cpu) {
+						p->attached = new_cpu;
+						p->patience = INIT_PATIENCE;
+					}
+					set_expand_mask(new_cpu);
+					goto out;
+				}
+			}
+		}
+#endif
+#else
+		{}
+#endif
+		//trace_printk("cfs\n");
+	}
+
 	for_each_domain(cpu, tmp) {
 		/*
 		 * If both 'cpu' and 'prev_cpu' are part of this domain,
@@ -6721,6 +7013,18 @@ select_task_rq_fair(struct task_struct *p, int prev_cpu, int sd_flag, int wake_f
 		if (want_affine)
 			current->recent_used_cpu = cpu;
 	}
+	if (p->use_expand_mask) {
+#ifndef NO_NEST_RESERVE_MASK
+		if (impatient)
+#else
+		if (1)
+#endif
+			set_expand_mask(new_cpu);
+		else
+			set_reserve_mask(new_cpu);
+	}
+
+out:
 	rcu_read_unlock();
 
 	return new_cpu;
```

# Spinning

``` diff
diff --git a/kernel/sched/idle.c b/kernel/sched/idle.c
index f324dc36fc43..622255f43b5f 100644
--- a/kernel/sched/idle.c
+++ b/kernel/sched/idle.c
@@ -235,6 +235,29 @@ static void cpuidle_idle_call(void)
 static void do_idle(void)
 {
 	int cpu = smp_processor_id();
+
+#ifndef NO_NEST_SPINNING
+	if (atomic_read(&cpu_rq(cpu)->should_spin)) {
+		int sibling, spinning = 1;
+		const cpumask_t *m = cpu_smt_mask(cpu);
+		struct rq *rq = cpu_rq(cpu);
+		while (!tif_need_resched() && atomic_read(&rq->should_spin)) {
+			for_each_cpu(sibling, m)
+				if (sibling != cpu && cpu_rq(sibling)->nr_running) {
+					atomic_set(&rq->should_spin,0);
+					spinning = 0;
+					break;
+				}
+		}
+		if (tif_need_resched()) {
+			if (spinning)
+				atomic_set(&rq->should_spin,0);
+			schedule_idle();
+			return;
+		}
+	}
+#endif
+
 	/*
 	 * If the arch has a polling bit, we maintain an invariant:
 	 *
```

# Runqueue fields and Nest parameters

``` diff
diff --git a/kernel/sched/sched.h b/kernel/sched/sched.h
index 28709f6b0975..f1287661a847 100644
--- a/kernel/sched/sched.h
+++ b/kernel/sched/sched.h
@@ -918,6 +918,8 @@ struct rq {
 
 #ifdef CONFIG_SMP
 	unsigned int		ttwu_pending;
+	atomic_t		should_spin, drop_expand_ctr, taken;
+	int			drop_expand;
 #endif
 	u64			nr_switches;
 
@@ -1097,6 +1099,53 @@ DECLARE_PER_CPU_SHARED_ALIGNED(struct rq, runqueues);
 #define cpu_curr(cpu)		(cpu_rq(cpu)->curr)
 #define raw_rq()		raw_cpu_ptr(&runqueues)
 
+#ifndef NO_NEST_SPIN_DELAY
+#define SPIN_DELAY 2  // nest value
+#else
+#ifndef NO_NEST_SPIN_DELAY_T10
+#define SPIN_DELAY 4
+#else
+#define SPIN_DELAY 20
+#endif
+#endif
+#ifndef NO_NEST_EXPAND_DELAY
+#define EXPAND_DELAY 2  // nest value
+#else
+#ifndef NO_NEST_EXPAND_DELAY_T10
+#define EXPAND_DELAY 4
+#else
+#define EXPAND_DELAY 20
+#endif
+#endif
+#define RESERVE_DELAY 20
+#ifndef NO_NEST_INIT_PATIENCE
+#define INIT_PATIENCE 2  // nest value
+#else
+#ifndef NO_NEST_INIT_PATIENCE_T10
+#define INIT_PATIENCE 4
+#else
+#define INIT_PATIENCE 20
+#endif
+#endif
+#ifndef NO_NEST_RESERVE_MAX
+#define RESERVE_MAX 5
+#else
+#ifndef NO_NEST_RESERVE_MAX_T10
+#define RESERVE_MAX 10
+#else
+#define RESERVE_MAX 50
+#endif
+#endif
+
+static void inline start_spinning(int cpu) {
+	int sibling;
+
+	for_each_cpu(sibling, cpu_smt_mask(cpu))
+		if (sibling != cpu && (!available_idle_cpu(sibling) || atomic_read(&cpu_rq(sibling)->should_spin)))
+			return;
+	atomic_set(&cpu_rq(cpu)->should_spin,SPIN_DELAY);
+}
+
 extern void update_rq_clock(struct rq *rq);
 
 static inline u64 __rq_clock_broken(struct rq *rq)
```
